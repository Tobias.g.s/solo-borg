goog.provide('cljs.repl');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__34167){
var map__34169 = p__34167;
var map__34169__$1 = cljs.core.__destructure_map(map__34169);
var m = map__34169__$1;
var n = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34169__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34169__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["-------------------------"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([(function (){var or__5002__auto__ = new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return [(function (){var temp__5804__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(ns),"/"].join('');
} else {
return null;
}
})(),cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('');
}
})()], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Protocol"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__34216_34521 = cljs.core.seq(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__34218_34522 = null;
var count__34219_34523 = (0);
var i__34220_34524 = (0);
while(true){
if((i__34220_34524 < count__34219_34523)){
var f_34530 = chunk__34218_34522.cljs$core$IIndexed$_nth$arity$2(null,i__34220_34524);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_34530], 0));


var G__34535 = seq__34216_34521;
var G__34536 = chunk__34218_34522;
var G__34537 = count__34219_34523;
var G__34538 = (i__34220_34524 + (1));
seq__34216_34521 = G__34535;
chunk__34218_34522 = G__34536;
count__34219_34523 = G__34537;
i__34220_34524 = G__34538;
continue;
} else {
var temp__5804__auto___34539 = cljs.core.seq(seq__34216_34521);
if(temp__5804__auto___34539){
var seq__34216_34540__$1 = temp__5804__auto___34539;
if(cljs.core.chunked_seq_QMARK_(seq__34216_34540__$1)){
var c__5525__auto___34546 = cljs.core.chunk_first(seq__34216_34540__$1);
var G__34547 = cljs.core.chunk_rest(seq__34216_34540__$1);
var G__34548 = c__5525__auto___34546;
var G__34549 = cljs.core.count(c__5525__auto___34546);
var G__34550 = (0);
seq__34216_34521 = G__34547;
chunk__34218_34522 = G__34548;
count__34219_34523 = G__34549;
i__34220_34524 = G__34550;
continue;
} else {
var f_34551 = cljs.core.first(seq__34216_34540__$1);
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["  ",f_34551], 0));


var G__34552 = cljs.core.next(seq__34216_34540__$1);
var G__34553 = null;
var G__34554 = (0);
var G__34555 = (0);
seq__34216_34521 = G__34552;
chunk__34218_34522 = G__34553;
count__34219_34523 = G__34554;
i__34220_34524 = G__34555;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_34557 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__5002__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([arglists_34557], 0));
} else {
cljs.core.prn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first(arglists_34557)))?cljs.core.second(arglists_34557):arglists_34557)], 0));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Special Form"], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.contains_QMARK_(m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
} else {
return null;
}
} else {
return cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n  Please see http://clojure.org/special_forms#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join('')], 0));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Macro"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"spec","spec",347520401).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["REPL Special Function"], 0));
} else {
}

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m)], 0));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__34235_34572 = cljs.core.seq(new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__34236_34573 = null;
var count__34237_34574 = (0);
var i__34238_34575 = (0);
while(true){
if((i__34238_34575 < count__34237_34574)){
var vec__34262_34577 = chunk__34236_34573.cljs$core$IIndexed$_nth$arity$2(null,i__34238_34575);
var name_34578 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34262_34577,(0),null);
var map__34265_34579 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34262_34577,(1),null);
var map__34265_34580__$1 = cljs.core.__destructure_map(map__34265_34579);
var doc_34581 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34265_34580__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_34582 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34265_34580__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_34578], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_34582], 0));

if(cljs.core.truth_(doc_34581)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_34581], 0));
} else {
}


var G__34585 = seq__34235_34572;
var G__34586 = chunk__34236_34573;
var G__34587 = count__34237_34574;
var G__34588 = (i__34238_34575 + (1));
seq__34235_34572 = G__34585;
chunk__34236_34573 = G__34586;
count__34237_34574 = G__34587;
i__34238_34575 = G__34588;
continue;
} else {
var temp__5804__auto___34590 = cljs.core.seq(seq__34235_34572);
if(temp__5804__auto___34590){
var seq__34235_34591__$1 = temp__5804__auto___34590;
if(cljs.core.chunked_seq_QMARK_(seq__34235_34591__$1)){
var c__5525__auto___34592 = cljs.core.chunk_first(seq__34235_34591__$1);
var G__34593 = cljs.core.chunk_rest(seq__34235_34591__$1);
var G__34594 = c__5525__auto___34592;
var G__34595 = cljs.core.count(c__5525__auto___34592);
var G__34596 = (0);
seq__34235_34572 = G__34593;
chunk__34236_34573 = G__34594;
count__34237_34574 = G__34595;
i__34238_34575 = G__34596;
continue;
} else {
var vec__34274_34598 = cljs.core.first(seq__34235_34591__$1);
var name_34599 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34274_34598,(0),null);
var map__34277_34600 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34274_34598,(1),null);
var map__34277_34601__$1 = cljs.core.__destructure_map(map__34277_34600);
var doc_34602 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34277_34601__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_34603 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34277_34601__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println();

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",name_34599], 0));

cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",arglists_34603], 0));

if(cljs.core.truth_(doc_34602)){
cljs.core.println.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([" ",doc_34602], 0));
} else {
}


var G__34608 = cljs.core.next(seq__34235_34591__$1);
var G__34609 = null;
var G__34610 = (0);
var G__34611 = (0);
seq__34235_34572 = G__34608;
chunk__34236_34573 = G__34609;
count__34237_34574 = G__34610;
i__34238_34575 = G__34611;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__5804__auto__ = cljs.spec.alpha.get_spec(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2(cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.ns_name(n)),cljs.core.name(nm)));
if(cljs.core.truth_(temp__5804__auto__)){
var fnspec = temp__5804__auto__;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["Spec"], 0));

var seq__34278 = cljs.core.seq(new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__34279 = null;
var count__34280 = (0);
var i__34281 = (0);
while(true){
if((i__34281 < count__34280)){
var role = chunk__34279.cljs$core$IIndexed$_nth$arity$2(null,i__34281);
var temp__5804__auto___34617__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5804__auto___34617__$1)){
var spec_34619 = temp__5804__auto___34617__$1;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_34619)], 0));
} else {
}


var G__34621 = seq__34278;
var G__34622 = chunk__34279;
var G__34623 = count__34280;
var G__34624 = (i__34281 + (1));
seq__34278 = G__34621;
chunk__34279 = G__34622;
count__34280 = G__34623;
i__34281 = G__34624;
continue;
} else {
var temp__5804__auto____$1 = cljs.core.seq(seq__34278);
if(temp__5804__auto____$1){
var seq__34278__$1 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(seq__34278__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__34278__$1);
var G__34626 = cljs.core.chunk_rest(seq__34278__$1);
var G__34627 = c__5525__auto__;
var G__34628 = cljs.core.count(c__5525__auto__);
var G__34629 = (0);
seq__34278 = G__34626;
chunk__34279 = G__34627;
count__34280 = G__34628;
i__34281 = G__34629;
continue;
} else {
var role = cljs.core.first(seq__34278__$1);
var temp__5804__auto___34631__$2 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(fnspec,role);
if(cljs.core.truth_(temp__5804__auto___34631__$2)){
var spec_34632 = temp__5804__auto___34631__$2;
cljs.core.print.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([["\n ",cljs.core.name(role),":"].join(''),cljs.spec.alpha.describe(spec_34632)], 0));
} else {
}


var G__34634 = cljs.core.next(seq__34278__$1);
var G__34635 = null;
var G__34636 = (0);
var G__34637 = (0);
seq__34278 = G__34634;
chunk__34279 = G__34635;
count__34280 = G__34636;
i__34281 = G__34637;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Constructs a data representation for a Error with keys:
 *  :cause - root cause message
 *  :phase - error phase
 *  :via - cause chain, with cause keys:
 *           :type - exception class symbol
 *           :message - exception message
 *           :data - ex-data
 *           :at - top stack element
 *  :trace - root cause stack elements
 */
cljs.repl.Error__GT_map = (function cljs$repl$Error__GT_map(o){
return cljs.core.Throwable__GT_map(o);
});
/**
 * Returns an analysis of the phase, error, cause, and location of an error that occurred
 *   based on Throwable data, as returned by Throwable->map. All attributes other than phase
 *   are optional:
 *  :clojure.error/phase - keyword phase indicator, one of:
 *    :read-source :compile-syntax-check :compilation :macro-syntax-check :macroexpansion
 *    :execution :read-eval-result :print-eval-result
 *  :clojure.error/source - file name (no path)
 *  :clojure.error/line - integer line number
 *  :clojure.error/column - integer column number
 *  :clojure.error/symbol - symbol being expanded/compiled/invoked
 *  :clojure.error/class - cause exception class symbol
 *  :clojure.error/cause - cause exception message
 *  :clojure.error/spec - explain-data for spec error
 */
cljs.repl.ex_triage = (function cljs$repl$ex_triage(datafied_throwable){
var map__34314 = datafied_throwable;
var map__34314__$1 = cljs.core.__destructure_map(map__34314);
var via = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34314__$1,new cljs.core.Keyword(null,"via","via",-1904457336));
var trace = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34314__$1,new cljs.core.Keyword(null,"trace","trace",-1082747415));
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$3(map__34314__$1,new cljs.core.Keyword(null,"phase","phase",575722892),new cljs.core.Keyword(null,"execution","execution",253283524));
var map__34315 = cljs.core.last(via);
var map__34315__$1 = cljs.core.__destructure_map(map__34315);
var type = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34315__$1,new cljs.core.Keyword(null,"type","type",1174270348));
var message = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34315__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var data = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34315__$1,new cljs.core.Keyword(null,"data","data",-232669377));
var map__34316 = data;
var map__34316__$1 = cljs.core.__destructure_map(map__34316);
var problems = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34316__$1,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814));
var fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34316__$1,new cljs.core.Keyword("cljs.spec.alpha","fn","cljs.spec.alpha/fn",408600443));
var caller = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34316__$1,new cljs.core.Keyword("cljs.spec.test.alpha","caller","cljs.spec.test.alpha/caller",-398302390));
var map__34317 = new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.first(via));
var map__34317__$1 = cljs.core.__destructure_map(map__34317);
var top_data = map__34317__$1;
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34317__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3((function (){var G__34326 = phase;
var G__34326__$1 = (((G__34326 instanceof cljs.core.Keyword))?G__34326.fqn:null);
switch (G__34326__$1) {
case "read-source":
var map__34334 = data;
var map__34334__$1 = cljs.core.__destructure_map(map__34334);
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34334__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34334__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var G__34339 = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"data","data",-232669377).cljs$core$IFn$_invoke$arity$1(cljs.core.second(via)),top_data], 0));
var G__34339__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34339,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__34339);
var G__34339__$2 = (cljs.core.truth_((function (){var fexpr__34345 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__34345.cljs$core$IFn$_invoke$arity$1 ? fexpr__34345.cljs$core$IFn$_invoke$arity$1(source) : fexpr__34345.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__34339__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__34339__$1);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34339__$2,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__34339__$2;
}

break;
case "compile-syntax-check":
case "compilation":
case "macro-syntax-check":
case "macroexpansion":
var G__34354 = top_data;
var G__34354__$1 = (cljs.core.truth_(source)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34354,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),source):G__34354);
var G__34354__$2 = (cljs.core.truth_((function (){var fexpr__34360 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__34360.cljs$core$IFn$_invoke$arity$1 ? fexpr__34360.cljs$core$IFn$_invoke$arity$1(source) : fexpr__34360.call(null,source));
})())?cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(G__34354__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397)):G__34354__$1);
var G__34354__$3 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34354__$2,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__34354__$2);
var G__34354__$4 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34354__$3,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__34354__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34354__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__34354__$4;
}

break;
case "read-eval-result":
case "print-eval-result":
var vec__34367 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34367,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34367,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34367,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34367,(3),null);
var G__34377 = top_data;
var G__34377__$1 = (cljs.core.truth_(line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34377,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),line):G__34377);
var G__34377__$2 = (cljs.core.truth_(file)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34377__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file):G__34377__$1);
var G__34377__$3 = (cljs.core.truth_((function (){var and__5000__auto__ = source__$1;
if(cljs.core.truth_(and__5000__auto__)){
return method;
} else {
return and__5000__auto__;
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34377__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null))):G__34377__$2);
var G__34377__$4 = (cljs.core.truth_(type)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34377__$3,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type):G__34377__$3);
if(cljs.core.truth_(message)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34377__$4,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message);
} else {
return G__34377__$4;
}

break;
case "execution":
var vec__34387 = cljs.core.first(trace);
var source__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34387,(0),null);
var method = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34387,(1),null);
var file = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34387,(2),null);
var line = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34387,(3),null);
var file__$1 = cljs.core.first(cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p1__34306_SHARP_){
var or__5002__auto__ = (p1__34306_SHARP_ == null);
if(or__5002__auto__){
return or__5002__auto__;
} else {
var fexpr__34392 = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["NO_SOURCE_PATH",null,"NO_SOURCE_FILE",null], null), null);
return (fexpr__34392.cljs$core$IFn$_invoke$arity$1 ? fexpr__34392.cljs$core$IFn$_invoke$arity$1(p1__34306_SHARP_) : fexpr__34392.call(null,p1__34306_SHARP_));
}
}),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(caller),file], null)));
var err_line = (function (){var or__5002__auto__ = new cljs.core.Keyword(null,"line","line",212345235).cljs$core$IFn$_invoke$arity$1(caller);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return line;
}
})();
var G__34396 = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890),type], null);
var G__34396__$1 = (cljs.core.truth_(err_line)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34396,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471),err_line):G__34396);
var G__34396__$2 = (cljs.core.truth_(message)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34396__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742),message):G__34396__$1);
var G__34396__$3 = (cljs.core.truth_((function (){var or__5002__auto__ = fn;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
var and__5000__auto__ = source__$1;
if(cljs.core.truth_(and__5000__auto__)){
return method;
} else {
return and__5000__auto__;
}
}
})())?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34396__$2,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994),(function (){var or__5002__auto__ = fn;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return (new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[source__$1,method],null));
}
})()):G__34396__$2);
var G__34396__$4 = (cljs.core.truth_(file__$1)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34396__$3,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397),file__$1):G__34396__$3);
if(cljs.core.truth_(problems)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__34396__$4,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595),data);
} else {
return G__34396__$4;
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__34326__$1)].join('')));

}
})(),new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358),phase);
});
/**
 * Returns a string from exception data, as produced by ex-triage.
 *   The first line summarizes the exception phase and location.
 *   The subsequent lines describe the cause.
 */
cljs.repl.ex_str = (function cljs$repl$ex_str(p__34416){
var map__34417 = p__34416;
var map__34417__$1 = cljs.core.__destructure_map(map__34417);
var triage_data = map__34417__$1;
var phase = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34417__$1,new cljs.core.Keyword("clojure.error","phase","clojure.error/phase",275140358));
var source = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34417__$1,new cljs.core.Keyword("clojure.error","source","clojure.error/source",-2011936397));
var line = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34417__$1,new cljs.core.Keyword("clojure.error","line","clojure.error/line",-1816287471));
var column = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34417__$1,new cljs.core.Keyword("clojure.error","column","clojure.error/column",304721553));
var symbol = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34417__$1,new cljs.core.Keyword("clojure.error","symbol","clojure.error/symbol",1544821994));
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34417__$1,new cljs.core.Keyword("clojure.error","class","clojure.error/class",278435890));
var cause = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34417__$1,new cljs.core.Keyword("clojure.error","cause","clojure.error/cause",-1879175742));
var spec = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34417__$1,new cljs.core.Keyword("clojure.error","spec","clojure.error/spec",2055032595));
var loc = [cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__5002__auto__ = source;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return "<cljs repl>";
}
})()),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1((function (){var or__5002__auto__ = line;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return (1);
}
})()),(cljs.core.truth_(column)?[":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column)].join(''):"")].join('');
var class_name = cljs.core.name((function (){var or__5002__auto__ = class$;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return "";
}
})());
var simple_class = class_name;
var cause_type = ((cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, ["RuntimeException",null,"Exception",null], null), null),simple_class))?"":[" (",simple_class,")"].join(''));
var format = goog.string.format;
var G__34426 = phase;
var G__34426__$1 = (((G__34426 instanceof cljs.core.Keyword))?G__34426.fqn:null);
switch (G__34426__$1) {
case "read-source":
return (format.cljs$core$IFn$_invoke$arity$3 ? format.cljs$core$IFn$_invoke$arity$3("Syntax error reading source at (%s).\n%s\n",loc,cause) : format.call(null,"Syntax error reading source at (%s).\n%s\n",loc,cause));

break;
case "macro-syntax-check":
var G__34431 = "Syntax error macroexpanding %sat (%s).\n%s";
var G__34432 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__34433 = loc;
var G__34434 = (cljs.core.truth_(spec)?(function (){var sb__5647__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__34437_34763 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__34438_34764 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__34439_34765 = true;
var _STAR_print_fn_STAR__temp_val__34440_34766 = (function (x__5648__auto__){
return sb__5647__auto__.append(x__5648__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__34439_34765);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__34440_34766);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__34412_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__34412_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__34438_34764);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__34437_34763);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__5647__auto__);
})():(format.cljs$core$IFn$_invoke$arity$2 ? format.cljs$core$IFn$_invoke$arity$2("%s\n",cause) : format.call(null,"%s\n",cause)));
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__34431,G__34432,G__34433,G__34434) : format.call(null,G__34431,G__34432,G__34433,G__34434));

break;
case "macroexpansion":
var G__34448 = "Unexpected error%s macroexpanding %sat (%s).\n%s\n";
var G__34449 = cause_type;
var G__34450 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__34451 = loc;
var G__34452 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__34448,G__34449,G__34450,G__34451,G__34452) : format.call(null,G__34448,G__34449,G__34450,G__34451,G__34452));

break;
case "compile-syntax-check":
var G__34455 = "Syntax error%s compiling %sat (%s).\n%s\n";
var G__34456 = cause_type;
var G__34457 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__34458 = loc;
var G__34459 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__34455,G__34456,G__34457,G__34458,G__34459) : format.call(null,G__34455,G__34456,G__34457,G__34458,G__34459));

break;
case "compilation":
var G__34461 = "Unexpected error%s compiling %sat (%s).\n%s\n";
var G__34462 = cause_type;
var G__34463 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__34464 = loc;
var G__34465 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__34461,G__34462,G__34463,G__34464,G__34465) : format.call(null,G__34461,G__34462,G__34463,G__34464,G__34465));

break;
case "read-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error reading eval result%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "print-eval-result":
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5("Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause) : format.call(null,"Error printing return value%s at %s (%s).\n%s\n",cause_type,symbol,loc,cause));

break;
case "execution":
if(cljs.core.truth_(spec)){
var G__34468 = "Execution error - invalid arguments to %s at (%s).\n%s";
var G__34469 = symbol;
var G__34470 = loc;
var G__34471 = (function (){var sb__5647__auto__ = (new goog.string.StringBuffer());
var _STAR_print_newline_STAR__orig_val__34473_34781 = cljs.core._STAR_print_newline_STAR_;
var _STAR_print_fn_STAR__orig_val__34474_34782 = cljs.core._STAR_print_fn_STAR_;
var _STAR_print_newline_STAR__temp_val__34475_34783 = true;
var _STAR_print_fn_STAR__temp_val__34476_34784 = (function (x__5648__auto__){
return sb__5647__auto__.append(x__5648__auto__);
});
(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__temp_val__34475_34783);

(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__temp_val__34476_34784);

try{cljs.spec.alpha.explain_out(cljs.core.update.cljs$core$IFn$_invoke$arity$3(spec,new cljs.core.Keyword("cljs.spec.alpha","problems","cljs.spec.alpha/problems",447400814),(function (probs){
return cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p1__34414_SHARP_){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(p1__34414_SHARP_,new cljs.core.Keyword(null,"in","in",-1531184865));
}),probs);
}))
);
}finally {(cljs.core._STAR_print_fn_STAR_ = _STAR_print_fn_STAR__orig_val__34474_34782);

(cljs.core._STAR_print_newline_STAR_ = _STAR_print_newline_STAR__orig_val__34473_34781);
}
return cljs.core.str.cljs$core$IFn$_invoke$arity$1(sb__5647__auto__);
})();
return (format.cljs$core$IFn$_invoke$arity$4 ? format.cljs$core$IFn$_invoke$arity$4(G__34468,G__34469,G__34470,G__34471) : format.call(null,G__34468,G__34469,G__34470,G__34471));
} else {
var G__34479 = "Execution error%s at %s(%s).\n%s\n";
var G__34480 = cause_type;
var G__34481 = (cljs.core.truth_(symbol)?[cljs.core.str.cljs$core$IFn$_invoke$arity$1(symbol)," "].join(''):"");
var G__34482 = loc;
var G__34483 = cause;
return (format.cljs$core$IFn$_invoke$arity$5 ? format.cljs$core$IFn$_invoke$arity$5(G__34479,G__34480,G__34481,G__34482,G__34483) : format.call(null,G__34479,G__34480,G__34481,G__34482,G__34483));
}

break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__34426__$1)].join('')));

}
});
cljs.repl.error__GT_str = (function cljs$repl$error__GT_str(error){
return cljs.repl.ex_str(cljs.repl.ex_triage(cljs.repl.Error__GT_map(error)));
});

//# sourceMappingURL=cljs.repl.js.map
