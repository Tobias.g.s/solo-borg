goog.provide('my_first_biden.core');
if((typeof my_first_biden !== 'undefined') && (typeof my_first_biden.core !== 'undefined') && (typeof my_first_biden.core.root !== 'undefined')){
} else {
my_first_biden.core.root = reagent.dom.client.create_root(document.getElementById("app"));
}
my_first_biden.core.main_view = (function my_first_biden$core$main_view(){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),"its so joever"], null);
});
my_first_biden.core.render = (function my_first_biden$core$render(){
return reagent.dom.client.render.cljs$core$IFn$_invoke$arity$2(my_first_biden.core.root,reagent.core.as_element.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(goog.DEBUG)?(function (){
return my_first_biden.core.main_view;
}):my_first_biden.core.main_view)));
});
my_first_biden.core.start = (function my_first_biden$core$start(){
console.log("start");

return my_first_biden.core.render();
});
my_first_biden.core.init = (function my_first_biden$core$init(){
my_first_biden.core.start();

return cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1("we own the finishline");
});
goog.exportSymbol('my_first_biden.core.init', my_first_biden.core.init);

//# sourceMappingURL=my_first_biden.core.js.map
