goog.provide('solo_borg.components.player_component');
solo_borg.components.player_component.stat_block = (function solo_borg$components$player_component$stat_block(p__34553){
var map__34554 = p__34553;
var map__34554__$1 = cljs.core.__destructure_map(map__34554);
var key = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34554__$1,new cljs.core.Keyword(null,"key","key",-1516042587));
var stats = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34554__$1,new cljs.core.Keyword(null,"stats","stats",-85643011));
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.box","div.box",2023391427)], null),cljs.core.map_indexed.cljs$core$IFn$_invoke$arity$2((function (idx,stat){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.subtitle","p.subtitle",1220832976),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"key","key",-1516042587),["statblock",cljs.core.str.cljs$core$IFn$_invoke$arity$1(idx)].join('')], null),[cljs.core.name(cljs.core.first(stat)),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.second(stat))].join('')], null);
}),stats));
});
solo_borg.components.player_component.omens = (function solo_borg$components$player_component$omens(p__34555){
var map__34556 = p__34555;
var map__34556__$1 = cljs.core.__destructure_map(map__34556);
var omens = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34556__$1,new cljs.core.Keyword(null,"omens","omens",1897425920));
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.subtitle","p.subtitle",1220832976),["Omens: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(omens)].join('')], null);
});
solo_borg.components.player_component.player = (function solo_borg$components$player_component$player(props){
var player_data = new cljs.core.Keyword(null,"player","player",-97687400).cljs$core$IFn$_invoke$arity$1(props);
return new cljs.core.PersistentVector(null, 8, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.box","div.box",2023391427),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.subtitle","p.subtitle",1220832976),["Name: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(player_data))].join('')], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.subtitle","p.subtitle",1220832976),["Class: ",cljs.core.name(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(player_data))].join('')], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.subtitle","p.subtitle",1220832976),"Stats: "], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [solo_borg.components.player_component.stat_block,player_data], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.subtitle","p.subtitle",1220832976),["Hp: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"hp","hp",-1541237831).cljs$core$IFn$_invoke$arity$1(player_data))].join('')], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"p.subtitle","p.subtitle",1220832976),["Silver: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"silver","silver",1044501468).cljs$core$IFn$_invoke$arity$1(player_data))].join('')], null),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [solo_borg.components.player_component.omens,player_data], null)], null);
});

//# sourceMappingURL=solo_borg.components.player_component.js.map
