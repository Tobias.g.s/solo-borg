goog.provide('shadow.cljs.devtools.client.browser');
shadow.cljs.devtools.client.browser.devtools_msg = (function shadow$cljs$devtools$client$browser$devtools_msg(var_args){
var args__5732__auto__ = [];
var len__5726__auto___37726 = arguments.length;
var i__5727__auto___37727 = (0);
while(true){
if((i__5727__auto___37727 < len__5726__auto___37726)){
args__5732__auto__.push((arguments[i__5727__auto___37727]));

var G__37728 = (i__5727__auto___37727 + (1));
i__5727__auto___37727 = G__37728;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic = (function (msg,args){
if(shadow.cljs.devtools.client.env.log){
if(cljs.core.seq(shadow.cljs.devtools.client.env.log_style)){
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [["%cshadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join(''),shadow.cljs.devtools.client.env.log_style], null),args)));
} else {
return console.log.apply(console,cljs.core.into_array.cljs$core$IFn$_invoke$arity$1(cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [["shadow-cljs: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg)].join('')], null),args)));
}
} else {
return null;
}
}));

(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.cljs.devtools.client.browser.devtools_msg.cljs$lang$applyTo = (function (seq37441){
var G__37442 = cljs.core.first(seq37441);
var seq37441__$1 = cljs.core.next(seq37441);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__37442,seq37441__$1);
}));

shadow.cljs.devtools.client.browser.script_eval = (function shadow$cljs$devtools$client$browser$script_eval(code){
return goog.globalEval(code);
});
shadow.cljs.devtools.client.browser.do_js_load = (function shadow$cljs$devtools$client$browser$do_js_load(sources){
var seq__37448 = cljs.core.seq(sources);
var chunk__37449 = null;
var count__37450 = (0);
var i__37451 = (0);
while(true){
if((i__37451 < count__37450)){
var map__37458 = chunk__37449.cljs$core$IIndexed$_nth$arity$2(null,i__37451);
var map__37458__$1 = cljs.core.__destructure_map(map__37458);
var src = map__37458__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37458__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37458__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37458__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37458__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e37459){var e_37729 = e37459;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_37729);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_37729.message)].join('')));
}

var G__37730 = seq__37448;
var G__37731 = chunk__37449;
var G__37732 = count__37450;
var G__37733 = (i__37451 + (1));
seq__37448 = G__37730;
chunk__37449 = G__37731;
count__37450 = G__37732;
i__37451 = G__37733;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__37448);
if(temp__5804__auto__){
var seq__37448__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37448__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__37448__$1);
var G__37734 = cljs.core.chunk_rest(seq__37448__$1);
var G__37735 = c__5525__auto__;
var G__37736 = cljs.core.count(c__5525__auto__);
var G__37737 = (0);
seq__37448 = G__37734;
chunk__37449 = G__37735;
count__37450 = G__37736;
i__37451 = G__37737;
continue;
} else {
var map__37460 = cljs.core.first(seq__37448__$1);
var map__37460__$1 = cljs.core.__destructure_map(map__37460);
var src = map__37460__$1;
var resource_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37460__$1,new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582));
var output_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37460__$1,new cljs.core.Keyword(null,"output-name","output-name",-1769107767));
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37460__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37460__$1,new cljs.core.Keyword(null,"js","js",1768080579));
$CLJS.SHADOW_ENV.setLoaded(output_name);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load JS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([resource_name], 0));

shadow.cljs.devtools.client.env.before_load_src(src);

try{shadow.cljs.devtools.client.browser.script_eval([cljs.core.str.cljs$core$IFn$_invoke$arity$1(js),"\n//# sourceURL=",cljs.core.str.cljs$core$IFn$_invoke$arity$1($CLJS.SHADOW_ENV.scriptBase),cljs.core.str.cljs$core$IFn$_invoke$arity$1(output_name)].join(''));
}catch (e37461){var e_37738 = e37461;
if(shadow.cljs.devtools.client.env.log){
console.error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name)].join(''),e_37738);
} else {
}

throw (new Error(["Failed to load ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name),": ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(e_37738.message)].join('')));
}

var G__37739 = cljs.core.next(seq__37448__$1);
var G__37740 = null;
var G__37741 = (0);
var G__37742 = (0);
seq__37448 = G__37739;
chunk__37449 = G__37740;
count__37450 = G__37741;
i__37451 = G__37742;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.do_js_reload = (function shadow$cljs$devtools$client$browser$do_js_reload(msg,sources,complete_fn,failure_fn){
return shadow.cljs.devtools.client.env.do_js_reload.cljs$core$IFn$_invoke$arity$4(cljs.core.assoc.cljs$core$IFn$_invoke$arity$variadic(msg,new cljs.core.Keyword(null,"log-missing-fn","log-missing-fn",732676765),(function (fn_sym){
return null;
}),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"log-call-async","log-call-async",183826192),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call async ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
}),new cljs.core.Keyword(null,"log-call","log-call",412404391),(function (fn_sym){
return shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym)].join(''));
})], 0)),(function (){
return shadow.cljs.devtools.client.browser.do_js_load(sources);
}),complete_fn,failure_fn);
});
/**
 * when (require '["some-str" :as x]) is done at the REPL we need to manually call the shadow.js.require for it
 * since the file only adds the shadow$provide. only need to do this for shadow-js.
 */
shadow.cljs.devtools.client.browser.do_js_requires = (function shadow$cljs$devtools$client$browser$do_js_requires(js_requires){
var seq__37462 = cljs.core.seq(js_requires);
var chunk__37463 = null;
var count__37464 = (0);
var i__37465 = (0);
while(true){
if((i__37465 < count__37464)){
var js_ns = chunk__37463.cljs$core$IIndexed$_nth$arity$2(null,i__37465);
var require_str_37743 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_37743);


var G__37744 = seq__37462;
var G__37745 = chunk__37463;
var G__37746 = count__37464;
var G__37747 = (i__37465 + (1));
seq__37462 = G__37744;
chunk__37463 = G__37745;
count__37464 = G__37746;
i__37465 = G__37747;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__37462);
if(temp__5804__auto__){
var seq__37462__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37462__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__37462__$1);
var G__37748 = cljs.core.chunk_rest(seq__37462__$1);
var G__37749 = c__5525__auto__;
var G__37750 = cljs.core.count(c__5525__auto__);
var G__37751 = (0);
seq__37462 = G__37748;
chunk__37463 = G__37749;
count__37464 = G__37750;
i__37465 = G__37751;
continue;
} else {
var js_ns = cljs.core.first(seq__37462__$1);
var require_str_37752 = ["var ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns)," = shadow.js.require(\"",cljs.core.str.cljs$core$IFn$_invoke$arity$1(js_ns),"\");"].join('');
shadow.cljs.devtools.client.browser.script_eval(require_str_37752);


var G__37753 = cljs.core.next(seq__37462__$1);
var G__37754 = null;
var G__37755 = (0);
var G__37756 = (0);
seq__37462 = G__37753;
chunk__37463 = G__37754;
count__37464 = G__37755;
i__37465 = G__37756;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.handle_build_complete = (function shadow$cljs$devtools$client$browser$handle_build_complete(runtime,p__37467){
var map__37468 = p__37467;
var map__37468__$1 = cljs.core.__destructure_map(map__37468);
var msg = map__37468__$1;
var info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37468__$1,new cljs.core.Keyword(null,"info","info",-317069002));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37468__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var warnings = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.distinct.cljs$core$IFn$_invoke$arity$1((function (){var iter__5480__auto__ = (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37469(s__37470){
return (new cljs.core.LazySeq(null,(function (){
var s__37470__$1 = s__37470;
while(true){
var temp__5804__auto__ = cljs.core.seq(s__37470__$1);
if(temp__5804__auto__){
var xs__6360__auto__ = temp__5804__auto__;
var map__37475 = cljs.core.first(xs__6360__auto__);
var map__37475__$1 = cljs.core.__destructure_map(map__37475);
var src = map__37475__$1;
var resource_name = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37475__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
var warnings = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37475__$1,new cljs.core.Keyword(null,"warnings","warnings",-735437651));
if(cljs.core.not(new cljs.core.Keyword(null,"from-jar","from-jar",1050932827).cljs$core$IFn$_invoke$arity$1(src))){
var iterys__5476__auto__ = ((function (s__37470__$1,map__37475,map__37475__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__37468,map__37468__$1,msg,info,reload_info){
return (function shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37469_$_iter__37471(s__37472){
return (new cljs.core.LazySeq(null,((function (s__37470__$1,map__37475,map__37475__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__37468,map__37468__$1,msg,info,reload_info){
return (function (){
var s__37472__$1 = s__37472;
while(true){
var temp__5804__auto____$1 = cljs.core.seq(s__37472__$1);
if(temp__5804__auto____$1){
var s__37472__$2 = temp__5804__auto____$1;
if(cljs.core.chunked_seq_QMARK_(s__37472__$2)){
var c__5478__auto__ = cljs.core.chunk_first(s__37472__$2);
var size__5479__auto__ = cljs.core.count(c__5478__auto__);
var b__37474 = cljs.core.chunk_buffer(size__5479__auto__);
if((function (){var i__37473 = (0);
while(true){
if((i__37473 < size__5479__auto__)){
var warning = cljs.core._nth(c__5478__auto__,i__37473);
cljs.core.chunk_append(b__37474,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name));

var G__37757 = (i__37473 + (1));
i__37473 = G__37757;
continue;
} else {
return true;
}
break;
}
})()){
return cljs.core.chunk_cons(cljs.core.chunk(b__37474),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37469_$_iter__37471(cljs.core.chunk_rest(s__37472__$2)));
} else {
return cljs.core.chunk_cons(cljs.core.chunk(b__37474),null);
}
} else {
var warning = cljs.core.first(s__37472__$2);
return cljs.core.cons(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(warning,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100),resource_name),shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37469_$_iter__37471(cljs.core.rest(s__37472__$2)));
}
} else {
return null;
}
break;
}
});})(s__37470__$1,map__37475,map__37475__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__37468,map__37468__$1,msg,info,reload_info))
,null,null));
});})(s__37470__$1,map__37475,map__37475__$1,src,resource_name,warnings,xs__6360__auto__,temp__5804__auto__,map__37468,map__37468__$1,msg,info,reload_info))
;
var fs__5477__auto__ = cljs.core.seq(iterys__5476__auto__(warnings));
if(fs__5477__auto__){
return cljs.core.concat.cljs$core$IFn$_invoke$arity$2(fs__5477__auto__,shadow$cljs$devtools$client$browser$handle_build_complete_$_iter__37469(cljs.core.rest(s__37470__$1)));
} else {
var G__37758 = cljs.core.rest(s__37470__$1);
s__37470__$1 = G__37758;
continue;
}
} else {
var G__37759 = cljs.core.rest(s__37470__$1);
s__37470__$1 = G__37759;
continue;
}
} else {
return null;
}
break;
}
}),null,null));
});
return iter__5480__auto__(new cljs.core.Keyword(null,"sources","sources",-321166424).cljs$core$IFn$_invoke$arity$1(info));
})()));
if(shadow.cljs.devtools.client.env.log){
var seq__37476_37760 = cljs.core.seq(warnings);
var chunk__37477_37761 = null;
var count__37478_37762 = (0);
var i__37479_37763 = (0);
while(true){
if((i__37479_37763 < count__37478_37762)){
var map__37482_37764 = chunk__37477_37761.cljs$core$IIndexed$_nth$arity$2(null,i__37479_37763);
var map__37482_37765__$1 = cljs.core.__destructure_map(map__37482_37764);
var w_37766 = map__37482_37765__$1;
var msg_37767__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37482_37765__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_37768 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37482_37765__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_37769 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37482_37765__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_37770 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37482_37765__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_37770)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_37768),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_37769),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_37767__$1)].join(''));


var G__37771 = seq__37476_37760;
var G__37772 = chunk__37477_37761;
var G__37773 = count__37478_37762;
var G__37774 = (i__37479_37763 + (1));
seq__37476_37760 = G__37771;
chunk__37477_37761 = G__37772;
count__37478_37762 = G__37773;
i__37479_37763 = G__37774;
continue;
} else {
var temp__5804__auto___37775 = cljs.core.seq(seq__37476_37760);
if(temp__5804__auto___37775){
var seq__37476_37776__$1 = temp__5804__auto___37775;
if(cljs.core.chunked_seq_QMARK_(seq__37476_37776__$1)){
var c__5525__auto___37777 = cljs.core.chunk_first(seq__37476_37776__$1);
var G__37778 = cljs.core.chunk_rest(seq__37476_37776__$1);
var G__37779 = c__5525__auto___37777;
var G__37780 = cljs.core.count(c__5525__auto___37777);
var G__37781 = (0);
seq__37476_37760 = G__37778;
chunk__37477_37761 = G__37779;
count__37478_37762 = G__37780;
i__37479_37763 = G__37781;
continue;
} else {
var map__37483_37782 = cljs.core.first(seq__37476_37776__$1);
var map__37483_37783__$1 = cljs.core.__destructure_map(map__37483_37782);
var w_37784 = map__37483_37783__$1;
var msg_37785__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37483_37783__$1,new cljs.core.Keyword(null,"msg","msg",-1386103444));
var line_37786 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37483_37783__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column_37787 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37483_37783__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var resource_name_37788 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37483_37783__$1,new cljs.core.Keyword(null,"resource-name","resource-name",2001617100));
console.warn(["BUILD-WARNING in ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(resource_name_37788)," at [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(line_37786),":",cljs.core.str.cljs$core$IFn$_invoke$arity$1(column_37787),"]\n\t",cljs.core.str.cljs$core$IFn$_invoke$arity$1(msg_37785__$1)].join(''));


var G__37789 = cljs.core.next(seq__37476_37776__$1);
var G__37790 = null;
var G__37791 = (0);
var G__37792 = (0);
seq__37476_37760 = G__37789;
chunk__37477_37761 = G__37790;
count__37478_37762 = G__37791;
i__37479_37763 = G__37792;
continue;
}
} else {
}
}
break;
}
} else {
}

if((!(shadow.cljs.devtools.client.env.autoload))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(((cljs.core.empty_QMARK_(warnings)) || (shadow.cljs.devtools.client.env.ignore_warnings))){
var sources_to_get = shadow.cljs.devtools.client.env.filter_reload_sources(info,reload_info);
if(cljs.core.not(cljs.core.seq(sources_to_get))){
return shadow.cljs.devtools.client.hud.load_end_success();
} else {
if(cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"after-load","after-load",-1278503285)], null)))){
} else {
shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("reloading code but no :after-load hooks are configured!",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2(["https://shadow-cljs.github.io/docs/UsersGuide.html#_lifecycle_hooks"], 0));
}

return shadow.cljs.devtools.client.shared.load_sources(runtime,sources_to_get,(function (p1__37466_SHARP_){
return shadow.cljs.devtools.client.browser.do_js_reload(msg,p1__37466_SHARP_,shadow.cljs.devtools.client.hud.load_end_success,shadow.cljs.devtools.client.hud.load_failure);
}));
}
} else {
return null;
}
}
});
shadow.cljs.devtools.client.browser.page_load_uri = (cljs.core.truth_(goog.global.document)?goog.Uri.parse(document.location.href):null);
shadow.cljs.devtools.client.browser.match_paths = (function shadow$cljs$devtools$client$browser$match_paths(old,new$){
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2("file",shadow.cljs.devtools.client.browser.page_load_uri.getScheme())){
var rel_new = cljs.core.subs.cljs$core$IFn$_invoke$arity$2(new$,(1));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(old,rel_new)) || (clojure.string.starts_with_QMARK_(old,[rel_new,"?"].join(''))))){
return rel_new;
} else {
return null;
}
} else {
var node_uri = goog.Uri.parse(old);
var node_uri_resolved = shadow.cljs.devtools.client.browser.page_load_uri.resolve(node_uri);
var node_abs = node_uri_resolved.getPath();
var and__5000__auto__ = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$1(shadow.cljs.devtools.client.browser.page_load_uri.hasSameDomainAs(node_uri))) || (cljs.core.not(node_uri.hasDomain())));
if(and__5000__auto__){
var and__5000__auto____$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(node_abs,new$);
if(and__5000__auto____$1){
return new$;
} else {
return and__5000__auto____$1;
}
} else {
return and__5000__auto__;
}
}
});
shadow.cljs.devtools.client.browser.handle_asset_update = (function shadow$cljs$devtools$client$browser$handle_asset_update(p__37484){
var map__37485 = p__37484;
var map__37485__$1 = cljs.core.__destructure_map(map__37485);
var msg = map__37485__$1;
var updates = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37485__$1,new cljs.core.Keyword(null,"updates","updates",2013983452));
var reload_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37485__$1,new cljs.core.Keyword(null,"reload-info","reload-info",1648088086));
var seq__37486 = cljs.core.seq(updates);
var chunk__37488 = null;
var count__37489 = (0);
var i__37490 = (0);
while(true){
if((i__37490 < count__37489)){
var path = chunk__37488.cljs$core$IIndexed$_nth$arity$2(null,i__37490);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__37600_37793 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__37604_37794 = null;
var count__37605_37795 = (0);
var i__37606_37796 = (0);
while(true){
if((i__37606_37796 < count__37605_37795)){
var node_37797 = chunk__37604_37794.cljs$core$IIndexed$_nth$arity$2(null,i__37606_37796);
if(cljs.core.not(node_37797.shadow$old)){
var path_match_37798 = shadow.cljs.devtools.client.browser.match_paths(node_37797.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37798)){
var new_link_37799 = (function (){var G__37632 = node_37797.cloneNode(true);
G__37632.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37798),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37632;
})();
(node_37797.shadow$old = true);

(new_link_37799.onload = ((function (seq__37600_37793,chunk__37604_37794,count__37605_37795,i__37606_37796,seq__37486,chunk__37488,count__37489,i__37490,new_link_37799,path_match_37798,node_37797,path,map__37485,map__37485__$1,msg,updates,reload_info){
return (function (e){
var seq__37633_37800 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__37635_37801 = null;
var count__37636_37802 = (0);
var i__37637_37803 = (0);
while(true){
if((i__37637_37803 < count__37636_37802)){
var map__37641_37804 = chunk__37635_37801.cljs$core$IIndexed$_nth$arity$2(null,i__37637_37803);
var map__37641_37805__$1 = cljs.core.__destructure_map(map__37641_37804);
var task_37806 = map__37641_37805__$1;
var fn_str_37807 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37641_37805__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37808 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37641_37805__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37809 = goog.getObjectByName(fn_str_37807,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37808)].join(''));

(fn_obj_37809.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37809.cljs$core$IFn$_invoke$arity$2(path,new_link_37799) : fn_obj_37809.call(null,path,new_link_37799));


var G__37810 = seq__37633_37800;
var G__37811 = chunk__37635_37801;
var G__37812 = count__37636_37802;
var G__37813 = (i__37637_37803 + (1));
seq__37633_37800 = G__37810;
chunk__37635_37801 = G__37811;
count__37636_37802 = G__37812;
i__37637_37803 = G__37813;
continue;
} else {
var temp__5804__auto___37814 = cljs.core.seq(seq__37633_37800);
if(temp__5804__auto___37814){
var seq__37633_37815__$1 = temp__5804__auto___37814;
if(cljs.core.chunked_seq_QMARK_(seq__37633_37815__$1)){
var c__5525__auto___37816 = cljs.core.chunk_first(seq__37633_37815__$1);
var G__37817 = cljs.core.chunk_rest(seq__37633_37815__$1);
var G__37818 = c__5525__auto___37816;
var G__37819 = cljs.core.count(c__5525__auto___37816);
var G__37820 = (0);
seq__37633_37800 = G__37817;
chunk__37635_37801 = G__37818;
count__37636_37802 = G__37819;
i__37637_37803 = G__37820;
continue;
} else {
var map__37642_37821 = cljs.core.first(seq__37633_37815__$1);
var map__37642_37822__$1 = cljs.core.__destructure_map(map__37642_37821);
var task_37823 = map__37642_37822__$1;
var fn_str_37824 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37642_37822__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37825 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37642_37822__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37826 = goog.getObjectByName(fn_str_37824,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37825)].join(''));

(fn_obj_37826.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37826.cljs$core$IFn$_invoke$arity$2(path,new_link_37799) : fn_obj_37826.call(null,path,new_link_37799));


var G__37827 = cljs.core.next(seq__37633_37815__$1);
var G__37828 = null;
var G__37829 = (0);
var G__37830 = (0);
seq__37633_37800 = G__37827;
chunk__37635_37801 = G__37828;
count__37636_37802 = G__37829;
i__37637_37803 = G__37830;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_37797);
});})(seq__37600_37793,chunk__37604_37794,count__37605_37795,i__37606_37796,seq__37486,chunk__37488,count__37489,i__37490,new_link_37799,path_match_37798,node_37797,path,map__37485,map__37485__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37798], 0));

goog.dom.insertSiblingAfter(new_link_37799,node_37797);


var G__37831 = seq__37600_37793;
var G__37832 = chunk__37604_37794;
var G__37833 = count__37605_37795;
var G__37834 = (i__37606_37796 + (1));
seq__37600_37793 = G__37831;
chunk__37604_37794 = G__37832;
count__37605_37795 = G__37833;
i__37606_37796 = G__37834;
continue;
} else {
var G__37835 = seq__37600_37793;
var G__37836 = chunk__37604_37794;
var G__37837 = count__37605_37795;
var G__37838 = (i__37606_37796 + (1));
seq__37600_37793 = G__37835;
chunk__37604_37794 = G__37836;
count__37605_37795 = G__37837;
i__37606_37796 = G__37838;
continue;
}
} else {
var G__37839 = seq__37600_37793;
var G__37840 = chunk__37604_37794;
var G__37841 = count__37605_37795;
var G__37842 = (i__37606_37796 + (1));
seq__37600_37793 = G__37839;
chunk__37604_37794 = G__37840;
count__37605_37795 = G__37841;
i__37606_37796 = G__37842;
continue;
}
} else {
var temp__5804__auto___37843 = cljs.core.seq(seq__37600_37793);
if(temp__5804__auto___37843){
var seq__37600_37844__$1 = temp__5804__auto___37843;
if(cljs.core.chunked_seq_QMARK_(seq__37600_37844__$1)){
var c__5525__auto___37845 = cljs.core.chunk_first(seq__37600_37844__$1);
var G__37846 = cljs.core.chunk_rest(seq__37600_37844__$1);
var G__37847 = c__5525__auto___37845;
var G__37848 = cljs.core.count(c__5525__auto___37845);
var G__37849 = (0);
seq__37600_37793 = G__37846;
chunk__37604_37794 = G__37847;
count__37605_37795 = G__37848;
i__37606_37796 = G__37849;
continue;
} else {
var node_37850 = cljs.core.first(seq__37600_37844__$1);
if(cljs.core.not(node_37850.shadow$old)){
var path_match_37851 = shadow.cljs.devtools.client.browser.match_paths(node_37850.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37851)){
var new_link_37852 = (function (){var G__37643 = node_37850.cloneNode(true);
G__37643.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37851),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37643;
})();
(node_37850.shadow$old = true);

(new_link_37852.onload = ((function (seq__37600_37793,chunk__37604_37794,count__37605_37795,i__37606_37796,seq__37486,chunk__37488,count__37489,i__37490,new_link_37852,path_match_37851,node_37850,seq__37600_37844__$1,temp__5804__auto___37843,path,map__37485,map__37485__$1,msg,updates,reload_info){
return (function (e){
var seq__37644_37853 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__37646_37854 = null;
var count__37647_37855 = (0);
var i__37648_37856 = (0);
while(true){
if((i__37648_37856 < count__37647_37855)){
var map__37652_37857 = chunk__37646_37854.cljs$core$IIndexed$_nth$arity$2(null,i__37648_37856);
var map__37652_37858__$1 = cljs.core.__destructure_map(map__37652_37857);
var task_37859 = map__37652_37858__$1;
var fn_str_37860 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37652_37858__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37861 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37652_37858__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37862 = goog.getObjectByName(fn_str_37860,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37861)].join(''));

(fn_obj_37862.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37862.cljs$core$IFn$_invoke$arity$2(path,new_link_37852) : fn_obj_37862.call(null,path,new_link_37852));


var G__37863 = seq__37644_37853;
var G__37864 = chunk__37646_37854;
var G__37865 = count__37647_37855;
var G__37866 = (i__37648_37856 + (1));
seq__37644_37853 = G__37863;
chunk__37646_37854 = G__37864;
count__37647_37855 = G__37865;
i__37648_37856 = G__37866;
continue;
} else {
var temp__5804__auto___37867__$1 = cljs.core.seq(seq__37644_37853);
if(temp__5804__auto___37867__$1){
var seq__37644_37868__$1 = temp__5804__auto___37867__$1;
if(cljs.core.chunked_seq_QMARK_(seq__37644_37868__$1)){
var c__5525__auto___37869 = cljs.core.chunk_first(seq__37644_37868__$1);
var G__37870 = cljs.core.chunk_rest(seq__37644_37868__$1);
var G__37871 = c__5525__auto___37869;
var G__37872 = cljs.core.count(c__5525__auto___37869);
var G__37873 = (0);
seq__37644_37853 = G__37870;
chunk__37646_37854 = G__37871;
count__37647_37855 = G__37872;
i__37648_37856 = G__37873;
continue;
} else {
var map__37653_37874 = cljs.core.first(seq__37644_37868__$1);
var map__37653_37875__$1 = cljs.core.__destructure_map(map__37653_37874);
var task_37876 = map__37653_37875__$1;
var fn_str_37877 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37653_37875__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37878 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37653_37875__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37879 = goog.getObjectByName(fn_str_37877,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37878)].join(''));

(fn_obj_37879.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37879.cljs$core$IFn$_invoke$arity$2(path,new_link_37852) : fn_obj_37879.call(null,path,new_link_37852));


var G__37880 = cljs.core.next(seq__37644_37868__$1);
var G__37881 = null;
var G__37882 = (0);
var G__37883 = (0);
seq__37644_37853 = G__37880;
chunk__37646_37854 = G__37881;
count__37647_37855 = G__37882;
i__37648_37856 = G__37883;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_37850);
});})(seq__37600_37793,chunk__37604_37794,count__37605_37795,i__37606_37796,seq__37486,chunk__37488,count__37489,i__37490,new_link_37852,path_match_37851,node_37850,seq__37600_37844__$1,temp__5804__auto___37843,path,map__37485,map__37485__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37851], 0));

goog.dom.insertSiblingAfter(new_link_37852,node_37850);


var G__37884 = cljs.core.next(seq__37600_37844__$1);
var G__37885 = null;
var G__37886 = (0);
var G__37887 = (0);
seq__37600_37793 = G__37884;
chunk__37604_37794 = G__37885;
count__37605_37795 = G__37886;
i__37606_37796 = G__37887;
continue;
} else {
var G__37888 = cljs.core.next(seq__37600_37844__$1);
var G__37889 = null;
var G__37890 = (0);
var G__37891 = (0);
seq__37600_37793 = G__37888;
chunk__37604_37794 = G__37889;
count__37605_37795 = G__37890;
i__37606_37796 = G__37891;
continue;
}
} else {
var G__37892 = cljs.core.next(seq__37600_37844__$1);
var G__37893 = null;
var G__37894 = (0);
var G__37895 = (0);
seq__37600_37793 = G__37892;
chunk__37604_37794 = G__37893;
count__37605_37795 = G__37894;
i__37606_37796 = G__37895;
continue;
}
}
} else {
}
}
break;
}


var G__37896 = seq__37486;
var G__37897 = chunk__37488;
var G__37898 = count__37489;
var G__37899 = (i__37490 + (1));
seq__37486 = G__37896;
chunk__37488 = G__37897;
count__37489 = G__37898;
i__37490 = G__37899;
continue;
} else {
var G__37900 = seq__37486;
var G__37901 = chunk__37488;
var G__37902 = count__37489;
var G__37903 = (i__37490 + (1));
seq__37486 = G__37900;
chunk__37488 = G__37901;
count__37489 = G__37902;
i__37490 = G__37903;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__37486);
if(temp__5804__auto__){
var seq__37486__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__37486__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__37486__$1);
var G__37904 = cljs.core.chunk_rest(seq__37486__$1);
var G__37905 = c__5525__auto__;
var G__37906 = cljs.core.count(c__5525__auto__);
var G__37907 = (0);
seq__37486 = G__37904;
chunk__37488 = G__37905;
count__37489 = G__37906;
i__37490 = G__37907;
continue;
} else {
var path = cljs.core.first(seq__37486__$1);
if(clojure.string.ends_with_QMARK_(path,"css")){
var seq__37654_37908 = cljs.core.seq(cljs.core.array_seq.cljs$core$IFn$_invoke$arity$1(document.querySelectorAll("link[rel=\"stylesheet\"]")));
var chunk__37658_37909 = null;
var count__37659_37910 = (0);
var i__37660_37911 = (0);
while(true){
if((i__37660_37911 < count__37659_37910)){
var node_37912 = chunk__37658_37909.cljs$core$IIndexed$_nth$arity$2(null,i__37660_37911);
if(cljs.core.not(node_37912.shadow$old)){
var path_match_37913 = shadow.cljs.devtools.client.browser.match_paths(node_37912.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37913)){
var new_link_37914 = (function (){var G__37686 = node_37912.cloneNode(true);
G__37686.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37913),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37686;
})();
(node_37912.shadow$old = true);

(new_link_37914.onload = ((function (seq__37654_37908,chunk__37658_37909,count__37659_37910,i__37660_37911,seq__37486,chunk__37488,count__37489,i__37490,new_link_37914,path_match_37913,node_37912,path,seq__37486__$1,temp__5804__auto__,map__37485,map__37485__$1,msg,updates,reload_info){
return (function (e){
var seq__37687_37915 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__37689_37916 = null;
var count__37690_37917 = (0);
var i__37691_37918 = (0);
while(true){
if((i__37691_37918 < count__37690_37917)){
var map__37695_37919 = chunk__37689_37916.cljs$core$IIndexed$_nth$arity$2(null,i__37691_37918);
var map__37695_37920__$1 = cljs.core.__destructure_map(map__37695_37919);
var task_37921 = map__37695_37920__$1;
var fn_str_37922 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37695_37920__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37923 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37695_37920__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37924 = goog.getObjectByName(fn_str_37922,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37923)].join(''));

(fn_obj_37924.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37924.cljs$core$IFn$_invoke$arity$2(path,new_link_37914) : fn_obj_37924.call(null,path,new_link_37914));


var G__37925 = seq__37687_37915;
var G__37926 = chunk__37689_37916;
var G__37927 = count__37690_37917;
var G__37928 = (i__37691_37918 + (1));
seq__37687_37915 = G__37925;
chunk__37689_37916 = G__37926;
count__37690_37917 = G__37927;
i__37691_37918 = G__37928;
continue;
} else {
var temp__5804__auto___37929__$1 = cljs.core.seq(seq__37687_37915);
if(temp__5804__auto___37929__$1){
var seq__37687_37930__$1 = temp__5804__auto___37929__$1;
if(cljs.core.chunked_seq_QMARK_(seq__37687_37930__$1)){
var c__5525__auto___37931 = cljs.core.chunk_first(seq__37687_37930__$1);
var G__37932 = cljs.core.chunk_rest(seq__37687_37930__$1);
var G__37933 = c__5525__auto___37931;
var G__37934 = cljs.core.count(c__5525__auto___37931);
var G__37935 = (0);
seq__37687_37915 = G__37932;
chunk__37689_37916 = G__37933;
count__37690_37917 = G__37934;
i__37691_37918 = G__37935;
continue;
} else {
var map__37696_37936 = cljs.core.first(seq__37687_37930__$1);
var map__37696_37937__$1 = cljs.core.__destructure_map(map__37696_37936);
var task_37938 = map__37696_37937__$1;
var fn_str_37939 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37696_37937__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37940 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37696_37937__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37941 = goog.getObjectByName(fn_str_37939,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37940)].join(''));

(fn_obj_37941.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37941.cljs$core$IFn$_invoke$arity$2(path,new_link_37914) : fn_obj_37941.call(null,path,new_link_37914));


var G__37942 = cljs.core.next(seq__37687_37930__$1);
var G__37943 = null;
var G__37944 = (0);
var G__37945 = (0);
seq__37687_37915 = G__37942;
chunk__37689_37916 = G__37943;
count__37690_37917 = G__37944;
i__37691_37918 = G__37945;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_37912);
});})(seq__37654_37908,chunk__37658_37909,count__37659_37910,i__37660_37911,seq__37486,chunk__37488,count__37489,i__37490,new_link_37914,path_match_37913,node_37912,path,seq__37486__$1,temp__5804__auto__,map__37485,map__37485__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37913], 0));

goog.dom.insertSiblingAfter(new_link_37914,node_37912);


var G__37946 = seq__37654_37908;
var G__37947 = chunk__37658_37909;
var G__37948 = count__37659_37910;
var G__37949 = (i__37660_37911 + (1));
seq__37654_37908 = G__37946;
chunk__37658_37909 = G__37947;
count__37659_37910 = G__37948;
i__37660_37911 = G__37949;
continue;
} else {
var G__37950 = seq__37654_37908;
var G__37951 = chunk__37658_37909;
var G__37952 = count__37659_37910;
var G__37953 = (i__37660_37911 + (1));
seq__37654_37908 = G__37950;
chunk__37658_37909 = G__37951;
count__37659_37910 = G__37952;
i__37660_37911 = G__37953;
continue;
}
} else {
var G__37954 = seq__37654_37908;
var G__37955 = chunk__37658_37909;
var G__37956 = count__37659_37910;
var G__37957 = (i__37660_37911 + (1));
seq__37654_37908 = G__37954;
chunk__37658_37909 = G__37955;
count__37659_37910 = G__37956;
i__37660_37911 = G__37957;
continue;
}
} else {
var temp__5804__auto___37958__$1 = cljs.core.seq(seq__37654_37908);
if(temp__5804__auto___37958__$1){
var seq__37654_37959__$1 = temp__5804__auto___37958__$1;
if(cljs.core.chunked_seq_QMARK_(seq__37654_37959__$1)){
var c__5525__auto___37960 = cljs.core.chunk_first(seq__37654_37959__$1);
var G__37961 = cljs.core.chunk_rest(seq__37654_37959__$1);
var G__37962 = c__5525__auto___37960;
var G__37963 = cljs.core.count(c__5525__auto___37960);
var G__37964 = (0);
seq__37654_37908 = G__37961;
chunk__37658_37909 = G__37962;
count__37659_37910 = G__37963;
i__37660_37911 = G__37964;
continue;
} else {
var node_37965 = cljs.core.first(seq__37654_37959__$1);
if(cljs.core.not(node_37965.shadow$old)){
var path_match_37966 = shadow.cljs.devtools.client.browser.match_paths(node_37965.getAttribute("href"),path);
if(cljs.core.truth_(path_match_37966)){
var new_link_37967 = (function (){var G__37697 = node_37965.cloneNode(true);
G__37697.setAttribute("href",[cljs.core.str.cljs$core$IFn$_invoke$arity$1(path_match_37966),"?r=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(cljs.core.rand.cljs$core$IFn$_invoke$arity$0())].join(''));

return G__37697;
})();
(node_37965.shadow$old = true);

(new_link_37967.onload = ((function (seq__37654_37908,chunk__37658_37909,count__37659_37910,i__37660_37911,seq__37486,chunk__37488,count__37489,i__37490,new_link_37967,path_match_37966,node_37965,seq__37654_37959__$1,temp__5804__auto___37958__$1,path,seq__37486__$1,temp__5804__auto__,map__37485,map__37485__$1,msg,updates,reload_info){
return (function (e){
var seq__37698_37968 = cljs.core.seq(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(msg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"reload-info","reload-info",1648088086),new cljs.core.Keyword(null,"asset-load","asset-load",-1925902322)], null)));
var chunk__37700_37969 = null;
var count__37701_37970 = (0);
var i__37702_37971 = (0);
while(true){
if((i__37702_37971 < count__37701_37970)){
var map__37706_37972 = chunk__37700_37969.cljs$core$IIndexed$_nth$arity$2(null,i__37702_37971);
var map__37706_37973__$1 = cljs.core.__destructure_map(map__37706_37972);
var task_37974 = map__37706_37973__$1;
var fn_str_37975 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37706_37973__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37976 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37706_37973__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37977 = goog.getObjectByName(fn_str_37975,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37976)].join(''));

(fn_obj_37977.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37977.cljs$core$IFn$_invoke$arity$2(path,new_link_37967) : fn_obj_37977.call(null,path,new_link_37967));


var G__37978 = seq__37698_37968;
var G__37979 = chunk__37700_37969;
var G__37980 = count__37701_37970;
var G__37981 = (i__37702_37971 + (1));
seq__37698_37968 = G__37978;
chunk__37700_37969 = G__37979;
count__37701_37970 = G__37980;
i__37702_37971 = G__37981;
continue;
} else {
var temp__5804__auto___37982__$2 = cljs.core.seq(seq__37698_37968);
if(temp__5804__auto___37982__$2){
var seq__37698_37983__$1 = temp__5804__auto___37982__$2;
if(cljs.core.chunked_seq_QMARK_(seq__37698_37983__$1)){
var c__5525__auto___37984 = cljs.core.chunk_first(seq__37698_37983__$1);
var G__37985 = cljs.core.chunk_rest(seq__37698_37983__$1);
var G__37986 = c__5525__auto___37984;
var G__37987 = cljs.core.count(c__5525__auto___37984);
var G__37988 = (0);
seq__37698_37968 = G__37985;
chunk__37700_37969 = G__37986;
count__37701_37970 = G__37987;
i__37702_37971 = G__37988;
continue;
} else {
var map__37707_37989 = cljs.core.first(seq__37698_37983__$1);
var map__37707_37990__$1 = cljs.core.__destructure_map(map__37707_37989);
var task_37991 = map__37707_37990__$1;
var fn_str_37992 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37707_37990__$1,new cljs.core.Keyword(null,"fn-str","fn-str",-1348506402));
var fn_sym_37993 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37707_37990__$1,new cljs.core.Keyword(null,"fn-sym","fn-sym",1423988510));
var fn_obj_37994 = goog.getObjectByName(fn_str_37992,$CLJS);
shadow.cljs.devtools.client.browser.devtools_msg(["call ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(fn_sym_37993)].join(''));

(fn_obj_37994.cljs$core$IFn$_invoke$arity$2 ? fn_obj_37994.cljs$core$IFn$_invoke$arity$2(path,new_link_37967) : fn_obj_37994.call(null,path,new_link_37967));


var G__37995 = cljs.core.next(seq__37698_37983__$1);
var G__37996 = null;
var G__37997 = (0);
var G__37998 = (0);
seq__37698_37968 = G__37995;
chunk__37700_37969 = G__37996;
count__37701_37970 = G__37997;
i__37702_37971 = G__37998;
continue;
}
} else {
}
}
break;
}

return goog.dom.removeNode(node_37965);
});})(seq__37654_37908,chunk__37658_37909,count__37659_37910,i__37660_37911,seq__37486,chunk__37488,count__37489,i__37490,new_link_37967,path_match_37966,node_37965,seq__37654_37959__$1,temp__5804__auto___37958__$1,path,seq__37486__$1,temp__5804__auto__,map__37485,map__37485__$1,msg,updates,reload_info))
);

shadow.cljs.devtools.client.browser.devtools_msg.cljs$core$IFn$_invoke$arity$variadic("load CSS",cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([path_match_37966], 0));

goog.dom.insertSiblingAfter(new_link_37967,node_37965);


var G__37999 = cljs.core.next(seq__37654_37959__$1);
var G__38000 = null;
var G__38001 = (0);
var G__38002 = (0);
seq__37654_37908 = G__37999;
chunk__37658_37909 = G__38000;
count__37659_37910 = G__38001;
i__37660_37911 = G__38002;
continue;
} else {
var G__38003 = cljs.core.next(seq__37654_37959__$1);
var G__38004 = null;
var G__38005 = (0);
var G__38006 = (0);
seq__37654_37908 = G__38003;
chunk__37658_37909 = G__38004;
count__37659_37910 = G__38005;
i__37660_37911 = G__38006;
continue;
}
} else {
var G__38007 = cljs.core.next(seq__37654_37959__$1);
var G__38008 = null;
var G__38009 = (0);
var G__38010 = (0);
seq__37654_37908 = G__38007;
chunk__37658_37909 = G__38008;
count__37659_37910 = G__38009;
i__37660_37911 = G__38010;
continue;
}
}
} else {
}
}
break;
}


var G__38011 = cljs.core.next(seq__37486__$1);
var G__38012 = null;
var G__38013 = (0);
var G__38014 = (0);
seq__37486 = G__38011;
chunk__37488 = G__38012;
count__37489 = G__38013;
i__37490 = G__38014;
continue;
} else {
var G__38015 = cljs.core.next(seq__37486__$1);
var G__38016 = null;
var G__38017 = (0);
var G__38018 = (0);
seq__37486 = G__38015;
chunk__37488 = G__38016;
count__37489 = G__38017;
i__37490 = G__38018;
continue;
}
}
} else {
return null;
}
}
break;
}
});
shadow.cljs.devtools.client.browser.global_eval = (function shadow$cljs$devtools$client$browser$global_eval(js){
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2("undefined",typeof(module))){
return eval(js);
} else {
return (0,eval)(js);;
}
});
shadow.cljs.devtools.client.browser.runtime_info = (((typeof SHADOW_CONFIG !== 'undefined'))?shadow.json.to_clj.cljs$core$IFn$_invoke$arity$1(SHADOW_CONFIG):null);
shadow.cljs.devtools.client.browser.client_info = cljs.core.merge.cljs$core$IFn$_invoke$arity$variadic(cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([shadow.cljs.devtools.client.browser.runtime_info,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"host","host",-1558485167),(cljs.core.truth_(goog.global.document)?new cljs.core.Keyword(null,"browser","browser",828191719):new cljs.core.Keyword(null,"browser-worker","browser-worker",1638998282)),new cljs.core.Keyword(null,"user-agent","user-agent",1220426212),[(cljs.core.truth_(goog.userAgent.OPERA)?"Opera":(cljs.core.truth_(goog.userAgent.product.CHROME)?"Chrome":(cljs.core.truth_(goog.userAgent.IE)?"MSIE":(cljs.core.truth_(goog.userAgent.EDGE)?"Edge":(cljs.core.truth_(goog.userAgent.GECKO)?"Firefox":(cljs.core.truth_(goog.userAgent.SAFARI)?"Safari":(cljs.core.truth_(goog.userAgent.WEBKIT)?"Webkit":null)))))))," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.VERSION)," [",cljs.core.str.cljs$core$IFn$_invoke$arity$1(goog.userAgent.PLATFORM),"]"].join(''),new cljs.core.Keyword(null,"dom","dom",-1236537922),(!((goog.global.document == null)))], null)], 0));
if((typeof shadow !== 'undefined') && (typeof shadow.cljs !== 'undefined') && (typeof shadow.cljs.devtools !== 'undefined') && (typeof shadow.cljs.devtools.client !== 'undefined') && (typeof shadow.cljs.devtools.client.browser !== 'undefined') && (typeof shadow.cljs.devtools.client.browser.ws_was_welcome_ref !== 'undefined')){
} else {
shadow.cljs.devtools.client.browser.ws_was_welcome_ref = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(false);
}
if(((shadow.cljs.devtools.client.env.enabled) && ((shadow.cljs.devtools.client.env.worker_client_id > (0))))){
(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$remote$runtime$api$IEvalJS$_js_eval$arity$2 = (function (this$,code){
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(code);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_invoke$arity$3 = (function (this$,ns,p__37708){
var map__37709 = p__37708;
var map__37709__$1 = cljs.core.__destructure_map(map__37709);
var js = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37709__$1,new cljs.core.Keyword(null,"js","js",1768080579));
var this$__$1 = this;
return shadow.cljs.devtools.client.browser.global_eval(js);
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_init$arity$4 = (function (runtime,p__37710,done,error){
var map__37711 = p__37710;
var map__37711__$1 = cljs.core.__destructure_map(map__37711);
var repl_sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37711__$1,new cljs.core.Keyword(null,"repl-sources","repl-sources",723867535));
var runtime__$1 = this;
return shadow.cljs.devtools.client.shared.load_sources(runtime__$1,cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2(shadow.cljs.devtools.client.env.src_is_loaded_QMARK_,repl_sources)),(function (sources){
shadow.cljs.devtools.client.browser.do_js_load(sources);

return (done.cljs$core$IFn$_invoke$arity$0 ? done.cljs$core$IFn$_invoke$arity$0() : done.call(null));
}));
}));

(shadow.cljs.devtools.client.shared.Runtime.prototype.shadow$cljs$devtools$client$shared$IHostSpecific$do_repl_require$arity$4 = (function (runtime,p__37712,done,error){
var map__37713 = p__37712;
var map__37713__$1 = cljs.core.__destructure_map(map__37713);
var msg = map__37713__$1;
var sources = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37713__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
var reload_namespaces = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37713__$1,new cljs.core.Keyword(null,"reload-namespaces","reload-namespaces",250210134));
var js_requires = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37713__$1,new cljs.core.Keyword(null,"js-requires","js-requires",-1311472051));
var runtime__$1 = this;
var sources_to_load = cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentVector.EMPTY,cljs.core.remove.cljs$core$IFn$_invoke$arity$2((function (p__37714){
var map__37715 = p__37714;
var map__37715__$1 = cljs.core.__destructure_map(map__37715);
var src = map__37715__$1;
var provides = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37715__$1,new cljs.core.Keyword(null,"provides","provides",-1634397992));
var and__5000__auto__ = shadow.cljs.devtools.client.env.src_is_loaded_QMARK_(src);
if(cljs.core.truth_(and__5000__auto__)){
return cljs.core.not(cljs.core.some(reload_namespaces,provides));
} else {
return and__5000__auto__;
}
}),sources));
if(cljs.core.not(cljs.core.seq(sources_to_load))){
var G__37716 = cljs.core.PersistentVector.EMPTY;
return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(G__37716) : done.call(null,G__37716));
} else {
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3(runtime__$1,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"cljs-load-sources","cljs-load-sources",-1458295962),new cljs.core.Keyword(null,"to","to",192099007),shadow.cljs.devtools.client.env.worker_client_id,new cljs.core.Keyword(null,"sources","sources",-321166424),cljs.core.into.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentVector.EMPTY,cljs.core.map.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"resource-id","resource-id",-1308422582)),sources_to_load)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"cljs-sources","cljs-sources",31121610),(function (p__37717){
var map__37718 = p__37717;
var map__37718__$1 = cljs.core.__destructure_map(map__37718);
var msg__$1 = map__37718__$1;
var sources__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37718__$1,new cljs.core.Keyword(null,"sources","sources",-321166424));
try{shadow.cljs.devtools.client.browser.do_js_load(sources__$1);

if(cljs.core.seq(js_requires)){
shadow.cljs.devtools.client.browser.do_js_requires(js_requires);
} else {
}

return (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(sources_to_load) : done.call(null,sources_to_load));
}catch (e37719){var ex = e37719;
return (error.cljs$core$IFn$_invoke$arity$1 ? error.cljs$core$IFn$_invoke$arity$1(ex) : error.call(null,ex));
}})], null));
}
}));

shadow.cljs.devtools.client.shared.add_plugin_BANG_(new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),cljs.core.PersistentHashSet.EMPTY,(function (p__37720){
var map__37721 = p__37720;
var map__37721__$1 = cljs.core.__destructure_map(map__37721);
var env = map__37721__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37721__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
var svc = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null);
shadow.remote.runtime.api.add_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125),(function (){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,true);

shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.env.patch_goog_BANG_();

return shadow.cljs.devtools.client.browser.devtools_msg(["#",cljs.core.str.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(new cljs.core.Keyword(null,"state-ref","state-ref",2127874952).cljs$core$IFn$_invoke$arity$1(runtime))))," ready!"].join(''));
}),new cljs.core.Keyword(null,"on-disconnect","on-disconnect",-809021814),(function (e){
if(cljs.core.truth_(cljs.core.deref(shadow.cljs.devtools.client.browser.ws_was_welcome_ref))){
shadow.cljs.devtools.client.hud.connection_error("The Websocket connection was closed!");

return cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);
} else {
return null;
}
}),new cljs.core.Keyword(null,"on-reconnect","on-reconnect",1239988702),(function (e){
return shadow.cljs.devtools.client.hud.connection_error("Reconnecting ...");
}),new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"access-denied","access-denied",959449406),(function (msg){
cljs.core.reset_BANG_(shadow.cljs.devtools.client.browser.ws_was_welcome_ref,false);

return shadow.cljs.devtools.client.hud.connection_error(["Stale Output! Your loaded JS was not produced by the running shadow-cljs instance."," Is the watch for this build running?"].join(''));
}),new cljs.core.Keyword(null,"cljs-asset-update","cljs-asset-update",1224093028),(function (msg){
return shadow.cljs.devtools.client.browser.handle_asset_update(msg);
}),new cljs.core.Keyword(null,"cljs-build-configure","cljs-build-configure",-2089891268),(function (msg){
return null;
}),new cljs.core.Keyword(null,"cljs-build-start","cljs-build-start",-725781241),(function (msg){
shadow.cljs.devtools.client.hud.hud_hide();

shadow.cljs.devtools.client.hud.load_start();

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-start","build-start",-959649480)));
}),new cljs.core.Keyword(null,"cljs-build-complete","cljs-build-complete",273626153),(function (msg){
var msg__$1 = shadow.cljs.devtools.client.env.add_warnings_to_info(msg);
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

shadow.cljs.devtools.client.hud.hud_warnings(msg__$1);

shadow.cljs.devtools.client.browser.handle_build_complete(runtime,msg__$1);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg__$1,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-complete","build-complete",-501868472)));
}),new cljs.core.Keyword(null,"cljs-build-failure","cljs-build-failure",1718154990),(function (msg){
shadow.cljs.devtools.client.hud.load_end();

shadow.cljs.devtools.client.hud.hud_error(msg);

return shadow.cljs.devtools.client.env.run_custom_notify_BANG_(cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"build-failure","build-failure",-2107487466)));
}),new cljs.core.Keyword("shadow.cljs.devtools.client.env","worker-notify","shadow.cljs.devtools.client.env/worker-notify",-1456820670),(function (p__37722){
var map__37723 = p__37722;
var map__37723__$1 = cljs.core.__destructure_map(map__37723);
var event_op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37723__$1,new cljs.core.Keyword(null,"event-op","event-op",200358057));
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37723__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-disconnect","client-disconnect",640227957),event_op)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(client_id,shadow.cljs.devtools.client.env.worker_client_id)))){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was stopped!");
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-connect","client-connect",-1113973888),event_op)){
shadow.cljs.devtools.client.hud.connection_error_clear_BANG_();

return shadow.cljs.devtools.client.hud.connection_error("The watch for this build was restarted. Reload required!");
} else {
return null;
}
}
})], null)], null));

return svc;
}),(function (p__37724){
var map__37725 = p__37724;
var map__37725__$1 = cljs.core.__destructure_map(map__37725);
var svc = map__37725__$1;
var runtime = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__37725__$1,new cljs.core.Keyword(null,"runtime","runtime",-1331573996));
return shadow.remote.runtime.api.del_extension(runtime,new cljs.core.Keyword("shadow.cljs.devtools.client.browser","client","shadow.cljs.devtools.client.browser/client",-1461019282));
}));

shadow.cljs.devtools.client.shared.init_runtime_BANG_(shadow.cljs.devtools.client.browser.client_info,shadow.cljs.devtools.client.websocket.start,shadow.cljs.devtools.client.websocket.send,shadow.cljs.devtools.client.websocket.stop);
} else {
}

//# sourceMappingURL=shadow.cljs.devtools.client.browser.js.map
