goog.provide('shadow.dom');
shadow.dom.transition_supported_QMARK_ = (((typeof window !== 'undefined'))?goog.style.transition.isSupported():null);

/**
 * @interface
 */
shadow.dom.IElement = function(){};

var shadow$dom$IElement$_to_dom$dyn_35964 = (function (this$){
var x__5350__auto__ = (((this$ == null))?null:this$);
var m__5351__auto__ = (shadow.dom._to_dom[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5351__auto__.call(null,this$));
} else {
var m__5349__auto__ = (shadow.dom._to_dom["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5349__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("IElement.-to-dom",this$);
}
}
});
shadow.dom._to_dom = (function shadow$dom$_to_dom(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$IElement$_to_dom$arity$1 == null)))))){
return this$.shadow$dom$IElement$_to_dom$arity$1(this$);
} else {
return shadow$dom$IElement$_to_dom$dyn_35964(this$);
}
});


/**
 * @interface
 */
shadow.dom.SVGElement = function(){};

var shadow$dom$SVGElement$_to_svg$dyn_35970 = (function (this$){
var x__5350__auto__ = (((this$ == null))?null:this$);
var m__5351__auto__ = (shadow.dom._to_svg[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5351__auto__.call(null,this$));
} else {
var m__5349__auto__ = (shadow.dom._to_svg["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(this$) : m__5349__auto__.call(null,this$));
} else {
throw cljs.core.missing_protocol("SVGElement.-to-svg",this$);
}
}
});
shadow.dom._to_svg = (function shadow$dom$_to_svg(this$){
if((((!((this$ == null)))) && ((!((this$.shadow$dom$SVGElement$_to_svg$arity$1 == null)))))){
return this$.shadow$dom$SVGElement$_to_svg$arity$1(this$);
} else {
return shadow$dom$SVGElement$_to_svg$dyn_35970(this$);
}
});

shadow.dom.lazy_native_coll_seq = (function shadow$dom$lazy_native_coll_seq(coll,idx){
if((idx < coll.length)){
return (new cljs.core.LazySeq(null,(function (){
return cljs.core.cons((coll[idx]),(function (){var G__34221 = coll;
var G__34222 = (idx + (1));
return (shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2 ? shadow.dom.lazy_native_coll_seq.cljs$core$IFn$_invoke$arity$2(G__34221,G__34222) : shadow.dom.lazy_native_coll_seq.call(null,G__34221,G__34222));
})());
}),null,null));
} else {
return null;
}
});

/**
* @constructor
 * @implements {cljs.core.IIndexed}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IDeref}
 * @implements {shadow.dom.IElement}
*/
shadow.dom.NativeColl = (function (coll){
this.coll = coll;
this.cljs$lang$protocol_mask$partition0$ = 8421394;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(shadow.dom.NativeColl.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$2 = (function (this$,n){
var self__ = this;
var this$__$1 = this;
return (self__.coll[n]);
}));

(shadow.dom.NativeColl.prototype.cljs$core$IIndexed$_nth$arity$3 = (function (this$,n,not_found){
var self__ = this;
var this$__$1 = this;
var or__5002__auto__ = (self__.coll[n]);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return not_found;
}
}));

(shadow.dom.NativeColl.prototype.cljs$core$ICounted$_count$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll.length;
}));

(shadow.dom.NativeColl.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return shadow.dom.lazy_native_coll_seq(self__.coll,(0));
}));

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(shadow.dom.NativeColl.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
return self__.coll;
}));

(shadow.dom.NativeColl.getBasis = (function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"coll","coll",-1006698606,null)], null);
}));

(shadow.dom.NativeColl.cljs$lang$type = true);

(shadow.dom.NativeColl.cljs$lang$ctorStr = "shadow.dom/NativeColl");

(shadow.dom.NativeColl.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"shadow.dom/NativeColl");
}));

/**
 * Positional factory function for shadow.dom/NativeColl.
 */
shadow.dom.__GT_NativeColl = (function shadow$dom$__GT_NativeColl(coll){
return (new shadow.dom.NativeColl(coll));
});

shadow.dom.native_coll = (function shadow$dom$native_coll(coll){
return (new shadow.dom.NativeColl(coll));
});
shadow.dom.dom_node = (function shadow$dom$dom_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$IElement$))))?true:false):false)){
return el.shadow$dom$IElement$_to_dom$arity$1(null);
} else {
if(typeof el === 'string'){
return document.createTextNode(el);
} else {
if(typeof el === 'number'){
return document.createTextNode(cljs.core.str.cljs$core$IFn$_invoke$arity$1(el));
} else {
return el;

}
}
}
}
});
shadow.dom.query_one = (function shadow$dom$query_one(var_args){
var G__34324 = arguments.length;
switch (G__34324) {
case 1:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return document.querySelector(sel);
}));

(shadow.dom.query_one.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return shadow.dom.dom_node(root).querySelector(sel);
}));

(shadow.dom.query_one.cljs$lang$maxFixedArity = 2);

shadow.dom.query = (function shadow$dom$query(var_args){
var G__34349 = arguments.length;
switch (G__34349) {
case 1:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.query.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.query.cljs$core$IFn$_invoke$arity$1 = (function (sel){
return (new shadow.dom.NativeColl(document.querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$core$IFn$_invoke$arity$2 = (function (sel,root){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(root).querySelectorAll(sel)));
}));

(shadow.dom.query.cljs$lang$maxFixedArity = 2);

shadow.dom.by_id = (function shadow$dom$by_id(var_args){
var G__34385 = arguments.length;
switch (G__34385) {
case 2:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$2 = (function (id,el){
return shadow.dom.dom_node(el).getElementById(id);
}));

(shadow.dom.by_id.cljs$core$IFn$_invoke$arity$1 = (function (id){
return document.getElementById(id);
}));

(shadow.dom.by_id.cljs$lang$maxFixedArity = 2);

shadow.dom.build = shadow.dom.dom_node;
shadow.dom.ev_stop = (function shadow$dom$ev_stop(var_args){
var G__34407 = arguments.length;
switch (G__34407) {
case 1:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1 = (function (e){
if(cljs.core.truth_(e.stopPropagation)){
e.stopPropagation();

e.preventDefault();
} else {
(e.cancelBubble = true);

(e.returnValue = false);
}

return e;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$2 = (function (e,el){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$4 = (function (e,el,scope,owner){
shadow.dom.ev_stop.cljs$core$IFn$_invoke$arity$1(e);

return el;
}));

(shadow.dom.ev_stop.cljs$lang$maxFixedArity = 4);

/**
 * check wether a parent node (or the document) contains the child
 */
shadow.dom.contains_QMARK_ = (function shadow$dom$contains_QMARK_(var_args){
var G__34423 = arguments.length;
switch (G__34423) {
case 1:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$1 = (function (el){
return goog.dom.contains(document,shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$core$IFn$_invoke$arity$2 = (function (parent,el){
return goog.dom.contains(shadow.dom.dom_node(parent),shadow.dom.dom_node(el));
}));

(shadow.dom.contains_QMARK_.cljs$lang$maxFixedArity = 2);

shadow.dom.add_class = (function shadow$dom$add_class(el,cls){
return goog.dom.classlist.add(shadow.dom.dom_node(el),cls);
});
shadow.dom.remove_class = (function shadow$dom$remove_class(el,cls){
return goog.dom.classlist.remove(shadow.dom.dom_node(el),cls);
});
shadow.dom.toggle_class = (function shadow$dom$toggle_class(var_args){
var G__34460 = arguments.length;
switch (G__34460) {
case 2:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$2 = (function (el,cls){
return goog.dom.classlist.toggle(shadow.dom.dom_node(el),cls);
}));

(shadow.dom.toggle_class.cljs$core$IFn$_invoke$arity$3 = (function (el,cls,v){
if(cljs.core.truth_(v)){
return shadow.dom.add_class(el,cls);
} else {
return shadow.dom.remove_class(el,cls);
}
}));

(shadow.dom.toggle_class.cljs$lang$maxFixedArity = 3);

shadow.dom.dom_listen = (cljs.core.truth_((function (){var or__5002__auto__ = (!((typeof document !== 'undefined')));
if(or__5002__auto__){
return or__5002__auto__;
} else {
return document.addEventListener;
}
})())?(function shadow$dom$dom_listen_good(el,ev,handler){
return el.addEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_ie(el,ev,handler){
try{return el.attachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),(function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
}));
}catch (e34484){if((e34484 instanceof Object)){
var e = e34484;
return console.log("didnt support attachEvent",el,e);
} else {
throw e34484;

}
}}));
shadow.dom.dom_listen_remove = (cljs.core.truth_((function (){var or__5002__auto__ = (!((typeof document !== 'undefined')));
if(or__5002__auto__){
return or__5002__auto__;
} else {
return document.removeEventListener;
}
})())?(function shadow$dom$dom_listen_remove_good(el,ev,handler){
return el.removeEventListener(ev,handler,false);
}):(function shadow$dom$dom_listen_remove_ie(el,ev,handler){
return el.detachEvent(["on",cljs.core.str.cljs$core$IFn$_invoke$arity$1(ev)].join(''),handler);
}));
shadow.dom.on_query = (function shadow$dom$on_query(root_el,ev,selector,handler){
var seq__34512 = cljs.core.seq(shadow.dom.query.cljs$core$IFn$_invoke$arity$2(selector,root_el));
var chunk__34513 = null;
var count__34514 = (0);
var i__34515 = (0);
while(true){
if((i__34515 < count__34514)){
var el = chunk__34513.cljs$core$IIndexed$_nth$arity$2(null,i__34515);
var handler_35995__$1 = ((function (seq__34512,chunk__34513,count__34514,i__34515,el){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__34512,chunk__34513,count__34514,i__34515,el))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_35995__$1);


var G__35996 = seq__34512;
var G__35997 = chunk__34513;
var G__35998 = count__34514;
var G__35999 = (i__34515 + (1));
seq__34512 = G__35996;
chunk__34513 = G__35997;
count__34514 = G__35998;
i__34515 = G__35999;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__34512);
if(temp__5804__auto__){
var seq__34512__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34512__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__34512__$1);
var G__36000 = cljs.core.chunk_rest(seq__34512__$1);
var G__36001 = c__5525__auto__;
var G__36002 = cljs.core.count(c__5525__auto__);
var G__36003 = (0);
seq__34512 = G__36000;
chunk__34513 = G__36001;
count__34514 = G__36002;
i__34515 = G__36003;
continue;
} else {
var el = cljs.core.first(seq__34512__$1);
var handler_36004__$1 = ((function (seq__34512,chunk__34513,count__34514,i__34515,el,seq__34512__$1,temp__5804__auto__){
return (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});})(seq__34512,chunk__34513,count__34514,i__34515,el,seq__34512__$1,temp__5804__auto__))
;
shadow.dom.dom_listen(el,cljs.core.name(ev),handler_36004__$1);


var G__36005 = cljs.core.next(seq__34512__$1);
var G__36006 = null;
var G__36007 = (0);
var G__36008 = (0);
seq__34512 = G__36005;
chunk__34513 = G__36006;
count__34514 = G__36007;
i__34515 = G__36008;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.on = (function shadow$dom$on(var_args){
var G__34620 = arguments.length;
switch (G__34620) {
case 3:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.on.cljs$core$IFn$_invoke$arity$3 = (function (el,ev,handler){
return shadow.dom.on.cljs$core$IFn$_invoke$arity$4(el,ev,handler,false);
}));

(shadow.dom.on.cljs$core$IFn$_invoke$arity$4 = (function (el,ev,handler,capture){
if(cljs.core.vector_QMARK_(ev)){
return shadow.dom.on_query(el,cljs.core.first(ev),cljs.core.second(ev),handler);
} else {
var handler__$1 = (function (e){
return (handler.cljs$core$IFn$_invoke$arity$2 ? handler.cljs$core$IFn$_invoke$arity$2(e,el) : handler.call(null,e,el));
});
return shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(ev),handler__$1);
}
}));

(shadow.dom.on.cljs$lang$maxFixedArity = 4);

shadow.dom.remove_event_handler = (function shadow$dom$remove_event_handler(el,ev,handler){
return shadow.dom.dom_listen_remove(shadow.dom.dom_node(el),cljs.core.name(ev),handler);
});
shadow.dom.add_event_listeners = (function shadow$dom$add_event_listeners(el,events){
var seq__34664 = cljs.core.seq(events);
var chunk__34665 = null;
var count__34666 = (0);
var i__34667 = (0);
while(true){
if((i__34667 < count__34666)){
var vec__34690 = chunk__34665.cljs$core$IIndexed$_nth$arity$2(null,i__34667);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34690,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34690,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__36011 = seq__34664;
var G__36012 = chunk__34665;
var G__36013 = count__34666;
var G__36014 = (i__34667 + (1));
seq__34664 = G__36011;
chunk__34665 = G__36012;
count__34666 = G__36013;
i__34667 = G__36014;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__34664);
if(temp__5804__auto__){
var seq__34664__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34664__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__34664__$1);
var G__36015 = cljs.core.chunk_rest(seq__34664__$1);
var G__36016 = c__5525__auto__;
var G__36017 = cljs.core.count(c__5525__auto__);
var G__36018 = (0);
seq__34664 = G__36015;
chunk__34665 = G__36016;
count__34666 = G__36017;
i__34667 = G__36018;
continue;
} else {
var vec__34701 = cljs.core.first(seq__34664__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34701,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34701,(1),null);
shadow.dom.on.cljs$core$IFn$_invoke$arity$3(el,k,v);


var G__36019 = cljs.core.next(seq__34664__$1);
var G__36020 = null;
var G__36021 = (0);
var G__36022 = (0);
seq__34664 = G__36019;
chunk__34665 = G__36020;
count__34666 = G__36021;
i__34667 = G__36022;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_style = (function shadow$dom$set_style(el,styles){
var dom = shadow.dom.dom_node(el);
var seq__34711 = cljs.core.seq(styles);
var chunk__34712 = null;
var count__34713 = (0);
var i__34714 = (0);
while(true){
if((i__34714 < count__34713)){
var vec__34741 = chunk__34712.cljs$core$IIndexed$_nth$arity$2(null,i__34714);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34741,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34741,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__36028 = seq__34711;
var G__36029 = chunk__34712;
var G__36030 = count__34713;
var G__36031 = (i__34714 + (1));
seq__34711 = G__36028;
chunk__34712 = G__36029;
count__34713 = G__36030;
i__34714 = G__36031;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__34711);
if(temp__5804__auto__){
var seq__34711__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__34711__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__34711__$1);
var G__36033 = cljs.core.chunk_rest(seq__34711__$1);
var G__36034 = c__5525__auto__;
var G__36035 = cljs.core.count(c__5525__auto__);
var G__36036 = (0);
seq__34711 = G__36033;
chunk__34712 = G__36034;
count__34713 = G__36035;
i__34714 = G__36036;
continue;
} else {
var vec__34752 = cljs.core.first(seq__34711__$1);
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34752,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34752,(1),null);
goog.style.setStyle(dom,cljs.core.name(k),(((v == null))?"":v));


var G__36037 = cljs.core.next(seq__34711__$1);
var G__36038 = null;
var G__36039 = (0);
var G__36040 = (0);
seq__34711 = G__36037;
chunk__34712 = G__36038;
count__34713 = G__36039;
i__34714 = G__36040;
continue;
}
} else {
return null;
}
}
break;
}
});
shadow.dom.set_attr_STAR_ = (function shadow$dom$set_attr_STAR_(el,key,value){
var G__34769_36045 = key;
var G__34769_36046__$1 = (((G__34769_36045 instanceof cljs.core.Keyword))?G__34769_36045.fqn:null);
switch (G__34769_36046__$1) {
case "id":
(el.id = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "class":
(el.className = cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));

break;
case "for":
(el.htmlFor = value);

break;
case "cellpadding":
el.setAttribute("cellPadding",value);

break;
case "cellspacing":
el.setAttribute("cellSpacing",value);

break;
case "colspan":
el.setAttribute("colSpan",value);

break;
case "frameborder":
el.setAttribute("frameBorder",value);

break;
case "height":
el.setAttribute("height",value);

break;
case "maxlength":
el.setAttribute("maxLength",value);

break;
case "role":
el.setAttribute("role",value);

break;
case "rowspan":
el.setAttribute("rowSpan",value);

break;
case "type":
el.setAttribute("type",value);

break;
case "usemap":
el.setAttribute("useMap",value);

break;
case "valign":
el.setAttribute("vAlign",value);

break;
case "width":
el.setAttribute("width",value);

break;
case "on":
shadow.dom.add_event_listeners(el,value);

break;
case "style":
if((value == null)){
} else {
if(typeof value === 'string'){
el.setAttribute("style",value);
} else {
if(cljs.core.map_QMARK_(value)){
shadow.dom.set_style(el,value);
} else {
goog.style.setStyle(el,value);

}
}
}

break;
default:
var ks_36072 = cljs.core.name(key);
if(cljs.core.truth_((function (){var or__5002__auto__ = goog.string.startsWith(ks_36072,"data-");
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return goog.string.startsWith(ks_36072,"aria-");
}
})())){
el.setAttribute(ks_36072,value);
} else {
(el[ks_36072] = value);
}

}

return el;
});
shadow.dom.set_attrs = (function shadow$dom$set_attrs(el,attrs){
return cljs.core.reduce_kv((function (el__$1,key,value){
shadow.dom.set_attr_STAR_(el__$1,key,value);

return el__$1;
}),shadow.dom.dom_node(el),attrs);
});
shadow.dom.set_attr = (function shadow$dom$set_attr(el,key,value){
return shadow.dom.set_attr_STAR_(shadow.dom.dom_node(el),key,value);
});
shadow.dom.has_class_QMARK_ = (function shadow$dom$has_class_QMARK_(el,cls){
return goog.dom.classlist.contains(shadow.dom.dom_node(el),cls);
});
shadow.dom.merge_class_string = (function shadow$dom$merge_class_string(current,extra_class){
if(cljs.core.seq(current)){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(current)," ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(extra_class)].join('');
} else {
return extra_class;
}
});
shadow.dom.parse_tag = (function shadow$dom$parse_tag(spec){
var spec__$1 = cljs.core.name(spec);
var fdot = spec__$1.indexOf(".");
var fhash = spec__$1.indexOf("#");
if(((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)))){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1,null,null], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fhash)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fdot),null,clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2((-1),fdot)){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1))),null], null);
} else {
if((fhash > fdot)){
throw ["cant have id after class?",spec__$1].join('');
} else {
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [spec__$1.substring((0),fhash),spec__$1.substring((fhash + (1)),fdot),clojure.string.replace(spec__$1.substring((fdot + (1))),/\./," ")], null);

}
}
}
}
});
shadow.dom.create_dom_node = (function shadow$dom$create_dom_node(tag_def,p__34813){
var map__34815 = p__34813;
var map__34815__$1 = cljs.core.__destructure_map(map__34815);
var props = map__34815__$1;
var class$ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__34815__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
var tag_props = ({});
var vec__34818 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34818,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34818,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34818,(2),null);
if(cljs.core.truth_(tag_id)){
(tag_props["id"] = tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
(tag_props["class"] = shadow.dom.merge_class_string(class$,tag_classes));
} else {
}

var G__34822 = goog.dom.createDom(tag_name,tag_props);
shadow.dom.set_attrs(G__34822,cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(props,new cljs.core.Keyword(null,"class","class",-2030961996)));

return G__34822;
});
shadow.dom.append = (function shadow$dom$append(var_args){
var G__34829 = arguments.length;
switch (G__34829) {
case 1:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.append.cljs$core$IFn$_invoke$arity$1 = (function (node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
document.body.appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$core$IFn$_invoke$arity$2 = (function (el,node){
if(cljs.core.truth_(node)){
var temp__5804__auto__ = shadow.dom.dom_node(node);
if(cljs.core.truth_(temp__5804__auto__)){
var n = temp__5804__auto__;
shadow.dom.dom_node(el).appendChild(n);

return n;
} else {
return null;
}
} else {
return null;
}
}));

(shadow.dom.append.cljs$lang$maxFixedArity = 2);

shadow.dom.destructure_node = (function shadow$dom$destructure_node(create_fn,p__34844){
var vec__34847 = p__34844;
var seq__34848 = cljs.core.seq(vec__34847);
var first__34849 = cljs.core.first(seq__34848);
var seq__34848__$1 = cljs.core.next(seq__34848);
var nn = first__34849;
var first__34849__$1 = cljs.core.first(seq__34848__$1);
var seq__34848__$2 = cljs.core.next(seq__34848__$1);
var np = first__34849__$1;
var nc = seq__34848__$2;
var node = vec__34847;
if((nn instanceof cljs.core.Keyword)){
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("invalid dom node",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"node","node",581201198),node], null));
}

if((((np == null)) && ((nc == null)))){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__34857 = nn;
var G__34858 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__34857,G__34858) : create_fn.call(null,G__34857,G__34858));
})(),cljs.core.List.EMPTY], null);
} else {
if(cljs.core.map_QMARK_(np)){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(nn,np) : create_fn.call(null,nn,np)),nc], null);
} else {
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){var G__34864 = nn;
var G__34865 = cljs.core.PersistentArrayMap.EMPTY;
return (create_fn.cljs$core$IFn$_invoke$arity$2 ? create_fn.cljs$core$IFn$_invoke$arity$2(G__34864,G__34865) : create_fn.call(null,G__34864,G__34865));
})(),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(nc,np)], null);

}
}
});
shadow.dom.make_dom_node = (function shadow$dom$make_dom_node(structure){
var vec__34872 = shadow.dom.destructure_node(shadow.dom.create_dom_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34872,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34872,(1),null);
var seq__34879_36081 = cljs.core.seq(node_children);
var chunk__34880_36082 = null;
var count__34881_36083 = (0);
var i__34882_36084 = (0);
while(true){
if((i__34882_36084 < count__34881_36083)){
var child_struct_36085 = chunk__34880_36082.cljs$core$IIndexed$_nth$arity$2(null,i__34882_36084);
var children_36086 = shadow.dom.dom_node(child_struct_36085);
if(cljs.core.seq_QMARK_(children_36086)){
var seq__34943_36087 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_36086));
var chunk__34945_36088 = null;
var count__34946_36089 = (0);
var i__34947_36090 = (0);
while(true){
if((i__34947_36090 < count__34946_36089)){
var child_36091 = chunk__34945_36088.cljs$core$IIndexed$_nth$arity$2(null,i__34947_36090);
if(cljs.core.truth_(child_36091)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_36091);


var G__36092 = seq__34943_36087;
var G__36093 = chunk__34945_36088;
var G__36094 = count__34946_36089;
var G__36095 = (i__34947_36090 + (1));
seq__34943_36087 = G__36092;
chunk__34945_36088 = G__36093;
count__34946_36089 = G__36094;
i__34947_36090 = G__36095;
continue;
} else {
var G__36096 = seq__34943_36087;
var G__36097 = chunk__34945_36088;
var G__36098 = count__34946_36089;
var G__36099 = (i__34947_36090 + (1));
seq__34943_36087 = G__36096;
chunk__34945_36088 = G__36097;
count__34946_36089 = G__36098;
i__34947_36090 = G__36099;
continue;
}
} else {
var temp__5804__auto___36100 = cljs.core.seq(seq__34943_36087);
if(temp__5804__auto___36100){
var seq__34943_36101__$1 = temp__5804__auto___36100;
if(cljs.core.chunked_seq_QMARK_(seq__34943_36101__$1)){
var c__5525__auto___36102 = cljs.core.chunk_first(seq__34943_36101__$1);
var G__36103 = cljs.core.chunk_rest(seq__34943_36101__$1);
var G__36104 = c__5525__auto___36102;
var G__36105 = cljs.core.count(c__5525__auto___36102);
var G__36106 = (0);
seq__34943_36087 = G__36103;
chunk__34945_36088 = G__36104;
count__34946_36089 = G__36105;
i__34947_36090 = G__36106;
continue;
} else {
var child_36107 = cljs.core.first(seq__34943_36101__$1);
if(cljs.core.truth_(child_36107)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_36107);


var G__36109 = cljs.core.next(seq__34943_36101__$1);
var G__36110 = null;
var G__36111 = (0);
var G__36112 = (0);
seq__34943_36087 = G__36109;
chunk__34945_36088 = G__36110;
count__34946_36089 = G__36111;
i__34947_36090 = G__36112;
continue;
} else {
var G__36113 = cljs.core.next(seq__34943_36101__$1);
var G__36114 = null;
var G__36115 = (0);
var G__36116 = (0);
seq__34943_36087 = G__36113;
chunk__34945_36088 = G__36114;
count__34946_36089 = G__36115;
i__34947_36090 = G__36116;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_36086);
}


var G__36117 = seq__34879_36081;
var G__36118 = chunk__34880_36082;
var G__36119 = count__34881_36083;
var G__36120 = (i__34882_36084 + (1));
seq__34879_36081 = G__36117;
chunk__34880_36082 = G__36118;
count__34881_36083 = G__36119;
i__34882_36084 = G__36120;
continue;
} else {
var temp__5804__auto___36122 = cljs.core.seq(seq__34879_36081);
if(temp__5804__auto___36122){
var seq__34879_36123__$1 = temp__5804__auto___36122;
if(cljs.core.chunked_seq_QMARK_(seq__34879_36123__$1)){
var c__5525__auto___36124 = cljs.core.chunk_first(seq__34879_36123__$1);
var G__36125 = cljs.core.chunk_rest(seq__34879_36123__$1);
var G__36126 = c__5525__auto___36124;
var G__36127 = cljs.core.count(c__5525__auto___36124);
var G__36128 = (0);
seq__34879_36081 = G__36125;
chunk__34880_36082 = G__36126;
count__34881_36083 = G__36127;
i__34882_36084 = G__36128;
continue;
} else {
var child_struct_36129 = cljs.core.first(seq__34879_36123__$1);
var children_36130 = shadow.dom.dom_node(child_struct_36129);
if(cljs.core.seq_QMARK_(children_36130)){
var seq__34961_36131 = cljs.core.seq(cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom.dom_node,children_36130));
var chunk__34963_36132 = null;
var count__34964_36133 = (0);
var i__34965_36134 = (0);
while(true){
if((i__34965_36134 < count__34964_36133)){
var child_36137 = chunk__34963_36132.cljs$core$IIndexed$_nth$arity$2(null,i__34965_36134);
if(cljs.core.truth_(child_36137)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_36137);


var G__36138 = seq__34961_36131;
var G__36139 = chunk__34963_36132;
var G__36140 = count__34964_36133;
var G__36141 = (i__34965_36134 + (1));
seq__34961_36131 = G__36138;
chunk__34963_36132 = G__36139;
count__34964_36133 = G__36140;
i__34965_36134 = G__36141;
continue;
} else {
var G__36142 = seq__34961_36131;
var G__36143 = chunk__34963_36132;
var G__36144 = count__34964_36133;
var G__36145 = (i__34965_36134 + (1));
seq__34961_36131 = G__36142;
chunk__34963_36132 = G__36143;
count__34964_36133 = G__36144;
i__34965_36134 = G__36145;
continue;
}
} else {
var temp__5804__auto___36146__$1 = cljs.core.seq(seq__34961_36131);
if(temp__5804__auto___36146__$1){
var seq__34961_36147__$1 = temp__5804__auto___36146__$1;
if(cljs.core.chunked_seq_QMARK_(seq__34961_36147__$1)){
var c__5525__auto___36148 = cljs.core.chunk_first(seq__34961_36147__$1);
var G__36149 = cljs.core.chunk_rest(seq__34961_36147__$1);
var G__36150 = c__5525__auto___36148;
var G__36151 = cljs.core.count(c__5525__auto___36148);
var G__36152 = (0);
seq__34961_36131 = G__36149;
chunk__34963_36132 = G__36150;
count__34964_36133 = G__36151;
i__34965_36134 = G__36152;
continue;
} else {
var child_36153 = cljs.core.first(seq__34961_36147__$1);
if(cljs.core.truth_(child_36153)){
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,child_36153);


var G__36154 = cljs.core.next(seq__34961_36147__$1);
var G__36155 = null;
var G__36156 = (0);
var G__36157 = (0);
seq__34961_36131 = G__36154;
chunk__34963_36132 = G__36155;
count__34964_36133 = G__36156;
i__34965_36134 = G__36157;
continue;
} else {
var G__36161 = cljs.core.next(seq__34961_36147__$1);
var G__36162 = null;
var G__36163 = (0);
var G__36164 = (0);
seq__34961_36131 = G__36161;
chunk__34963_36132 = G__36162;
count__34964_36133 = G__36163;
i__34965_36134 = G__36164;
continue;
}
}
} else {
}
}
break;
}
} else {
shadow.dom.append.cljs$core$IFn$_invoke$arity$2(node,children_36130);
}


var G__36165 = cljs.core.next(seq__34879_36123__$1);
var G__36166 = null;
var G__36167 = (0);
var G__36168 = (0);
seq__34879_36081 = G__36165;
chunk__34880_36082 = G__36166;
count__34881_36083 = G__36167;
i__34882_36084 = G__36168;
continue;
}
} else {
}
}
break;
}

return node;
});
(cljs.core.Keyword.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.Keyword.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$__$1], null));
}));

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_dom_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_dom,this$__$1);
}));
if(cljs.core.truth_(((typeof HTMLElement) != 'undefined'))){
(HTMLElement.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(HTMLElement.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
if(cljs.core.truth_(((typeof DocumentFragment) != 'undefined'))){
(DocumentFragment.prototype.shadow$dom$IElement$ = cljs.core.PROTOCOL_SENTINEL);

(DocumentFragment.prototype.shadow$dom$IElement$_to_dom$arity$1 = (function (this$){
var this$__$1 = this;
return this$__$1;
}));
} else {
}
/**
 * clear node children
 */
shadow.dom.reset = (function shadow$dom$reset(node){
return goog.dom.removeChildren(shadow.dom.dom_node(node));
});
shadow.dom.remove = (function shadow$dom$remove(node){
if((((!((node == null))))?(((((node.cljs$lang$protocol_mask$partition0$ & (8388608))) || ((cljs.core.PROTOCOL_SENTINEL === node.cljs$core$ISeqable$))))?true:false):false)){
var seq__35002 = cljs.core.seq(node);
var chunk__35003 = null;
var count__35004 = (0);
var i__35005 = (0);
while(true){
if((i__35005 < count__35004)){
var n = chunk__35003.cljs$core$IIndexed$_nth$arity$2(null,i__35005);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__36173 = seq__35002;
var G__36174 = chunk__35003;
var G__36175 = count__35004;
var G__36176 = (i__35005 + (1));
seq__35002 = G__36173;
chunk__35003 = G__36174;
count__35004 = G__36175;
i__35005 = G__36176;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__35002);
if(temp__5804__auto__){
var seq__35002__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35002__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__35002__$1);
var G__36177 = cljs.core.chunk_rest(seq__35002__$1);
var G__36178 = c__5525__auto__;
var G__36179 = cljs.core.count(c__5525__auto__);
var G__36180 = (0);
seq__35002 = G__36177;
chunk__35003 = G__36178;
count__35004 = G__36179;
i__35005 = G__36180;
continue;
} else {
var n = cljs.core.first(seq__35002__$1);
(shadow.dom.remove.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.remove.cljs$core$IFn$_invoke$arity$1(n) : shadow.dom.remove.call(null,n));


var G__36181 = cljs.core.next(seq__35002__$1);
var G__36182 = null;
var G__36183 = (0);
var G__36184 = (0);
seq__35002 = G__36181;
chunk__35003 = G__36182;
count__35004 = G__36183;
i__35005 = G__36184;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return goog.dom.removeNode(node);
}
});
shadow.dom.replace_node = (function shadow$dom$replace_node(old,new$){
return goog.dom.replaceNode(shadow.dom.dom_node(new$),shadow.dom.dom_node(old));
});
shadow.dom.text = (function shadow$dom$text(var_args){
var G__35061 = arguments.length;
switch (G__35061) {
case 2:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return shadow.dom.text.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.text.cljs$core$IFn$_invoke$arity$2 = (function (el,new_text){
return (shadow.dom.dom_node(el).innerText = new_text);
}));

(shadow.dom.text.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.dom_node(el).innerText;
}));

(shadow.dom.text.cljs$lang$maxFixedArity = 2);

shadow.dom.check = (function shadow$dom$check(var_args){
var G__35072 = arguments.length;
switch (G__35072) {
case 1:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.check.cljs$core$IFn$_invoke$arity$1 = (function (el){
return shadow.dom.check.cljs$core$IFn$_invoke$arity$2(el,true);
}));

(shadow.dom.check.cljs$core$IFn$_invoke$arity$2 = (function (el,checked){
return (shadow.dom.dom_node(el).checked = checked);
}));

(shadow.dom.check.cljs$lang$maxFixedArity = 2);

shadow.dom.checked_QMARK_ = (function shadow$dom$checked_QMARK_(el){
return shadow.dom.dom_node(el).checked;
});
shadow.dom.form_elements = (function shadow$dom$form_elements(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).elements));
});
shadow.dom.children = (function shadow$dom$children(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).children));
});
shadow.dom.child_nodes = (function shadow$dom$child_nodes(el){
return (new shadow.dom.NativeColl(shadow.dom.dom_node(el).childNodes));
});
shadow.dom.attr = (function shadow$dom$attr(var_args){
var G__35089 = arguments.length;
switch (G__35089) {
case 2:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.attr.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$2 = (function (el,key){
return shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
}));

(shadow.dom.attr.cljs$core$IFn$_invoke$arity$3 = (function (el,key,default$){
var or__5002__auto__ = shadow.dom.dom_node(el).getAttribute(cljs.core.name(key));
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return default$;
}
}));

(shadow.dom.attr.cljs$lang$maxFixedArity = 3);

shadow.dom.del_attr = (function shadow$dom$del_attr(el,key){
return shadow.dom.dom_node(el).removeAttribute(cljs.core.name(key));
});
shadow.dom.data = (function shadow$dom$data(el,key){
return shadow.dom.dom_node(el).getAttribute(["data-",cljs.core.name(key)].join(''));
});
shadow.dom.set_data = (function shadow$dom$set_data(el,key,value){
return shadow.dom.dom_node(el).setAttribute(["data-",cljs.core.name(key)].join(''),cljs.core.str.cljs$core$IFn$_invoke$arity$1(value));
});
shadow.dom.set_html = (function shadow$dom$set_html(node,text){
return (shadow.dom.dom_node(node).innerHTML = text);
});
shadow.dom.get_html = (function shadow$dom$get_html(node){
return shadow.dom.dom_node(node).innerHTML;
});
shadow.dom.fragment = (function shadow$dom$fragment(var_args){
var args__5732__auto__ = [];
var len__5726__auto___36198 = arguments.length;
var i__5727__auto___36199 = (0);
while(true){
if((i__5727__auto___36199 < len__5726__auto___36198)){
args__5732__auto__.push((arguments[i__5727__auto___36199]));

var G__36200 = (i__5727__auto___36199 + (1));
i__5727__auto___36199 = G__36200;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(shadow.dom.fragment.cljs$core$IFn$_invoke$arity$variadic = (function (nodes){
var fragment = document.createDocumentFragment();
var seq__35119_36201 = cljs.core.seq(nodes);
var chunk__35121_36202 = null;
var count__35122_36203 = (0);
var i__35123_36204 = (0);
while(true){
if((i__35123_36204 < count__35122_36203)){
var node_36206 = chunk__35121_36202.cljs$core$IIndexed$_nth$arity$2(null,i__35123_36204);
fragment.appendChild(shadow.dom._to_dom(node_36206));


var G__36208 = seq__35119_36201;
var G__36209 = chunk__35121_36202;
var G__36210 = count__35122_36203;
var G__36211 = (i__35123_36204 + (1));
seq__35119_36201 = G__36208;
chunk__35121_36202 = G__36209;
count__35122_36203 = G__36210;
i__35123_36204 = G__36211;
continue;
} else {
var temp__5804__auto___36212 = cljs.core.seq(seq__35119_36201);
if(temp__5804__auto___36212){
var seq__35119_36213__$1 = temp__5804__auto___36212;
if(cljs.core.chunked_seq_QMARK_(seq__35119_36213__$1)){
var c__5525__auto___36214 = cljs.core.chunk_first(seq__35119_36213__$1);
var G__36219 = cljs.core.chunk_rest(seq__35119_36213__$1);
var G__36220 = c__5525__auto___36214;
var G__36221 = cljs.core.count(c__5525__auto___36214);
var G__36222 = (0);
seq__35119_36201 = G__36219;
chunk__35121_36202 = G__36220;
count__35122_36203 = G__36221;
i__35123_36204 = G__36222;
continue;
} else {
var node_36223 = cljs.core.first(seq__35119_36213__$1);
fragment.appendChild(shadow.dom._to_dom(node_36223));


var G__36224 = cljs.core.next(seq__35119_36213__$1);
var G__36225 = null;
var G__36226 = (0);
var G__36227 = (0);
seq__35119_36201 = G__36224;
chunk__35121_36202 = G__36225;
count__35122_36203 = G__36226;
i__35123_36204 = G__36227;
continue;
}
} else {
}
}
break;
}

return (new shadow.dom.NativeColl(fragment));
}));

(shadow.dom.fragment.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(shadow.dom.fragment.cljs$lang$applyTo = (function (seq35112){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq35112));
}));

/**
 * given a html string, eval all <script> tags and return the html without the scripts
 * don't do this for everything, only content you trust.
 */
shadow.dom.eval_scripts = (function shadow$dom$eval_scripts(s){
var scripts = cljs.core.re_seq(/<script[^>]*?>(.+?)<\/script>/,s);
var seq__35195_36233 = cljs.core.seq(scripts);
var chunk__35196_36234 = null;
var count__35197_36235 = (0);
var i__35198_36236 = (0);
while(true){
if((i__35198_36236 < count__35197_36235)){
var vec__35244_36237 = chunk__35196_36234.cljs$core$IIndexed$_nth$arity$2(null,i__35198_36236);
var script_tag_36238 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35244_36237,(0),null);
var script_body_36239 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35244_36237,(1),null);
eval(script_body_36239);


var G__36240 = seq__35195_36233;
var G__36241 = chunk__35196_36234;
var G__36242 = count__35197_36235;
var G__36243 = (i__35198_36236 + (1));
seq__35195_36233 = G__36240;
chunk__35196_36234 = G__36241;
count__35197_36235 = G__36242;
i__35198_36236 = G__36243;
continue;
} else {
var temp__5804__auto___36245 = cljs.core.seq(seq__35195_36233);
if(temp__5804__auto___36245){
var seq__35195_36246__$1 = temp__5804__auto___36245;
if(cljs.core.chunked_seq_QMARK_(seq__35195_36246__$1)){
var c__5525__auto___36247 = cljs.core.chunk_first(seq__35195_36246__$1);
var G__36248 = cljs.core.chunk_rest(seq__35195_36246__$1);
var G__36249 = c__5525__auto___36247;
var G__36250 = cljs.core.count(c__5525__auto___36247);
var G__36251 = (0);
seq__35195_36233 = G__36248;
chunk__35196_36234 = G__36249;
count__35197_36235 = G__36250;
i__35198_36236 = G__36251;
continue;
} else {
var vec__35252_36252 = cljs.core.first(seq__35195_36246__$1);
var script_tag_36253 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35252_36252,(0),null);
var script_body_36254 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35252_36252,(1),null);
eval(script_body_36254);


var G__36255 = cljs.core.next(seq__35195_36246__$1);
var G__36256 = null;
var G__36257 = (0);
var G__36258 = (0);
seq__35195_36233 = G__36255;
chunk__35196_36234 = G__36256;
count__35197_36235 = G__36257;
i__35198_36236 = G__36258;
continue;
}
} else {
}
}
break;
}

return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (s__$1,p__35259){
var vec__35260 = p__35259;
var script_tag = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35260,(0),null);
var script_body = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35260,(1),null);
return clojure.string.replace(s__$1,script_tag,"");
}),s,scripts);
});
shadow.dom.str__GT_fragment = (function shadow$dom$str__GT_fragment(s){
var el = document.createElement("div");
(el.innerHTML = s);

return (new shadow.dom.NativeColl(goog.dom.childrenToNode_(document,el)));
});
shadow.dom.node_name = (function shadow$dom$node_name(el){
return shadow.dom.dom_node(el).nodeName;
});
shadow.dom.ancestor_by_class = (function shadow$dom$ancestor_by_class(el,cls){
return goog.dom.getAncestorByClass(shadow.dom.dom_node(el),cls);
});
shadow.dom.ancestor_by_tag = (function shadow$dom$ancestor_by_tag(var_args){
var G__35278 = arguments.length;
switch (G__35278) {
case 2:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$2 = (function (el,tag){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag));
}));

(shadow.dom.ancestor_by_tag.cljs$core$IFn$_invoke$arity$3 = (function (el,tag,cls){
return goog.dom.getAncestorByTagNameAndClass(shadow.dom.dom_node(el),cljs.core.name(tag),cljs.core.name(cls));
}));

(shadow.dom.ancestor_by_tag.cljs$lang$maxFixedArity = 3);

shadow.dom.get_value = (function shadow$dom$get_value(dom){
return goog.dom.forms.getValue(shadow.dom.dom_node(dom));
});
shadow.dom.set_value = (function shadow$dom$set_value(dom,value){
return goog.dom.forms.setValue(shadow.dom.dom_node(dom),value);
});
shadow.dom.px = (function shadow$dom$px(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1((value | (0))),"px"].join('');
});
shadow.dom.pct = (function shadow$dom$pct(value){
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(value),"%"].join('');
});
shadow.dom.remove_style_STAR_ = (function shadow$dom$remove_style_STAR_(el,style){
return el.style.removeProperty(cljs.core.name(style));
});
shadow.dom.remove_style = (function shadow$dom$remove_style(el,style){
var el__$1 = shadow.dom.dom_node(el);
return shadow.dom.remove_style_STAR_(el__$1,style);
});
shadow.dom.remove_styles = (function shadow$dom$remove_styles(el,style_keys){
var el__$1 = shadow.dom.dom_node(el);
var seq__35322 = cljs.core.seq(style_keys);
var chunk__35323 = null;
var count__35324 = (0);
var i__35325 = (0);
while(true){
if((i__35325 < count__35324)){
var it = chunk__35323.cljs$core$IIndexed$_nth$arity$2(null,i__35325);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__36262 = seq__35322;
var G__36263 = chunk__35323;
var G__36264 = count__35324;
var G__36265 = (i__35325 + (1));
seq__35322 = G__36262;
chunk__35323 = G__36263;
count__35324 = G__36264;
i__35325 = G__36265;
continue;
} else {
var temp__5804__auto__ = cljs.core.seq(seq__35322);
if(temp__5804__auto__){
var seq__35322__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__35322__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__35322__$1);
var G__36266 = cljs.core.chunk_rest(seq__35322__$1);
var G__36267 = c__5525__auto__;
var G__36268 = cljs.core.count(c__5525__auto__);
var G__36269 = (0);
seq__35322 = G__36266;
chunk__35323 = G__36267;
count__35324 = G__36268;
i__35325 = G__36269;
continue;
} else {
var it = cljs.core.first(seq__35322__$1);
shadow.dom.remove_style_STAR_(el__$1,it);


var G__36270 = cljs.core.next(seq__35322__$1);
var G__36271 = null;
var G__36272 = (0);
var G__36273 = (0);
seq__35322 = G__36270;
chunk__35323 = G__36271;
count__35324 = G__36272;
i__35325 = G__36273;
continue;
}
} else {
return null;
}
}
break;
}
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Coordinate = (function (x,y,__meta,__extmap,__hash){
this.x = x;
this.y = y;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__5300__auto__,k__5301__auto__){
var self__ = this;
var this__5300__auto____$1 = this;
return this__5300__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__5301__auto__,null);
}));

(shadow.dom.Coordinate.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__5302__auto__,k35345,else__5303__auto__){
var self__ = this;
var this__5302__auto____$1 = this;
var G__35373 = k35345;
var G__35373__$1 = (((G__35373 instanceof cljs.core.Keyword))?G__35373.fqn:null);
switch (G__35373__$1) {
case "x":
return self__.x;

break;
case "y":
return self__.y;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k35345,else__5303__auto__);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__5320__auto__,f__5321__auto__,init__5322__auto__){
var self__ = this;
var this__5320__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__5323__auto__,p__35380){
var vec__35382 = p__35380;
var k__5324__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35382,(0),null);
var v__5325__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35382,(1),null);
return (f__5321__auto__.cljs$core$IFn$_invoke$arity$3 ? f__5321__auto__.cljs$core$IFn$_invoke$arity$3(ret__5323__auto__,k__5324__auto__,v__5325__auto__) : f__5321__auto__.call(null,ret__5323__auto__,k__5324__auto__,v__5325__auto__));
}),init__5322__auto__,this__5320__auto____$1);
}));

(shadow.dom.Coordinate.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__5315__auto__,writer__5316__auto__,opts__5317__auto__){
var self__ = this;
var this__5315__auto____$1 = this;
var pr_pair__5318__auto__ = (function (keyval__5319__auto__){
return cljs.core.pr_sequential_writer(writer__5316__auto__,cljs.core.pr_writer,""," ","",opts__5317__auto__,keyval__5319__auto__);
});
return cljs.core.pr_sequential_writer(writer__5316__auto__,pr_pair__5318__auto__,"#shadow.dom.Coordinate{",", ","}",opts__5317__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"x","x",2099068185),self__.x],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"y","y",-1757859776),self__.y],null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__35344){
var self__ = this;
var G__35344__$1 = this;
return (new cljs.core.RecordIter((0),G__35344__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"x","x",2099068185),new cljs.core.Keyword(null,"y","y",-1757859776)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__5298__auto__){
var self__ = this;
var this__5298__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__5295__auto__){
var self__ = this;
var this__5295__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__5304__auto__){
var self__ = this;
var this__5304__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__5296__auto__){
var self__ = this;
var this__5296__auto____$1 = this;
var h__5111__auto__ = self__.__hash;
if((!((h__5111__auto__ == null)))){
return h__5111__auto__;
} else {
var h__5111__auto____$1 = (function (coll__5297__auto__){
return (145542109 ^ cljs.core.hash_unordered_coll(coll__5297__auto__));
})(this__5296__auto____$1);
(self__.__hash = h__5111__auto____$1);

return h__5111__auto____$1;
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this35346,other35347){
var self__ = this;
var this35346__$1 = this;
return (((!((other35347 == null)))) && ((((this35346__$1.constructor === other35347.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35346__$1.x,other35347.x)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35346__$1.y,other35347.y)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35346__$1.__extmap,other35347.__extmap)))))))));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__5310__auto__,k__5311__auto__){
var self__ = this;
var this__5310__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"y","y",-1757859776),null,new cljs.core.Keyword(null,"x","x",2099068185),null], null), null),k__5311__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__5310__auto____$1),self__.__meta),k__5311__auto__);
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__5311__auto__)),null));
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__5307__auto__,k35345){
var self__ = this;
var this__5307__auto____$1 = this;
var G__35433 = k35345;
var G__35433__$1 = (((G__35433 instanceof cljs.core.Keyword))?G__35433.fqn:null);
switch (G__35433__$1) {
case "x":
case "y":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k35345);

}
}));

(shadow.dom.Coordinate.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__5308__auto__,k__5309__auto__,G__35344){
var self__ = this;
var this__5308__auto____$1 = this;
var pred__35442 = cljs.core.keyword_identical_QMARK_;
var expr__35443 = k__5309__auto__;
if(cljs.core.truth_((pred__35442.cljs$core$IFn$_invoke$arity$2 ? pred__35442.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"x","x",2099068185),expr__35443) : pred__35442.call(null,new cljs.core.Keyword(null,"x","x",2099068185),expr__35443)))){
return (new shadow.dom.Coordinate(G__35344,self__.y,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__35442.cljs$core$IFn$_invoke$arity$2 ? pred__35442.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"y","y",-1757859776),expr__35443) : pred__35442.call(null,new cljs.core.Keyword(null,"y","y",-1757859776),expr__35443)))){
return (new shadow.dom.Coordinate(self__.x,G__35344,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Coordinate(self__.x,self__.y,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__5309__auto__,G__35344),null));
}
}
}));

(shadow.dom.Coordinate.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__5313__auto__){
var self__ = this;
var this__5313__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"x","x",2099068185),self__.x,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"y","y",-1757859776),self__.y,null))], null),self__.__extmap));
}));

(shadow.dom.Coordinate.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__5299__auto__,G__35344){
var self__ = this;
var this__5299__auto____$1 = this;
return (new shadow.dom.Coordinate(self__.x,self__.y,G__35344,self__.__extmap,self__.__hash));
}));

(shadow.dom.Coordinate.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__5305__auto__,entry__5306__auto__){
var self__ = this;
var this__5305__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__5306__auto__)){
return this__5305__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__5306__auto__,(0)),cljs.core._nth(entry__5306__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__5305__auto____$1,entry__5306__auto__);
}
}));

(shadow.dom.Coordinate.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"x","x",-555367584,null),new cljs.core.Symbol(null,"y","y",-117328249,null)], null);
}));

(shadow.dom.Coordinate.cljs$lang$type = true);

(shadow.dom.Coordinate.cljs$lang$ctorPrSeq = (function (this__5346__auto__){
return (new cljs.core.List(null,"shadow.dom/Coordinate",null,(1),null));
}));

(shadow.dom.Coordinate.cljs$lang$ctorPrWriter = (function (this__5346__auto__,writer__5347__auto__){
return cljs.core._write(writer__5347__auto__,"shadow.dom/Coordinate");
}));

/**
 * Positional factory function for shadow.dom/Coordinate.
 */
shadow.dom.__GT_Coordinate = (function shadow$dom$__GT_Coordinate(x,y){
return (new shadow.dom.Coordinate(x,y,null,null,null));
});

/**
 * Factory function for shadow.dom/Coordinate, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Coordinate = (function shadow$dom$map__GT_Coordinate(G__35354){
var extmap__5342__auto__ = (function (){var G__35480 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__35354,new cljs.core.Keyword(null,"x","x",2099068185),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"y","y",-1757859776)], 0));
if(cljs.core.record_QMARK_(G__35354)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__35480);
} else {
return G__35480;
}
})();
return (new shadow.dom.Coordinate(new cljs.core.Keyword(null,"x","x",2099068185).cljs$core$IFn$_invoke$arity$1(G__35354),new cljs.core.Keyword(null,"y","y",-1757859776).cljs$core$IFn$_invoke$arity$1(G__35354),null,cljs.core.not_empty(extmap__5342__auto__),null));
});

shadow.dom.get_position = (function shadow$dom$get_position(el){
var pos = goog.style.getPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_client_position = (function shadow$dom$get_client_position(el){
var pos = goog.style.getClientPosition(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});
shadow.dom.get_page_offset = (function shadow$dom$get_page_offset(el){
var pos = goog.style.getPageOffset(shadow.dom.dom_node(el));
return shadow.dom.__GT_Coordinate(pos.x,pos.y);
});

/**
* @constructor
 * @implements {cljs.core.IRecord}
 * @implements {cljs.core.IKVReduce}
 * @implements {cljs.core.IEquiv}
 * @implements {cljs.core.IHash}
 * @implements {cljs.core.ICollection}
 * @implements {cljs.core.ICounted}
 * @implements {cljs.core.ISeqable}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.ICloneable}
 * @implements {cljs.core.IPrintWithWriter}
 * @implements {cljs.core.IIterable}
 * @implements {cljs.core.IWithMeta}
 * @implements {cljs.core.IAssociative}
 * @implements {cljs.core.IMap}
 * @implements {cljs.core.ILookup}
*/
shadow.dom.Size = (function (w,h,__meta,__extmap,__hash){
this.w = w;
this.h = h;
this.__meta = __meta;
this.__extmap = __extmap;
this.__hash = __hash;
this.cljs$lang$protocol_mask$partition0$ = 2230716170;
this.cljs$lang$protocol_mask$partition1$ = 139264;
});
(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$2 = (function (this__5300__auto__,k__5301__auto__){
var self__ = this;
var this__5300__auto____$1 = this;
return this__5300__auto____$1.cljs$core$ILookup$_lookup$arity$3(null,k__5301__auto__,null);
}));

(shadow.dom.Size.prototype.cljs$core$ILookup$_lookup$arity$3 = (function (this__5302__auto__,k35532,else__5303__auto__){
var self__ = this;
var this__5302__auto____$1 = this;
var G__35554 = k35532;
var G__35554__$1 = (((G__35554 instanceof cljs.core.Keyword))?G__35554.fqn:null);
switch (G__35554__$1) {
case "w":
return self__.w;

break;
case "h":
return self__.h;

break;
default:
return cljs.core.get.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k35532,else__5303__auto__);

}
}));

(shadow.dom.Size.prototype.cljs$core$IKVReduce$_kv_reduce$arity$3 = (function (this__5320__auto__,f__5321__auto__,init__5322__auto__){
var self__ = this;
var this__5320__auto____$1 = this;
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3((function (ret__5323__auto__,p__35564){
var vec__35565 = p__35564;
var k__5324__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35565,(0),null);
var v__5325__auto__ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35565,(1),null);
return (f__5321__auto__.cljs$core$IFn$_invoke$arity$3 ? f__5321__auto__.cljs$core$IFn$_invoke$arity$3(ret__5323__auto__,k__5324__auto__,v__5325__auto__) : f__5321__auto__.call(null,ret__5323__auto__,k__5324__auto__,v__5325__auto__));
}),init__5322__auto__,this__5320__auto____$1);
}));

(shadow.dom.Size.prototype.cljs$core$IPrintWithWriter$_pr_writer$arity$3 = (function (this__5315__auto__,writer__5316__auto__,opts__5317__auto__){
var self__ = this;
var this__5315__auto____$1 = this;
var pr_pair__5318__auto__ = (function (keyval__5319__auto__){
return cljs.core.pr_sequential_writer(writer__5316__auto__,cljs.core.pr_writer,""," ","",opts__5317__auto__,keyval__5319__auto__);
});
return cljs.core.pr_sequential_writer(writer__5316__auto__,pr_pair__5318__auto__,"#shadow.dom.Size{",", ","}",opts__5317__auto__,cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"w","w",354169001),self__.w],null)),(new cljs.core.PersistentVector(null,2,(5),cljs.core.PersistentVector.EMPTY_NODE,[new cljs.core.Keyword(null,"h","h",1109658740),self__.h],null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IIterable$_iterator$arity$1 = (function (G__35531){
var self__ = this;
var G__35531__$1 = this;
return (new cljs.core.RecordIter((0),G__35531__$1,2,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"w","w",354169001),new cljs.core.Keyword(null,"h","h",1109658740)], null),(cljs.core.truth_(self__.__extmap)?cljs.core._iterator(self__.__extmap):cljs.core.nil_iter())));
}));

(shadow.dom.Size.prototype.cljs$core$IMeta$_meta$arity$1 = (function (this__5298__auto__){
var self__ = this;
var this__5298__auto____$1 = this;
return self__.__meta;
}));

(shadow.dom.Size.prototype.cljs$core$ICloneable$_clone$arity$1 = (function (this__5295__auto__){
var self__ = this;
var this__5295__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICounted$_count$arity$1 = (function (this__5304__auto__){
var self__ = this;
var this__5304__auto____$1 = this;
return (2 + cljs.core.count(self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IHash$_hash$arity$1 = (function (this__5296__auto__){
var self__ = this;
var this__5296__auto____$1 = this;
var h__5111__auto__ = self__.__hash;
if((!((h__5111__auto__ == null)))){
return h__5111__auto__;
} else {
var h__5111__auto____$1 = (function (coll__5297__auto__){
return (-1228019642 ^ cljs.core.hash_unordered_coll(coll__5297__auto__));
})(this__5296__auto____$1);
(self__.__hash = h__5111__auto____$1);

return h__5111__auto____$1;
}
}));

(shadow.dom.Size.prototype.cljs$core$IEquiv$_equiv$arity$2 = (function (this35533,other35534){
var self__ = this;
var this35533__$1 = this;
return (((!((other35534 == null)))) && ((((this35533__$1.constructor === other35534.constructor)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35533__$1.w,other35534.w)) && (((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35533__$1.h,other35534.h)) && (cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(this35533__$1.__extmap,other35534.__extmap)))))))));
}));

(shadow.dom.Size.prototype.cljs$core$IMap$_dissoc$arity$2 = (function (this__5310__auto__,k__5311__auto__){
var self__ = this;
var this__5310__auto____$1 = this;
if(cljs.core.contains_QMARK_(new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"w","w",354169001),null,new cljs.core.Keyword(null,"h","h",1109658740),null], null), null),k__5311__auto__)){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(cljs.core._with_meta(cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,this__5310__auto____$1),self__.__meta),k__5311__auto__);
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.not_empty(cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(self__.__extmap,k__5311__auto__)),null));
}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_contains_key_QMARK_$arity$2 = (function (this__5307__auto__,k35532){
var self__ = this;
var this__5307__auto____$1 = this;
var G__35613 = k35532;
var G__35613__$1 = (((G__35613 instanceof cljs.core.Keyword))?G__35613.fqn:null);
switch (G__35613__$1) {
case "w":
case "h":
return true;

break;
default:
return cljs.core.contains_QMARK_(self__.__extmap,k35532);

}
}));

(shadow.dom.Size.prototype.cljs$core$IAssociative$_assoc$arity$3 = (function (this__5308__auto__,k__5309__auto__,G__35531){
var self__ = this;
var this__5308__auto____$1 = this;
var pred__35617 = cljs.core.keyword_identical_QMARK_;
var expr__35618 = k__5309__auto__;
if(cljs.core.truth_((pred__35617.cljs$core$IFn$_invoke$arity$2 ? pred__35617.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"w","w",354169001),expr__35618) : pred__35617.call(null,new cljs.core.Keyword(null,"w","w",354169001),expr__35618)))){
return (new shadow.dom.Size(G__35531,self__.h,self__.__meta,self__.__extmap,null));
} else {
if(cljs.core.truth_((pred__35617.cljs$core$IFn$_invoke$arity$2 ? pred__35617.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"h","h",1109658740),expr__35618) : pred__35617.call(null,new cljs.core.Keyword(null,"h","h",1109658740),expr__35618)))){
return (new shadow.dom.Size(self__.w,G__35531,self__.__meta,self__.__extmap,null));
} else {
return (new shadow.dom.Size(self__.w,self__.h,self__.__meta,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(self__.__extmap,k__5309__auto__,G__35531),null));
}
}
}));

(shadow.dom.Size.prototype.cljs$core$ISeqable$_seq$arity$1 = (function (this__5313__auto__){
var self__ = this;
var this__5313__auto____$1 = this;
return cljs.core.seq(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(new cljs.core.MapEntry(new cljs.core.Keyword(null,"w","w",354169001),self__.w,null)),(new cljs.core.MapEntry(new cljs.core.Keyword(null,"h","h",1109658740),self__.h,null))], null),self__.__extmap));
}));

(shadow.dom.Size.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (this__5299__auto__,G__35531){
var self__ = this;
var this__5299__auto____$1 = this;
return (new shadow.dom.Size(self__.w,self__.h,G__35531,self__.__extmap,self__.__hash));
}));

(shadow.dom.Size.prototype.cljs$core$ICollection$_conj$arity$2 = (function (this__5305__auto__,entry__5306__auto__){
var self__ = this;
var this__5305__auto____$1 = this;
if(cljs.core.vector_QMARK_(entry__5306__auto__)){
return this__5305__auto____$1.cljs$core$IAssociative$_assoc$arity$3(null,cljs.core._nth(entry__5306__auto__,(0)),cljs.core._nth(entry__5306__auto__,(1)));
} else {
return cljs.core.reduce.cljs$core$IFn$_invoke$arity$3(cljs.core._conj,this__5305__auto____$1,entry__5306__auto__);
}
}));

(shadow.dom.Size.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"w","w",1994700528,null),new cljs.core.Symbol(null,"h","h",-1544777029,null)], null);
}));

(shadow.dom.Size.cljs$lang$type = true);

(shadow.dom.Size.cljs$lang$ctorPrSeq = (function (this__5346__auto__){
return (new cljs.core.List(null,"shadow.dom/Size",null,(1),null));
}));

(shadow.dom.Size.cljs$lang$ctorPrWriter = (function (this__5346__auto__,writer__5347__auto__){
return cljs.core._write(writer__5347__auto__,"shadow.dom/Size");
}));

/**
 * Positional factory function for shadow.dom/Size.
 */
shadow.dom.__GT_Size = (function shadow$dom$__GT_Size(w,h){
return (new shadow.dom.Size(w,h,null,null,null));
});

/**
 * Factory function for shadow.dom/Size, taking a map of keywords to field values.
 */
shadow.dom.map__GT_Size = (function shadow$dom$map__GT_Size(G__35537){
var extmap__5342__auto__ = (function (){var G__35651 = cljs.core.dissoc.cljs$core$IFn$_invoke$arity$variadic(G__35537,new cljs.core.Keyword(null,"w","w",354169001),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"h","h",1109658740)], 0));
if(cljs.core.record_QMARK_(G__35537)){
return cljs.core.into.cljs$core$IFn$_invoke$arity$2(cljs.core.PersistentArrayMap.EMPTY,G__35651);
} else {
return G__35651;
}
})();
return (new shadow.dom.Size(new cljs.core.Keyword(null,"w","w",354169001).cljs$core$IFn$_invoke$arity$1(G__35537),new cljs.core.Keyword(null,"h","h",1109658740).cljs$core$IFn$_invoke$arity$1(G__35537),null,cljs.core.not_empty(extmap__5342__auto__),null));
});

shadow.dom.size__GT_clj = (function shadow$dom$size__GT_clj(size){
return (new shadow.dom.Size(size.width,size.height,null,null,null));
});
shadow.dom.get_size = (function shadow$dom$get_size(el){
return shadow.dom.size__GT_clj(goog.style.getSize(shadow.dom.dom_node(el)));
});
shadow.dom.get_height = (function shadow$dom$get_height(el){
return shadow.dom.get_size(el).h;
});
shadow.dom.get_viewport_size = (function shadow$dom$get_viewport_size(){
return shadow.dom.size__GT_clj(goog.dom.getViewportSize());
});
shadow.dom.first_child = (function shadow$dom$first_child(el){
return (shadow.dom.dom_node(el).children[(0)]);
});
shadow.dom.select_option_values = (function shadow$dom$select_option_values(el){
var native$ = shadow.dom.dom_node(el);
var opts = (native$["options"]);
var a__5590__auto__ = opts;
var l__5591__auto__ = a__5590__auto__.length;
var i = (0);
var ret = cljs.core.PersistentVector.EMPTY;
while(true){
if((i < l__5591__auto__)){
var G__36330 = (i + (1));
var G__36331 = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,(opts[i]["value"]));
i = G__36330;
ret = G__36331;
continue;
} else {
return ret;
}
break;
}
});
shadow.dom.build_url = (function shadow$dom$build_url(path,query_params){
if(cljs.core.empty_QMARK_(query_params)){
return path;
} else {
return [cljs.core.str.cljs$core$IFn$_invoke$arity$1(path),"?",clojure.string.join.cljs$core$IFn$_invoke$arity$2("&",cljs.core.map.cljs$core$IFn$_invoke$arity$2((function (p__35683){
var vec__35684 = p__35683;
var k = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35684,(0),null);
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35684,(1),null);
return [cljs.core.name(k),"=",cljs.core.str.cljs$core$IFn$_invoke$arity$1(encodeURIComponent(cljs.core.str.cljs$core$IFn$_invoke$arity$1(v)))].join('');
}),query_params))].join('');
}
});
shadow.dom.redirect = (function shadow$dom$redirect(var_args){
var G__35695 = arguments.length;
switch (G__35695) {
case 1:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$1 = (function (path){
return shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2(path,cljs.core.PersistentArrayMap.EMPTY);
}));

(shadow.dom.redirect.cljs$core$IFn$_invoke$arity$2 = (function (path,query_params){
return (document["location"]["href"] = shadow.dom.build_url(path,query_params));
}));

(shadow.dom.redirect.cljs$lang$maxFixedArity = 2);

shadow.dom.reload_BANG_ = (function shadow$dom$reload_BANG_(){
return (document.location.href = document.location.href);
});
shadow.dom.tag_name = (function shadow$dom$tag_name(el){
var dom = shadow.dom.dom_node(el);
return dom.tagName;
});
shadow.dom.insert_after = (function shadow$dom$insert_after(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingAfter(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_before = (function shadow$dom$insert_before(ref,new$){
var new_node = shadow.dom.dom_node(new$);
goog.dom.insertSiblingBefore(new_node,shadow.dom.dom_node(ref));

return new_node;
});
shadow.dom.insert_first = (function shadow$dom$insert_first(ref,new$){
var temp__5802__auto__ = shadow.dom.dom_node(ref).firstChild;
if(cljs.core.truth_(temp__5802__auto__)){
var child = temp__5802__auto__;
return shadow.dom.insert_before(child,new$);
} else {
return shadow.dom.append.cljs$core$IFn$_invoke$arity$2(ref,new$);
}
});
shadow.dom.index_of = (function shadow$dom$index_of(el){
var el__$1 = shadow.dom.dom_node(el);
var i = (0);
while(true){
var ps = el__$1.previousSibling;
if((ps == null)){
return i;
} else {
var G__36340 = ps;
var G__36341 = (i + (1));
el__$1 = G__36340;
i = G__36341;
continue;
}
break;
}
});
shadow.dom.get_parent = (function shadow$dom$get_parent(el){
return goog.dom.getParentElement(shadow.dom.dom_node(el));
});
shadow.dom.parents = (function shadow$dom$parents(el){
var parent = shadow.dom.get_parent(el);
if(cljs.core.truth_(parent)){
return cljs.core.cons(parent,(new cljs.core.LazySeq(null,(function (){
return (shadow.dom.parents.cljs$core$IFn$_invoke$arity$1 ? shadow.dom.parents.cljs$core$IFn$_invoke$arity$1(parent) : shadow.dom.parents.call(null,parent));
}),null,null)));
} else {
return null;
}
});
shadow.dom.matches = (function shadow$dom$matches(el,sel){
return shadow.dom.dom_node(el).matches(sel);
});
shadow.dom.get_next_sibling = (function shadow$dom$get_next_sibling(el){
return goog.dom.getNextElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.get_previous_sibling = (function shadow$dom$get_previous_sibling(el){
return goog.dom.getPreviousElementSibling(shadow.dom.dom_node(el));
});
shadow.dom.xmlns = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 2, ["svg","http://www.w3.org/2000/svg","xlink","http://www.w3.org/1999/xlink"], null));
shadow.dom.create_svg_node = (function shadow$dom$create_svg_node(tag_def,props){
var vec__35760 = shadow.dom.parse_tag(tag_def);
var tag_name = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35760,(0),null);
var tag_id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35760,(1),null);
var tag_classes = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35760,(2),null);
var el = document.createElementNS("http://www.w3.org/2000/svg",tag_name);
if(cljs.core.truth_(tag_id)){
el.setAttribute("id",tag_id);
} else {
}

if(cljs.core.truth_(tag_classes)){
el.setAttribute("class",shadow.dom.merge_class_string(new cljs.core.Keyword(null,"class","class",-2030961996).cljs$core$IFn$_invoke$arity$1(props),tag_classes));
} else {
}

var seq__35765_36349 = cljs.core.seq(props);
var chunk__35769_36350 = null;
var count__35770_36351 = (0);
var i__35771_36352 = (0);
while(true){
if((i__35771_36352 < count__35770_36351)){
var vec__35782_36353 = chunk__35769_36350.cljs$core$IIndexed$_nth$arity$2(null,i__35771_36352);
var k_36354 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35782_36353,(0),null);
var v_36355 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35782_36353,(1),null);
el.setAttributeNS((function (){var temp__5804__auto__ = cljs.core.namespace(k_36354);
if(cljs.core.truth_(temp__5804__auto__)){
var ns = temp__5804__auto__;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_36354),v_36355);


var G__36356 = seq__35765_36349;
var G__36357 = chunk__35769_36350;
var G__36358 = count__35770_36351;
var G__36359 = (i__35771_36352 + (1));
seq__35765_36349 = G__36356;
chunk__35769_36350 = G__36357;
count__35770_36351 = G__36358;
i__35771_36352 = G__36359;
continue;
} else {
var temp__5804__auto___36360 = cljs.core.seq(seq__35765_36349);
if(temp__5804__auto___36360){
var seq__35765_36361__$1 = temp__5804__auto___36360;
if(cljs.core.chunked_seq_QMARK_(seq__35765_36361__$1)){
var c__5525__auto___36362 = cljs.core.chunk_first(seq__35765_36361__$1);
var G__36363 = cljs.core.chunk_rest(seq__35765_36361__$1);
var G__36364 = c__5525__auto___36362;
var G__36365 = cljs.core.count(c__5525__auto___36362);
var G__36366 = (0);
seq__35765_36349 = G__36363;
chunk__35769_36350 = G__36364;
count__35770_36351 = G__36365;
i__35771_36352 = G__36366;
continue;
} else {
var vec__35796_36368 = cljs.core.first(seq__35765_36361__$1);
var k_36369 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35796_36368,(0),null);
var v_36370 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35796_36368,(1),null);
el.setAttributeNS((function (){var temp__5804__auto____$1 = cljs.core.namespace(k_36369);
if(cljs.core.truth_(temp__5804__auto____$1)){
var ns = temp__5804__auto____$1;
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(shadow.dom.xmlns),ns);
} else {
return null;
}
})(),cljs.core.name(k_36369),v_36370);


var G__36371 = cljs.core.next(seq__35765_36361__$1);
var G__36372 = null;
var G__36373 = (0);
var G__36374 = (0);
seq__35765_36349 = G__36371;
chunk__35769_36350 = G__36372;
count__35770_36351 = G__36373;
i__35771_36352 = G__36374;
continue;
}
} else {
}
}
break;
}

return el;
});
shadow.dom.svg_node = (function shadow$dom$svg_node(el){
if((el == null)){
return null;
} else {
if((((!((el == null))))?((((false) || ((cljs.core.PROTOCOL_SENTINEL === el.shadow$dom$SVGElement$))))?true:false):false)){
return el.shadow$dom$SVGElement$_to_svg$arity$1(null);
} else {
return el;

}
}
});
shadow.dom.make_svg_node = (function shadow$dom$make_svg_node(structure){
var vec__35811 = shadow.dom.destructure_node(shadow.dom.create_svg_node,structure);
var node = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35811,(0),null);
var node_children = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__35811,(1),null);
var seq__35818_36375 = cljs.core.seq(node_children);
var chunk__35820_36376 = null;
var count__35821_36377 = (0);
var i__35822_36378 = (0);
while(true){
if((i__35822_36378 < count__35821_36377)){
var child_struct_36379 = chunk__35820_36376.cljs$core$IIndexed$_nth$arity$2(null,i__35822_36378);
if((!((child_struct_36379 == null)))){
if(typeof child_struct_36379 === 'string'){
var text_36380 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_36380),child_struct_36379].join(''));
} else {
var children_36391 = shadow.dom.svg_node(child_struct_36379);
if(cljs.core.seq_QMARK_(children_36391)){
var seq__35894_36392 = cljs.core.seq(children_36391);
var chunk__35896_36393 = null;
var count__35897_36394 = (0);
var i__35898_36395 = (0);
while(true){
if((i__35898_36395 < count__35897_36394)){
var child_36396 = chunk__35896_36393.cljs$core$IIndexed$_nth$arity$2(null,i__35898_36395);
if(cljs.core.truth_(child_36396)){
node.appendChild(child_36396);


var G__36397 = seq__35894_36392;
var G__36398 = chunk__35896_36393;
var G__36399 = count__35897_36394;
var G__36400 = (i__35898_36395 + (1));
seq__35894_36392 = G__36397;
chunk__35896_36393 = G__36398;
count__35897_36394 = G__36399;
i__35898_36395 = G__36400;
continue;
} else {
var G__36401 = seq__35894_36392;
var G__36402 = chunk__35896_36393;
var G__36403 = count__35897_36394;
var G__36404 = (i__35898_36395 + (1));
seq__35894_36392 = G__36401;
chunk__35896_36393 = G__36402;
count__35897_36394 = G__36403;
i__35898_36395 = G__36404;
continue;
}
} else {
var temp__5804__auto___36405 = cljs.core.seq(seq__35894_36392);
if(temp__5804__auto___36405){
var seq__35894_36406__$1 = temp__5804__auto___36405;
if(cljs.core.chunked_seq_QMARK_(seq__35894_36406__$1)){
var c__5525__auto___36407 = cljs.core.chunk_first(seq__35894_36406__$1);
var G__36408 = cljs.core.chunk_rest(seq__35894_36406__$1);
var G__36409 = c__5525__auto___36407;
var G__36410 = cljs.core.count(c__5525__auto___36407);
var G__36411 = (0);
seq__35894_36392 = G__36408;
chunk__35896_36393 = G__36409;
count__35897_36394 = G__36410;
i__35898_36395 = G__36411;
continue;
} else {
var child_36413 = cljs.core.first(seq__35894_36406__$1);
if(cljs.core.truth_(child_36413)){
node.appendChild(child_36413);


var G__36414 = cljs.core.next(seq__35894_36406__$1);
var G__36415 = null;
var G__36416 = (0);
var G__36417 = (0);
seq__35894_36392 = G__36414;
chunk__35896_36393 = G__36415;
count__35897_36394 = G__36416;
i__35898_36395 = G__36417;
continue;
} else {
var G__36418 = cljs.core.next(seq__35894_36406__$1);
var G__36419 = null;
var G__36420 = (0);
var G__36421 = (0);
seq__35894_36392 = G__36418;
chunk__35896_36393 = G__36419;
count__35897_36394 = G__36420;
i__35898_36395 = G__36421;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_36391);
}
}


var G__36422 = seq__35818_36375;
var G__36423 = chunk__35820_36376;
var G__36424 = count__35821_36377;
var G__36425 = (i__35822_36378 + (1));
seq__35818_36375 = G__36422;
chunk__35820_36376 = G__36423;
count__35821_36377 = G__36424;
i__35822_36378 = G__36425;
continue;
} else {
var G__36426 = seq__35818_36375;
var G__36427 = chunk__35820_36376;
var G__36428 = count__35821_36377;
var G__36429 = (i__35822_36378 + (1));
seq__35818_36375 = G__36426;
chunk__35820_36376 = G__36427;
count__35821_36377 = G__36428;
i__35822_36378 = G__36429;
continue;
}
} else {
var temp__5804__auto___36430 = cljs.core.seq(seq__35818_36375);
if(temp__5804__auto___36430){
var seq__35818_36431__$1 = temp__5804__auto___36430;
if(cljs.core.chunked_seq_QMARK_(seq__35818_36431__$1)){
var c__5525__auto___36432 = cljs.core.chunk_first(seq__35818_36431__$1);
var G__36433 = cljs.core.chunk_rest(seq__35818_36431__$1);
var G__36434 = c__5525__auto___36432;
var G__36435 = cljs.core.count(c__5525__auto___36432);
var G__36436 = (0);
seq__35818_36375 = G__36433;
chunk__35820_36376 = G__36434;
count__35821_36377 = G__36435;
i__35822_36378 = G__36436;
continue;
} else {
var child_struct_36437 = cljs.core.first(seq__35818_36431__$1);
if((!((child_struct_36437 == null)))){
if(typeof child_struct_36437 === 'string'){
var text_36438 = (node["textContent"]);
(node["textContent"] = [cljs.core.str.cljs$core$IFn$_invoke$arity$1(text_36438),child_struct_36437].join(''));
} else {
var children_36439 = shadow.dom.svg_node(child_struct_36437);
if(cljs.core.seq_QMARK_(children_36439)){
var seq__35907_36440 = cljs.core.seq(children_36439);
var chunk__35909_36441 = null;
var count__35910_36442 = (0);
var i__35911_36443 = (0);
while(true){
if((i__35911_36443 < count__35910_36442)){
var child_36444 = chunk__35909_36441.cljs$core$IIndexed$_nth$arity$2(null,i__35911_36443);
if(cljs.core.truth_(child_36444)){
node.appendChild(child_36444);


var G__36445 = seq__35907_36440;
var G__36446 = chunk__35909_36441;
var G__36447 = count__35910_36442;
var G__36448 = (i__35911_36443 + (1));
seq__35907_36440 = G__36445;
chunk__35909_36441 = G__36446;
count__35910_36442 = G__36447;
i__35911_36443 = G__36448;
continue;
} else {
var G__36449 = seq__35907_36440;
var G__36450 = chunk__35909_36441;
var G__36451 = count__35910_36442;
var G__36452 = (i__35911_36443 + (1));
seq__35907_36440 = G__36449;
chunk__35909_36441 = G__36450;
count__35910_36442 = G__36451;
i__35911_36443 = G__36452;
continue;
}
} else {
var temp__5804__auto___36453__$1 = cljs.core.seq(seq__35907_36440);
if(temp__5804__auto___36453__$1){
var seq__35907_36454__$1 = temp__5804__auto___36453__$1;
if(cljs.core.chunked_seq_QMARK_(seq__35907_36454__$1)){
var c__5525__auto___36455 = cljs.core.chunk_first(seq__35907_36454__$1);
var G__36456 = cljs.core.chunk_rest(seq__35907_36454__$1);
var G__36457 = c__5525__auto___36455;
var G__36458 = cljs.core.count(c__5525__auto___36455);
var G__36459 = (0);
seq__35907_36440 = G__36456;
chunk__35909_36441 = G__36457;
count__35910_36442 = G__36458;
i__35911_36443 = G__36459;
continue;
} else {
var child_36460 = cljs.core.first(seq__35907_36454__$1);
if(cljs.core.truth_(child_36460)){
node.appendChild(child_36460);


var G__36461 = cljs.core.next(seq__35907_36454__$1);
var G__36462 = null;
var G__36463 = (0);
var G__36464 = (0);
seq__35907_36440 = G__36461;
chunk__35909_36441 = G__36462;
count__35910_36442 = G__36463;
i__35911_36443 = G__36464;
continue;
} else {
var G__36465 = cljs.core.next(seq__35907_36454__$1);
var G__36466 = null;
var G__36467 = (0);
var G__36468 = (0);
seq__35907_36440 = G__36465;
chunk__35909_36441 = G__36466;
count__35910_36442 = G__36467;
i__35911_36443 = G__36468;
continue;
}
}
} else {
}
}
break;
}
} else {
node.appendChild(children_36439);
}
}


var G__36469 = cljs.core.next(seq__35818_36431__$1);
var G__36470 = null;
var G__36471 = (0);
var G__36472 = (0);
seq__35818_36375 = G__36469;
chunk__35820_36376 = G__36470;
count__35821_36377 = G__36471;
i__35822_36378 = G__36472;
continue;
} else {
var G__36473 = cljs.core.next(seq__35818_36431__$1);
var G__36474 = null;
var G__36475 = (0);
var G__36476 = (0);
seq__35818_36375 = G__36473;
chunk__35820_36376 = G__36474;
count__35821_36377 = G__36475;
i__35822_36378 = G__36476;
continue;
}
}
} else {
}
}
break;
}

return node;
});
(shadow.dom.SVGElement["string"] = true);

(shadow.dom._to_svg["string"] = (function (this$){
if((this$ instanceof cljs.core.Keyword)){
return shadow.dom.make_svg_node(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [this$], null));
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("strings cannot be in svgs",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"this","this",-611633625),this$], null));
}
}));

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.PersistentVector.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return shadow.dom.make_svg_node(this$__$1);
}));

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.LazySeq.prototype.shadow$dom$SVGElement$_to_svg$arity$1 = (function (this$){
var this$__$1 = this;
return cljs.core.map.cljs$core$IFn$_invoke$arity$2(shadow.dom._to_svg,this$__$1);
}));

(shadow.dom.SVGElement["null"] = true);

(shadow.dom._to_svg["null"] = (function (_){
return null;
}));
shadow.dom.svg = (function shadow$dom$svg(var_args){
var args__5732__auto__ = [];
var len__5726__auto___36483 = arguments.length;
var i__5727__auto___36484 = (0);
while(true){
if((i__5727__auto___36484 < len__5726__auto___36483)){
args__5732__auto__.push((arguments[i__5727__auto___36484]));

var G__36485 = (i__5727__auto___36484 + (1));
i__5727__auto___36484 = G__36485;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(shadow.dom.svg.cljs$core$IFn$_invoke$arity$variadic = (function (attrs,children){
return shadow.dom._to_svg(cljs.core.vec(cljs.core.concat.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"svg","svg",856789142),attrs], null),children)));
}));

(shadow.dom.svg.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(shadow.dom.svg.cljs$lang$applyTo = (function (seq35916){
var G__35917 = cljs.core.first(seq35916);
var seq35916__$1 = cljs.core.next(seq35916);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__35917,seq35916__$1);
}));

/**
 * returns a channel for events on el
 * transform-fn should be a (fn [e el] some-val) where some-val will be put on the chan
 * once-or-cleanup handles the removal of the event handler
 * - true: remove after one event
 * - false: never removed
 * - chan: remove on msg/close
 */
shadow.dom.event_chan = (function shadow$dom$event_chan(var_args){
var G__35920 = arguments.length;
switch (G__35920) {
case 2:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$2 = (function (el,event){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,null,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$3 = (function (el,event,xf){
return shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4(el,event,xf,false);
}));

(shadow.dom.event_chan.cljs$core$IFn$_invoke$arity$4 = (function (el,event,xf,once_or_cleanup){
var buf = cljs.core.async.sliding_buffer((1));
var chan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2(buf,xf);
var event_fn = (function shadow$dom$event_fn(e){
cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(chan,e);

if(once_or_cleanup === true){
shadow.dom.remove_event_handler(el,event,shadow$dom$event_fn);

return cljs.core.async.close_BANG_(chan);
} else {
return null;
}
});
shadow.dom.dom_listen(shadow.dom.dom_node(el),cljs.core.name(event),event_fn);

if(cljs.core.truth_((function (){var and__5000__auto__ = once_or_cleanup;
if(cljs.core.truth_(and__5000__auto__)){
return (!(once_or_cleanup === true));
} else {
return and__5000__auto__;
}
})())){
var c__30228__auto___36499 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_35928){
var state_val_35929 = (state_35928[(1)]);
if((state_val_35929 === (1))){
var state_35928__$1 = state_35928;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_35928__$1,(2),once_or_cleanup);
} else {
if((state_val_35929 === (2))){
var inst_35923 = (state_35928[(2)]);
var inst_35925 = shadow.dom.remove_event_handler(el,event,event_fn);
var state_35928__$1 = (function (){var statearr_35944 = state_35928;
(statearr_35944[(7)] = inst_35923);

return statearr_35944;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_35928__$1,inst_35925);
} else {
return null;
}
}
});
return (function() {
var shadow$dom$state_machine__29318__auto__ = null;
var shadow$dom$state_machine__29318__auto____0 = (function (){
var statearr_35945 = [null,null,null,null,null,null,null,null];
(statearr_35945[(0)] = shadow$dom$state_machine__29318__auto__);

(statearr_35945[(1)] = (1));

return statearr_35945;
});
var shadow$dom$state_machine__29318__auto____1 = (function (state_35928){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_35928);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e35946){var ex__29321__auto__ = e35946;
var statearr_35947_36500 = state_35928;
(statearr_35947_36500[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_35928[(4)]))){
var statearr_35948_36502 = state_35928;
(statearr_35948_36502[(1)] = cljs.core.first((state_35928[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36505 = state_35928;
state_35928 = G__36505;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
shadow$dom$state_machine__29318__auto__ = function(state_35928){
switch(arguments.length){
case 0:
return shadow$dom$state_machine__29318__auto____0.call(this);
case 1:
return shadow$dom$state_machine__29318__auto____1.call(this,state_35928);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
shadow$dom$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = shadow$dom$state_machine__29318__auto____0;
shadow$dom$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = shadow$dom$state_machine__29318__auto____1;
return shadow$dom$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_35949 = f__30229__auto__();
(statearr_35949[(6)] = c__30228__auto___36499);

return statearr_35949;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));

} else {
}

return chan;
}));

(shadow.dom.event_chan.cljs$lang$maxFixedArity = 4);


//# sourceMappingURL=shadow.dom.js.map
