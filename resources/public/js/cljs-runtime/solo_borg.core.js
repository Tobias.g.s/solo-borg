goog.provide('solo_borg.core');
if((typeof solo_borg !== 'undefined') && (typeof solo_borg.core !== 'undefined') && (typeof solo_borg.core.root !== 'undefined')){
} else {
solo_borg.core.root = reagent.dom.client.create_root(document.getElementById("app"));
}
solo_borg.core.render = (function solo_borg$core$render(){
return reagent.dom.client.render.cljs$core$IFn$_invoke$arity$2(solo_borg.core.root,reagent.core.as_element.cljs$core$IFn$_invoke$arity$1((cljs.core.truth_(goog.DEBUG)?new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [(function (){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [solo_borg.views.main_view], null);
})], null):new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [solo_borg.views.main_view], null))));
});
solo_borg.core.start = (function solo_borg$core$start(){
console.log("start");

return solo_borg.core.render();
});
solo_borg.core.init = (function solo_borg$core$init(){
solo_borg.core.start();

return cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1("hello borg");
});
goog.exportSymbol('solo_borg.core.init', solo_borg.core.init);

//# sourceMappingURL=solo_borg.core.js.map
