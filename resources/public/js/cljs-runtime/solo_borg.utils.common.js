goog.provide('solo_borg.utils.common');
solo_borg.utils.common.roll_dice = (function solo_borg$utils$common$roll_dice(mod){
if(cljs.core.truth_((function (){var and__5000__auto__ = mod;
if(cljs.core.truth_(and__5000__auto__)){
return (mod > (1));
} else {
return and__5000__auto__;
}
})())){
} else {
throw (new Error("Assert failed: (and mod (> mod 1))"));
}

return (cljs.core.rand_int(mod) + (1));
});
solo_borg.utils.common.roll_multiple_dice = (function solo_borg$utils$common$roll_multiple_dice(amount,mod){
if(cljs.core.truth_((function (){var and__5000__auto__ = amount;
if(cljs.core.truth_(and__5000__auto__)){
return (amount >= (1));
} else {
return and__5000__auto__;
}
})())){
} else {
throw (new Error("Assert failed: (and amount (>= amount 1))"));
}

return cljs.core.take.cljs$core$IFn$_invoke$arity$2(amount,cljs.core.repeatedly.cljs$core$IFn$_invoke$arity$1((function (){
return solo_borg.utils.common.roll_dice(mod);
})));
});
solo_borg.utils.common.dice__GT_stat = (function solo_borg$utils$common$dice__GT_stat(num){
if((((num >= (1))) && ((num <= (20))))){
} else {
throw (new Error("Assert failed: (and (>= num 1) (<= num 20))"));
}

if(((((1) <= num)) && ((num <= (4))))){
return (-3);
} else {
if(((((5) <= num)) && ((num <= (6))))){
return (-2);
} else {
if(((((7) <= num)) && ((num <= (8))))){
return (-1);
} else {
if(((((9) <= num)) && ((num <= (12))))){
return (0);
} else {
if(((((13) <= num)) && ((num <= (14))))){
return (1);
} else {
if(((((15) <= num)) && ((num <= (16))))){
return (2);
} else {
if(((((17) <= num)) && ((num <= (20))))){
return (3);
} else {
return (0);

}
}
}
}
}
}
}
});

//# sourceMappingURL=solo_borg.utils.common.js.map
