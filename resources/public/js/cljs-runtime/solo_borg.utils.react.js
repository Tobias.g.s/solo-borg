goog.provide('solo_borg.utils.react');
var module$node_modules$react$index=shadow.js.require("module$node_modules$react$index", {});
var module$node_modules$$formkit$auto_animate$react$index_mjs=shadow.js.require("module$node_modules$$formkit$auto_animate$react$index_mjs", {});
solo_borg.utils.react.use_state = module$node_modules$react$index.useState;
solo_borg.utils.react.use_effect = module$node_modules$react$index.useEffect;
solo_borg.utils.react.use_auto_animate = module$node_modules$$formkit$auto_animate$react$index_mjs.useAutoAnimate;

//# sourceMappingURL=solo_borg.utils.react.js.map
