goog.provide('solo_borg.views.layout');
solo_borg.views.layout.default$ = (function solo_borg$views$layout$default(var_args){
var args__5732__auto__ = [];
var len__5726__auto___28141 = arguments.length;
var i__5727__auto___28142 = (0);
while(true){
if((i__5727__auto___28142 < len__5726__auto___28141)){
args__5732__auto__.push((arguments[i__5727__auto___28142]));

var G__28143 = (i__5727__auto___28142 + (1));
i__5727__auto___28142 = G__28143;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return solo_borg.views.layout.default$.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(solo_borg.views.layout.default$.cljs$core$IFn$_invoke$arity$variadic = (function (content){
var vec__28138 = (solo_borg.utils.react.use_auto_animate.cljs$core$IFn$_invoke$arity$0 ? solo_borg.utils.react.use_auto_animate.cljs$core$IFn$_invoke$arity$0() : solo_borg.utils.react.use_auto_animate.call(null));
var parent_ref = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__28138,(0),null);
cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1(cljs.core.js_keys(shadow.js.shim.module$$formkit$auto_animate$default));

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div.container","div.container",72419955),cljs.core.into.cljs$core$IFn$_invoke$arity$2(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"section.section","section.section",-416807119),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ref","ref",1289896967),parent_ref], null)], null),content)], null);
}));

(solo_borg.views.layout.default$.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(solo_borg.views.layout.default$.cljs$lang$applyTo = (function (seq28137){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq28137));
}));


//# sourceMappingURL=solo_borg.views.layout.js.map
