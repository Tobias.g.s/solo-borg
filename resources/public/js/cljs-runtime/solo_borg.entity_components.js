goog.provide('solo_borg.entity_components');
solo_borg.entity_components.classes = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"gutter-born-scum","gutter-born-scum",1480511992),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"stats","stats",-85643011),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"strength","strength",-415606478),(-2),new cljs.core.Keyword(null,"agility","agility",-456641765),(0),new cljs.core.Keyword(null,"presence","presence",-1580756953),(0),new cljs.core.Keyword(null,"toughness","toughness",-1178302561),(0)], null),new cljs.core.Keyword(null,"omens","omens",1897425920),(1),new cljs.core.Keyword(null,"hp","hp",-1541237831),(function (){
return (cljs.core.rand_int((6)) + (1));
}),new cljs.core.Keyword(null,"silver","silver",1044501468),(function (){
return ((10) * ((2) * solo_borg.utils.common.roll_dice((6))));
})], null)], null),new cljs.core.Keyword(null,"esoteric-hermit","esoteric-hermit",-634621819),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"stats","stats",-85643011),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"strength","strength",-415606478),(-2),new cljs.core.Keyword(null,"agility","agility",-456641765),(0),new cljs.core.Keyword(null,"presence","presence",-1580756953),(2),new cljs.core.Keyword(null,"toughness","toughness",-1178302561),(0)], null),new cljs.core.Keyword(null,"omens","omens",1897425920),(4),new cljs.core.Keyword(null,"hp","hp",-1541237831),(function (){
return (cljs.core.rand_int((4)) + (1));
}),new cljs.core.Keyword(null,"silver","silver",1044501468),(function (){
return ((10) * ((2) * solo_borg.utils.common.roll_dice((6))));
})], null)], null),new cljs.core.Keyword(null,"wretched-royalty","wretched-royalty",-1283620036),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"stats","stats",-85643011),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"strength","strength",-415606478),(0),new cljs.core.Keyword(null,"agility","agility",-456641765),(0),new cljs.core.Keyword(null,"presence","presence",-1580756953),(0),new cljs.core.Keyword(null,"toughness","toughness",-1178302561),(0)], null),new cljs.core.Keyword(null,"omens","omens",1897425920),(4),new cljs.core.Keyword(null,"hp","hp",-1541237831),(function (){
return (cljs.core.rand_int((6)) + (1));
}),new cljs.core.Keyword(null,"silver","silver",1044501468),(function (){
return ((10) * ((4) * solo_borg.utils.common.roll_dice((6))));
})], null)], null),new cljs.core.Keyword(null,"heretical-priest","heretical-priest",1051392612),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"stats","stats",-85643011),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"strength","strength",-415606478),(-2),new cljs.core.Keyword(null,"agility","agility",-456641765),(0),new cljs.core.Keyword(null,"presence","presence",-1580756953),(2),new cljs.core.Keyword(null,"toughness","toughness",-1178302561),(0)], null),new cljs.core.Keyword(null,"omens","omens",1897425920),(4),new cljs.core.Keyword(null,"hp","hp",-1541237831),(function (){
return (cljs.core.rand_int((8)) + (1));
}),new cljs.core.Keyword(null,"silver","silver",1044501468),(function (){
return ((10) * ((3) * solo_borg.utils.common.roll_dice((6))));
})], null)], null),new cljs.core.Keyword(null,"occult-herbmaster","occult-herbmaster",2017210719),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"stats","stats",-85643011),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"strength","strength",-415606478),(-2),new cljs.core.Keyword(null,"agility","agility",-456641765),(0),new cljs.core.Keyword(null,"presence","presence",-1580756953),(0),new cljs.core.Keyword(null,"toughness","toughness",-1178302561),(2)], null),new cljs.core.Keyword(null,"omens","omens",1897425920),(4),new cljs.core.Keyword(null,"hp","hp",-1541237831),(function (){
return (cljs.core.rand_int((6)) + (1));
}),new cljs.core.Keyword(null,"silver","silver",1044501468),(function (){
return ((10) * ((2) * solo_borg.utils.common.roll_dice((6))));
})], null)], null),new cljs.core.Keyword(null,"fanged-deserter","fanged-deserter",1559164404),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"stats","stats",-85643011),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"strength","strength",-415606478),(2),new cljs.core.Keyword(null,"agility","agility",-456641765),(-1),new cljs.core.Keyword(null,"presence","presence",-1580756953),(-1),new cljs.core.Keyword(null,"toughness","toughness",-1178302561),(0)], null),new cljs.core.Keyword(null,"omens","omens",1897425920),(2),new cljs.core.Keyword(null,"hp","hp",-1541237831),(function (){
return (cljs.core.rand_int((8)) + (1));
}),new cljs.core.Keyword(null,"silver","silver",1044501468),(function (){
return ((10) * ((2) * solo_borg.utils.common.roll_dice((6))));
})], null)], null)], null);
solo_borg.entity_components.curses = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"gluttony","gluttony",-445416390),new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"curse","curse",-1793995544),new cljs.core.Keyword(null,"description","description",-1428560544),"you are fat kek",new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"stats","stats",-85643011),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"toughness","toughness",-1178302561),(2),new cljs.core.Keyword(null,"agility","agility",-456641765),(-1)], null)], null)], null)], null);
solo_borg.entity_components.base_traits = new cljs.core.PersistentArrayMap(null, 8, [new cljs.core.Keyword(null,"breathing","breathing",565216650),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"",new cljs.core.Keyword(null,"description","description",-1428560544),""], null),new cljs.core.Keyword(null,"head","head",-771383919),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"",new cljs.core.Keyword(null,"description","description",-1428560544),""], null),new cljs.core.Keyword(null,"torso","torso",386332103),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"",new cljs.core.Keyword(null,"description","description",-1428560544),""], null),new cljs.core.Keyword(null,"left-arm","left-arm",-593257832),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"",new cljs.core.Keyword(null,"description","description",-1428560544),""], null),new cljs.core.Keyword(null,"right-arm","right-arm",1433793260),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"",new cljs.core.Keyword(null,"description","description",-1428560544),""], null),new cljs.core.Keyword(null,"left-leg","left-leg",-406014397),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"",new cljs.core.Keyword(null,"description","description",-1428560544),""], null),new cljs.core.Keyword(null,"right-leg","right-leg",-547986289),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"",new cljs.core.Keyword(null,"description","description",-1428560544),""], null),new cljs.core.Keyword(null,"animated","animated",129318795),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"title","title",636505583),"",new cljs.core.Keyword(null,"description","description",-1428560544),""], null)], null);
solo_borg.entity_components.terrible_traits = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"suspicious","suspicious",-1252714336),new cljs.core.Keyword(null,"wasteful","wasteful",779080384),new cljs.core.Keyword(null,"worried","worried",-1028754462),new cljs.core.Keyword(null,"shrewd","shrewd",-905403353),new cljs.core.Keyword(null,"deceitful","deceitful",1350192519),new cljs.core.Keyword(null,"endlessly-aggravated","endlessly-aggravated",-1508725366),new cljs.core.Keyword(null,"nihilist","nihilist",1937897163),new cljs.core.Keyword(null,"conflicted","conflicted",-1359923057),new cljs.core.Keyword(null,"vindictive","vindictive",-2137709137),new cljs.core.Keyword(null,"ruthless","ruthless",1495646066),new cljs.core.Keyword(null,"loud-mouth","loud-mouth",-591619021),new cljs.core.Keyword(null,"lazy","lazy",-424547181),new cljs.core.Keyword(null,"egocentric","egocentric",1604817495),new cljs.core.Keyword(null,"bitter","bitter",1698197912),new cljs.core.Keyword(null,"problems-complex","problems-complex",931468922),new cljs.core.Keyword(null,"arrogant","arrogant",-461620964),new cljs.core.Keyword(null,"cowardly","cowardly",316211516),new cljs.core.Keyword(null,"cruel","cruel",922179678),new cljs.core.Keyword(null,"prone-to-substanc-abuse","prone-to-substanc-abuse",1065247198),new cljs.core.Keyword(null,"inferiority-complex","inferiority-complex",788338430)],[cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentArrayMap.EMPTY]);
solo_borg.entity_components.broken_body_traits = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"staring-maniac-gaze","staring-maniac-gaze",472479598),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"covered-in-blasphemous-tattoos","covered-in-blasphemous-tattoos",1715095103),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"face-rotting","face-rotting",-1501596012),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"masked","masked",1191724351),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"lost-three-toes","lost-three-toes",16897169),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"limps","limps",-1794027675),cljs.core.PersistentArrayMap.EMPTY], null);
solo_borg.entity_components.bad_behaviour_traits = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"obsessivly-collect-small-sharp-stones","obsessivly-collect-small-sharp-stones",-740922598),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"wont-use-blade-without-testing-it-on-your-own-flesh","wont-use-blade-without-testing-it-on-your-own-flesh",903576815),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"arms-knitted-with-scars","arms-knitted-with-scars",-1668357616),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"cant-stop-drinking-once-started","cant-stop-drinking-once-started",-1045677511),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"gambling-addict","gambling-addict",1865052605),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"cant-tolarate-critisism","cant-tolarate-critisism",1440668944),cljs.core.PersistentArrayMap.EMPTY], null);
solo_borg.entity_components.neutral_traits = new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"ugly","ugly",-1650246496),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hook-hand","hook-hand",1976845010),cljs.core.PersistentArrayMap.EMPTY], null);
solo_borg.entity_components.negative_traits = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ugly","ugly",-1650246496),cljs.core.PersistentArrayMap.EMPTY], null);
solo_borg.entity_components.positive_traits = new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"handsome","handsome",-2098227481),cljs.core.PersistentArrayMap.EMPTY], null);
solo_borg.entity_components.names = cljs.core.PersistentHashMap.fromArrays(["felban","agn","kutz","svind","katla","keftar","vemut","senkil","domm","nagl"],[new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player","player",-97687400),null,new cljs.core.Keyword(null,"npc","npc",2026900089),null], null), null)], null)]);
solo_borg.entity_components.pick_random = (function solo_borg$entity_components$pick_random(collection){
return cljs.core.first(cljs.core.shuffle(cljs.core.keys(collection)));
});
solo_borg.entity_components.generate_stats = (function solo_borg$entity_components$generate_stats(class$){
return cljs.core.merge_with.cljs$core$IFn$_invoke$arity$variadic(cljs.core._PLUS_,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.get_in.cljs$core$IFn$_invoke$arity$3(class$,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.Keyword(null,"stats","stats",-85643011)], null),cljs.core.PersistentArrayMap.EMPTY),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"strength","strength",-415606478),solo_borg.utils.common.dice__GT_stat(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,solo_borg.utils.common.roll_multiple_dice((3),(6)))),new cljs.core.Keyword(null,"agility","agility",-456641765),solo_borg.utils.common.dice__GT_stat(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,solo_borg.utils.common.roll_multiple_dice((3),(6)))),new cljs.core.Keyword(null,"presence","presence",-1580756953),solo_borg.utils.common.dice__GT_stat(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,solo_borg.utils.common.roll_multiple_dice((3),(6)))),new cljs.core.Keyword(null,"toughness","toughness",-1178302561),solo_borg.utils.common.dice__GT_stat(cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core._PLUS_,solo_borg.utils.common.roll_multiple_dice((3),(6))))], null)], 0));
});
solo_borg.entity_components.pick_name = (function solo_borg$entity_components$pick_name(){
return solo_borg.entity_components.pick_random(solo_borg.entity_components.names);
});
solo_borg.entity_components.pick_multiple_names = (function solo_borg$entity_components$pick_multiple_names(amount){
if(cljs.core.truth_((function (){var and__5000__auto__ = amount;
if(cljs.core.truth_(and__5000__auto__)){
return (amount <= cljs.core.count(solo_borg.entity_components.names));
} else {
return and__5000__auto__;
}
})())){
} else {
throw (new Error("Assert failed: (and amount (<= amount (count names)))"));
}

return cljs.core.take.cljs$core$IFn$_invoke$arity$2(amount,cljs.core.shuffle(cljs.core.keys(solo_borg.entity_components.names)));
});
solo_borg.entity_components.player_example = new cljs.core.PersistentArrayMap(null, 7, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"player-character","player-character",-50992952),null,new cljs.core.Keyword(null,"undead","undead",-1468440782),null], null), null),new cljs.core.Keyword(null,"name","name",1843675177),"Knut",new cljs.core.Keyword(null,"stats","stats",-85643011),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"strength","strength",-415606478),(0),new cljs.core.Keyword(null,"agility","agility",-456641765),(-1),new cljs.core.Keyword(null,"presence","presence",-1580756953),(3),new cljs.core.Keyword(null,"toughness","toughness",-1178302561),(1)], null),new cljs.core.Keyword(null,"hp","hp",-1541237831),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [(-1),(1)], null),new cljs.core.Keyword(null,"omens","omens",1897425920),(2),new cljs.core.Keyword(null,"class","class",-2030961996),new cljs.core.Keyword(null,"gutter-born-scum","gutter-born-scum",1480511992),new cljs.core.Keyword(null,"traits","traits",1778193407),new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 15, [new cljs.core.Keyword(null,"panicked","panicked",-2075235008),null,new cljs.core.Keyword(null,"empovered","empovered",748576931),null,new cljs.core.Keyword(null,"left-leg","left-leg",-406014397),null,new cljs.core.Keyword(null,"torso","torso",386332103),null,new cljs.core.Keyword(null,"heretic","heretic",1089876647),null,new cljs.core.Keyword(null,"handsome","handsome",-2098227481),null,new cljs.core.Keyword(null,"strong","strong",269529000),null,new cljs.core.Keyword(null,"bleeding","bleeding",372683946),null,new cljs.core.Keyword(null,"left-hand-is-hook","left-hand-is-hook",-1595672498),null,new cljs.core.Keyword(null,"right-leg","right-leg",-547986289),null,new cljs.core.Keyword(null,"head","head",-771383919),null,new cljs.core.Keyword(null,"face-rotting","face-rotting",-1501596012),null,new cljs.core.Keyword(null,"well-endowed","well-endowed",-148833606),null,new cljs.core.Keyword(null,"hungry","hungry",-1418806949),null,new cljs.core.Keyword(null,"right-hand","right-hand",1578446749),null], null), null)], null);
solo_borg.entity_components.create_random_player = (function solo_borg$entity_components$create_random_player(){
var class$ = solo_borg.entity_components.pick_random(solo_borg.entity_components.classes);
return new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"name","name",1843675177),solo_borg.entity_components.pick_random(solo_borg.entity_components.names),new cljs.core.Keyword(null,"stats","stats",-85643011),solo_borg.entity_components.generate_stats(class$),new cljs.core.Keyword(null,"class","class",-2030961996),class$,new cljs.core.Keyword(null,"omens","omens",1897425920),cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(solo_borg.entity_components.classes,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [class$,new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.Keyword(null,"omens","omens",1897425920)], null)),new cljs.core.Keyword(null,"hp","hp",-1541237831),((function (){var x__5087__auto__ = (0);
var y__5088__auto__ = (function (){var fexpr__34275 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(solo_borg.entity_components.classes,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [class$,new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.Keyword(null,"hp","hp",-1541237831)], null));
return (fexpr__34275.cljs$core$IFn$_invoke$arity$0 ? fexpr__34275.cljs$core$IFn$_invoke$arity$0() : fexpr__34275.call(null));
})();
return ((x__5087__auto__ > y__5088__auto__) ? x__5087__auto__ : y__5088__auto__);
})()),new cljs.core.Keyword(null,"silver","silver",1044501468),(function (){var fexpr__34276 = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(solo_borg.entity_components.classes,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [class$,new cljs.core.Keyword(null,"modifiers","modifiers",50378834),new cljs.core.Keyword(null,"silver","silver",1044501468)], null));
return (fexpr__34276.cljs$core$IFn$_invoke$arity$0 ? fexpr__34276.cljs$core$IFn$_invoke$arity$0() : fexpr__34276.call(null));
})()], null);
});
solo_borg.entity_components.add_player_BANG_ = (function solo_borg$entity_components$add_player_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___34290 = arguments.length;
var i__5727__auto___34291 = (0);
while(true){
if((i__5727__auto___34291 < len__5726__auto___34290)){
args__5732__auto__.push((arguments[i__5727__auto___34291]));

var G__34292 = (i__5727__auto___34291 + (1));
i__5727__auto___34291 = G__34292;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return solo_borg.entity_components.add_player_BANG_.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(solo_borg.entity_components.add_player_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__34278){
var vec__34279 = p__34278;
var player = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34279,(0),null);
var player__$1 = (function (){var or__5002__auto__ = player;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return solo_borg.entity_components.create_random_player();
}
})();
var id = (function (){var or__5002__auto__ = new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(player__$1);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return cljs.core.random_uuid();
}
})();
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(solo_borg.state.game,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"players","players",-1361554569),id], null),cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(player__$1,new cljs.core.Keyword(null,"id","id",-1388402092),id));
}));

(solo_borg.entity_components.add_player_BANG_.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(solo_borg.entity_components.add_player_BANG_.cljs$lang$applyTo = (function (seq34277){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq34277));
}));

solo_borg.entity_components.delete_player_BANG_ = (function solo_borg$entity_components$delete_player_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___34298 = arguments.length;
var i__5727__auto___34299 = (0);
while(true){
if((i__5727__auto___34299 < len__5726__auto___34298)){
args__5732__auto__.push((arguments[i__5727__auto___34299]));

var G__34300 = (i__5727__auto___34299 + (1));
i__5727__auto___34299 = G__34300;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((0) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((0)),(0),null)):null);
return solo_borg.entity_components.delete_player_BANG_.cljs$core$IFn$_invoke$arity$variadic(argseq__5733__auto__);
});

(solo_borg.entity_components.delete_player_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__34283){
var vec__34284 = p__34283;
var id = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34284,(0),null);
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(solo_borg.state.game,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"players","players",-1361554569)], null),(function (players){
return cljs.core.dissoc.cljs$core$IFn$_invoke$arity$2(players,id);
}));
}));

(solo_borg.entity_components.delete_player_BANG_.cljs$lang$maxFixedArity = (0));

/** @this {Function} */
(solo_borg.entity_components.delete_player_BANG_.cljs$lang$applyTo = (function (seq34282){
var self__5712__auto__ = this;
return self__5712__auto__.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq(seq34282));
}));


//# sourceMappingURL=solo_borg.entity_components.js.map
