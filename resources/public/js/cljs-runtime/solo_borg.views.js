goog.provide('solo_borg.views');
if((typeof solo_borg !== 'undefined') && (typeof solo_borg.views !== 'undefined') && (typeof solo_borg.views.scenes !== 'undefined')){
} else {
solo_borg.views.scenes = (function (){var method_table__5599__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__5600__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var method_cache__5601__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__5602__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__5603__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),(function (){var fexpr__34293 = cljs.core.get_global_hierarchy;
return (fexpr__34293.cljs$core$IFn$_invoke$arity$0 ? fexpr__34293.cljs$core$IFn$_invoke$arity$0() : fexpr__34293.call(null));
})());
return (new cljs.core.MultiFn(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("solo-borg.views","scenes"),(function (p__34294){
var vec__34295 = p__34294;
var key = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34295,(0),null);
var _props = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34295,(1),null);
return key;
}),new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__5603__auto__,method_table__5599__auto__,prefer_table__5600__auto__,method_cache__5601__auto__,cached_hierarchy__5602__auto__));
})();
}
solo_borg.views.scenes.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"home","home",-74557309),(function (p__34301){
var vec__34302 = p__34301;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34302,(0),null);
var props = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34302,(1),null);

return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [solo_borg.views.home.view,props], null);
}));
solo_borg.views.scenes.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"character-creator","character-creator",-2104335526),(function (p__34305){
var vec__34306 = p__34305;
var _ = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34306,(0),null);
var props = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__34306,(1),null);
cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1(_);

cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1(props);

return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"f>","f>",1484564198),solo_borg.views.character_creator.view,props], null);
}));
solo_borg.views.main_view = (function solo_borg$views$main_view(){
var selected_view = new cljs.core.Keyword(null,"selected-view","selected-view",501290513).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(solo_borg.state.app));
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"f>","f>",1484564198),solo_borg.views.layout.default$,solo_borg.views.scenes.cljs$core$IFn$_invoke$arity$1(selected_view)], null);
});

//# sourceMappingURL=solo_borg.views.js.map
