goog.provide('solo_borg.state');
if((typeof solo_borg !== 'undefined') && (typeof solo_borg.state !== 'undefined') && (typeof solo_borg.state.game !== 'undefined')){
} else {
solo_borg.state.game = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
}
if((typeof solo_borg !== 'undefined') && (typeof solo_borg.state !== 'undefined') && (typeof solo_borg.state.app !== 'undefined')){
} else {
solo_borg.state.app = reagent.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"selected-view","selected-view",501290513),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"home","home",-74557309),cljs.core.PersistentArrayMap.EMPTY], null)], null));
}
solo_borg.state.select_view = (function solo_borg$state$select_view(key,props){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(solo_borg.state.app,cljs.core.assoc,new cljs.core.Keyword(null,"selected-view","selected-view",501290513),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [key,props], null));
});

//# sourceMappingURL=solo_borg.state.js.map
