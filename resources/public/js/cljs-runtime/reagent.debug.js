goog.provide('reagent.debug');
reagent.debug.has_console = (typeof console !== 'undefined');
reagent.debug.tracking = false;
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.warnings !== 'undefined')){
} else {
reagent.debug.warnings = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
}
if((typeof reagent !== 'undefined') && (typeof reagent.debug !== 'undefined') && (typeof reagent.debug.track_console !== 'undefined')){
} else {
reagent.debug.track_console = (function (){var o = ({});
(o.warn = (function() { 
var G__32077__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"warn","warn",-436710552)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__32077 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__32079__i = 0, G__32079__a = new Array(arguments.length -  0);
while (G__32079__i < G__32079__a.length) {G__32079__a[G__32079__i] = arguments[G__32079__i + 0]; ++G__32079__i;}
  args = new cljs.core.IndexedSeq(G__32079__a,0,null);
} 
return G__32077__delegate.call(this,args);};
G__32077.cljs$lang$maxFixedArity = 0;
G__32077.cljs$lang$applyTo = (function (arglist__32084){
var args = cljs.core.seq(arglist__32084);
return G__32077__delegate(args);
});
G__32077.cljs$core$IFn$_invoke$arity$variadic = G__32077__delegate;
return G__32077;
})()
);

(o.error = (function() { 
var G__32086__delegate = function (args){
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(reagent.debug.warnings,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"error","error",-978969032)], null),cljs.core.conj,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([cljs.core.apply.cljs$core$IFn$_invoke$arity$2(cljs.core.str,args)], 0));
};
var G__32086 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__32089__i = 0, G__32089__a = new Array(arguments.length -  0);
while (G__32089__i < G__32089__a.length) {G__32089__a[G__32089__i] = arguments[G__32089__i + 0]; ++G__32089__i;}
  args = new cljs.core.IndexedSeq(G__32089__a,0,null);
} 
return G__32086__delegate.call(this,args);};
G__32086.cljs$lang$maxFixedArity = 0;
G__32086.cljs$lang$applyTo = (function (arglist__32091){
var args = cljs.core.seq(arglist__32091);
return G__32086__delegate(args);
});
G__32086.cljs$core$IFn$_invoke$arity$variadic = G__32086__delegate;
return G__32086;
})()
);

return o;
})();
}
reagent.debug.track_warnings = (function reagent$debug$track_warnings(f){
(reagent.debug.tracking = true);

cljs.core.reset_BANG_(reagent.debug.warnings,null);

(f.cljs$core$IFn$_invoke$arity$0 ? f.cljs$core$IFn$_invoke$arity$0() : f.call(null));

var warns = cljs.core.deref(reagent.debug.warnings);
cljs.core.reset_BANG_(reagent.debug.warnings,null);

(reagent.debug.tracking = false);

return warns;
});

//# sourceMappingURL=reagent.debug.js.map
