goog.provide('shadow.remote.runtime.shared');
shadow.remote.runtime.shared.init_state = (function shadow$remote$runtime$shared$init_state(client_info){
return new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),(0),new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.PersistentArrayMap.EMPTY], null);
});
shadow.remote.runtime.shared.now = (function shadow$remote$runtime$shared$now(){
return Date.now();
});
shadow.remote.runtime.shared.get_client_id = (function shadow$remote$runtime$shared$get_client_id(p__29427){
var map__29429 = p__29427;
var map__29429__$1 = cljs.core.__destructure_map(map__29429);
var runtime = map__29429__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29429__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var or__5002__auto__ = new cljs.core.Keyword(null,"client-id","client-id",-464622140).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("runtime has no assigned runtime-id",new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"runtime","runtime",-1331573996),runtime], null));
}
});
shadow.remote.runtime.shared.relay_msg = (function shadow$remote$runtime$shared$relay_msg(runtime,msg){
var self_id_29759 = shadow.remote.runtime.shared.get_client_id(runtime);
if(cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"to","to",192099007).cljs$core$IFn$_invoke$arity$1(msg),self_id_29759)){
shadow.remote.runtime.api.relay_msg(runtime,msg);
} else {
Promise.resolve((1)).then((function (){
var G__29434 = runtime;
var G__29435 = cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"from","from",1815293044),self_id_29759);
return (shadow.remote.runtime.shared.process.cljs$core$IFn$_invoke$arity$2 ? shadow.remote.runtime.shared.process.cljs$core$IFn$_invoke$arity$2(G__29434,G__29435) : shadow.remote.runtime.shared.process.call(null,G__29434,G__29435));
}));
}

return msg;
});
shadow.remote.runtime.shared.reply = (function shadow$remote$runtime$shared$reply(runtime,p__29443,res){
var map__29444 = p__29443;
var map__29444__$1 = cljs.core.__destructure_map(map__29444);
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29444__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var from = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29444__$1,new cljs.core.Keyword(null,"from","from",1815293044));
var res__$1 = (function (){var G__29445 = res;
var G__29445__$1 = (cljs.core.truth_(call_id)?cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__29445,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id):G__29445);
if(cljs.core.truth_(from)){
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(G__29445__$1,new cljs.core.Keyword(null,"to","to",192099007),from);
} else {
return G__29445__$1;
}
})();
return shadow.remote.runtime.api.relay_msg(runtime,res__$1);
});
shadow.remote.runtime.shared.call = (function shadow$remote$runtime$shared$call(var_args){
var G__29449 = arguments.length;
switch (G__29449) {
case 3:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$3 = (function (runtime,msg,handlers){
return shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4(runtime,msg,handlers,(0));
}));

(shadow.remote.runtime.shared.call.cljs$core$IFn$_invoke$arity$4 = (function (p__29453,msg,handlers,timeout_after_ms){
var map__29454 = p__29453;
var map__29454__$1 = cljs.core.__destructure_map(map__29454);
var runtime = map__29454__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29454__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
if(cljs.core.map_QMARK_(msg)){
} else {
throw (new Error("Assert failed: (map? msg)"));
}

if(cljs.core.map_QMARK_(handlers)){
} else {
throw (new Error("Assert failed: (map? handlers)"));
}

if(cljs.core.nat_int_QMARK_(timeout_after_ms)){
} else {
throw (new Error("Assert failed: (nat-int? timeout-after-ms)"));
}

var call_id = new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-id-seq","call-id-seq",-1679248218),cljs.core.inc);

cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null),new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"handlers","handlers",79528781),handlers,new cljs.core.Keyword(null,"called-at","called-at",607081160),shadow.remote.runtime.shared.now(),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg,new cljs.core.Keyword(null,"timeout","timeout",-318625318),timeout_after_ms], null));

return shadow.remote.runtime.api.relay_msg(runtime,cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(msg,new cljs.core.Keyword(null,"call-id","call-id",1043012968),call_id));
}));

(shadow.remote.runtime.shared.call.cljs$lang$maxFixedArity = 4);

shadow.remote.runtime.shared.trigger_BANG_ = (function shadow$remote$runtime$shared$trigger_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___29797 = arguments.length;
var i__5727__auto___29798 = (0);
while(true){
if((i__5727__auto___29798 < len__5726__auto___29797)){
args__5732__auto__.push((arguments[i__5727__auto___29798]));

var G__29803 = (i__5727__auto___29798 + (1));
i__5727__auto___29798 = G__29803;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((2) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((2)),(0),null)):null);
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),argseq__5733__auto__);
});

(shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (p__29481,ev,args){
var map__29482 = p__29481;
var map__29482__$1 = cljs.core.__destructure_map(map__29482);
var runtime = map__29482__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29482__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var seq__29484 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__29487 = null;
var count__29488 = (0);
var i__29489 = (0);
while(true){
if((i__29489 < count__29488)){
var ext = chunk__29487.cljs$core$IIndexed$_nth$arity$2(null,i__29489);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__29813 = seq__29484;
var G__29814 = chunk__29487;
var G__29815 = count__29488;
var G__29816 = (i__29489 + (1));
seq__29484 = G__29813;
chunk__29487 = G__29814;
count__29488 = G__29815;
i__29489 = G__29816;
continue;
} else {
var G__29817 = seq__29484;
var G__29819 = chunk__29487;
var G__29820 = count__29488;
var G__29821 = (i__29489 + (1));
seq__29484 = G__29817;
chunk__29487 = G__29819;
count__29488 = G__29820;
i__29489 = G__29821;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__29484);
if(temp__5804__auto__){
var seq__29484__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__29484__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__29484__$1);
var G__29829 = cljs.core.chunk_rest(seq__29484__$1);
var G__29830 = c__5525__auto__;
var G__29831 = cljs.core.count(c__5525__auto__);
var G__29832 = (0);
seq__29484 = G__29829;
chunk__29487 = G__29830;
count__29488 = G__29831;
i__29489 = G__29832;
continue;
} else {
var ext = cljs.core.first(seq__29484__$1);
var ev_fn = cljs.core.get.cljs$core$IFn$_invoke$arity$2(ext,ev);
if(cljs.core.truth_(ev_fn)){
cljs.core.apply.cljs$core$IFn$_invoke$arity$2(ev_fn,args);


var G__29836 = cljs.core.next(seq__29484__$1);
var G__29837 = null;
var G__29838 = (0);
var G__29839 = (0);
seq__29484 = G__29836;
chunk__29487 = G__29837;
count__29488 = G__29838;
i__29489 = G__29839;
continue;
} else {
var G__29841 = cljs.core.next(seq__29484__$1);
var G__29842 = null;
var G__29843 = (0);
var G__29844 = (0);
seq__29484 = G__29841;
chunk__29487 = G__29842;
count__29488 = G__29843;
i__29489 = G__29844;
continue;
}
}
} else {
return null;
}
}
break;
}
}));

(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$maxFixedArity = (2));

/** @this {Function} */
(shadow.remote.runtime.shared.trigger_BANG_.cljs$lang$applyTo = (function (seq29455){
var G__29456 = cljs.core.first(seq29455);
var seq29455__$1 = cljs.core.next(seq29455);
var G__29457 = cljs.core.first(seq29455__$1);
var seq29455__$2 = cljs.core.next(seq29455__$1);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__29456,G__29457,seq29455__$2);
}));

shadow.remote.runtime.shared.welcome = (function shadow$remote$runtime$shared$welcome(p__29515,p__29516){
var map__29517 = p__29515;
var map__29517__$1 = cljs.core.__destructure_map(map__29517);
var runtime = map__29517__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29517__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__29518 = p__29516;
var map__29518__$1 = cljs.core.__destructure_map(map__29518);
var msg = map__29518__$1;
var client_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29518__$1,new cljs.core.Keyword(null,"client-id","client-id",-464622140));
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,cljs.core.assoc,new cljs.core.Keyword(null,"client-id","client-id",-464622140),client_id);

var map__29525 = cljs.core.deref(state_ref);
var map__29525__$1 = cljs.core.__destructure_map(map__29525);
var client_info = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29525__$1,new cljs.core.Keyword(null,"client-info","client-info",1958982504));
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29525__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
shadow.remote.runtime.shared.relay_msg(runtime,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"hello","hello",-245025397),new cljs.core.Keyword(null,"client-info","client-info",1958982504),client_info], null));

return shadow.remote.runtime.shared.trigger_BANG_(runtime,new cljs.core.Keyword(null,"on-welcome","on-welcome",1895317125));
});
shadow.remote.runtime.shared.ping = (function shadow$remote$runtime$shared$ping(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"pong","pong",-172484958)], null));
});
shadow.remote.runtime.shared.request_supported_ops = (function shadow$remote$runtime$shared$request_supported_ops(p__29535,msg){
var map__29552 = p__29535;
var map__29552__$1 = cljs.core.__destructure_map(map__29552);
var runtime = map__29552__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29552__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"supported-ops","supported-ops",337914702),new cljs.core.Keyword(null,"ops","ops",1237330063),cljs.core.disj.cljs$core$IFn$_invoke$arity$variadic(cljs.core.set(cljs.core.keys(new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref)))),new cljs.core.Keyword(null,"welcome","welcome",-578152123),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),new cljs.core.Keyword(null,"tool-disconnect","tool-disconnect",189103996)], 0))], null));
});
shadow.remote.runtime.shared.unknown_relay_op = (function shadow$remote$runtime$shared$unknown_relay_op(msg){
return console.warn("unknown-relay-op",msg);
});
shadow.remote.runtime.shared.unknown_op = (function shadow$remote$runtime$shared$unknown_op(msg){
return console.warn("unknown-op",msg);
});
shadow.remote.runtime.shared.add_extension_STAR_ = (function shadow$remote$runtime$shared$add_extension_STAR_(p__29569,key,p__29570){
var map__29573 = p__29569;
var map__29573__$1 = cljs.core.__destructure_map(map__29573);
var state = map__29573__$1;
var extensions = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29573__$1,new cljs.core.Keyword(null,"extensions","extensions",-1103629196));
var map__29574 = p__29570;
var map__29574__$1 = cljs.core.__destructure_map(map__29574);
var spec = map__29574__$1;
var ops = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29574__$1,new cljs.core.Keyword(null,"ops","ops",1237330063));
if(cljs.core.contains_QMARK_(extensions,key)){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("extension already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"spec","spec",347520401),spec], null));
} else {
}

return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
if(cljs.core.truth_(cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null)))){
throw cljs.core.ex_info.cljs$core$IFn$_invoke$arity$2("op already registered",new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"key","key",-1516042587),key,new cljs.core.Keyword(null,"op","op",-1882987955),op_kw], null));
} else {
}

return cljs.core.assoc_in(state__$1,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op_kw], null),op_handler);
}),cljs.core.assoc_in(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null),spec),ops);
});
shadow.remote.runtime.shared.add_extension = (function shadow$remote$runtime$shared$add_extension(p__29586,key,spec){
var map__29589 = p__29586;
var map__29589__$1 = cljs.core.__destructure_map(map__29589);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29589__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(state_ref,shadow.remote.runtime.shared.add_extension_STAR_,key,spec);
});
shadow.remote.runtime.shared.add_defaults = (function shadow$remote$runtime$shared$add_defaults(runtime){
return shadow.remote.runtime.shared.add_extension(runtime,new cljs.core.Keyword("shadow.remote.runtime.shared","defaults","shadow.remote.runtime.shared/defaults",-1821257543),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"ops","ops",1237330063),new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"welcome","welcome",-578152123),(function (p1__29590_SHARP_){
return shadow.remote.runtime.shared.welcome(runtime,p1__29590_SHARP_);
}),new cljs.core.Keyword(null,"unknown-relay-op","unknown-relay-op",170832753),(function (p1__29591_SHARP_){
return shadow.remote.runtime.shared.unknown_relay_op(p1__29591_SHARP_);
}),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),(function (p1__29592_SHARP_){
return shadow.remote.runtime.shared.unknown_op(p1__29592_SHARP_);
}),new cljs.core.Keyword(null,"ping","ping",-1670114784),(function (p1__29594_SHARP_){
return shadow.remote.runtime.shared.ping(runtime,p1__29594_SHARP_);
}),new cljs.core.Keyword(null,"request-supported-ops","request-supported-ops",-1034994502),(function (p1__29595_SHARP_){
return shadow.remote.runtime.shared.request_supported_ops(runtime,p1__29595_SHARP_);
})], null)], null));
});
shadow.remote.runtime.shared.del_extension_STAR_ = (function shadow$remote$runtime$shared$del_extension_STAR_(state,key){
var ext = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"extensions","extensions",-1103629196),key], null));
if(cljs.core.not(ext)){
return state;
} else {
return cljs.core.reduce_kv((function (state__$1,op_kw,op_handler){
return cljs.core.update_in.cljs$core$IFn$_invoke$arity$4(state__$1,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063)], null),cljs.core.dissoc,op_kw);
}),cljs.core.update.cljs$core$IFn$_invoke$arity$4(state,new cljs.core.Keyword(null,"extensions","extensions",-1103629196),cljs.core.dissoc,key),new cljs.core.Keyword(null,"ops","ops",1237330063).cljs$core$IFn$_invoke$arity$1(ext));
}
});
shadow.remote.runtime.shared.del_extension = (function shadow$remote$runtime$shared$del_extension(p__29611,key){
var map__29612 = p__29611;
var map__29612__$1 = cljs.core.__destructure_map(map__29612);
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29612__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(state_ref,shadow.remote.runtime.shared.del_extension_STAR_,key);
});
shadow.remote.runtime.shared.unhandled_call_result = (function shadow$remote$runtime$shared$unhandled_call_result(call_config,msg){
return console.warn("unhandled call result",msg,call_config);
});
shadow.remote.runtime.shared.unhandled_client_not_found = (function shadow$remote$runtime$shared$unhandled_client_not_found(p__29633,msg){
var map__29636 = p__29633;
var map__29636__$1 = cljs.core.__destructure_map(map__29636);
var runtime = map__29636__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29636__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
return shadow.remote.runtime.shared.trigger_BANG_.cljs$core$IFn$_invoke$arity$variadic(runtime,new cljs.core.Keyword(null,"on-client-not-found","on-client-not-found",-642452849),cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([msg], 0));
});
shadow.remote.runtime.shared.reply_unknown_op = (function shadow$remote$runtime$shared$reply_unknown_op(runtime,msg){
return shadow.remote.runtime.shared.reply(runtime,msg,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"op","op",-1882987955),new cljs.core.Keyword(null,"unknown-op","unknown-op",1900385996),new cljs.core.Keyword(null,"msg","msg",-1386103444),msg], null));
});
shadow.remote.runtime.shared.process = (function shadow$remote$runtime$shared$process(p__29648,p__29649){
var map__29650 = p__29648;
var map__29650__$1 = cljs.core.__destructure_map(map__29650);
var runtime = map__29650__$1;
var state_ref = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29650__$1,new cljs.core.Keyword(null,"state-ref","state-ref",2127874952));
var map__29651 = p__29649;
var map__29651__$1 = cljs.core.__destructure_map(map__29651);
var msg = map__29651__$1;
var op = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29651__$1,new cljs.core.Keyword(null,"op","op",-1882987955));
var call_id = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29651__$1,new cljs.core.Keyword(null,"call-id","call-id",1043012968));
var state = cljs.core.deref(state_ref);
var op_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"ops","ops",1237330063),op], null));
if(cljs.core.truth_(call_id)){
var cfg = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(state,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),call_id], null));
var call_handler = cljs.core.get_in.cljs$core$IFn$_invoke$arity$2(cfg,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"handlers","handlers",79528781),op], null));
if(cljs.core.truth_(call_handler)){
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$variadic(state_ref,cljs.core.update,new cljs.core.Keyword(null,"call-handlers","call-handlers",386605551),cljs.core.dissoc,cljs.core.prim_seq.cljs$core$IFn$_invoke$arity$2([call_id], 0));

return (call_handler.cljs$core$IFn$_invoke$arity$1 ? call_handler.cljs$core$IFn$_invoke$arity$1(msg) : call_handler.call(null,msg));
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
return shadow.remote.runtime.shared.unhandled_call_result(cfg,msg);

}
}
} else {
if(cljs.core.truth_(op_handler)){
return (op_handler.cljs$core$IFn$_invoke$arity$1 ? op_handler.cljs$core$IFn$_invoke$arity$1(msg) : op_handler.call(null,msg));
} else {
if(cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(new cljs.core.Keyword(null,"client-not-found","client-not-found",-1754042614),op)){
return shadow.remote.runtime.shared.unhandled_client_not_found(runtime,msg);
} else {
return shadow.remote.runtime.shared.reply_unknown_op(runtime,msg);

}
}
}
});
shadow.remote.runtime.shared.run_on_idle = (function shadow$remote$runtime$shared$run_on_idle(state_ref){
var seq__29667 = cljs.core.seq(cljs.core.vals(new cljs.core.Keyword(null,"extensions","extensions",-1103629196).cljs$core$IFn$_invoke$arity$1(cljs.core.deref(state_ref))));
var chunk__29669 = null;
var count__29670 = (0);
var i__29671 = (0);
while(true){
if((i__29671 < count__29670)){
var map__29705 = chunk__29669.cljs$core$IIndexed$_nth$arity$2(null,i__29671);
var map__29705__$1 = cljs.core.__destructure_map(map__29705);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29705__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__29948 = seq__29667;
var G__29949 = chunk__29669;
var G__29950 = count__29670;
var G__29951 = (i__29671 + (1));
seq__29667 = G__29948;
chunk__29669 = G__29949;
count__29670 = G__29950;
i__29671 = G__29951;
continue;
} else {
var G__29952 = seq__29667;
var G__29953 = chunk__29669;
var G__29954 = count__29670;
var G__29955 = (i__29671 + (1));
seq__29667 = G__29952;
chunk__29669 = G__29953;
count__29670 = G__29954;
i__29671 = G__29955;
continue;
}
} else {
var temp__5804__auto__ = cljs.core.seq(seq__29667);
if(temp__5804__auto__){
var seq__29667__$1 = temp__5804__auto__;
if(cljs.core.chunked_seq_QMARK_(seq__29667__$1)){
var c__5525__auto__ = cljs.core.chunk_first(seq__29667__$1);
var G__29956 = cljs.core.chunk_rest(seq__29667__$1);
var G__29957 = c__5525__auto__;
var G__29958 = cljs.core.count(c__5525__auto__);
var G__29959 = (0);
seq__29667 = G__29956;
chunk__29669 = G__29957;
count__29670 = G__29958;
i__29671 = G__29959;
continue;
} else {
var map__29734 = cljs.core.first(seq__29667__$1);
var map__29734__$1 = cljs.core.__destructure_map(map__29734);
var on_idle = cljs.core.get.cljs$core$IFn$_invoke$arity$2(map__29734__$1,new cljs.core.Keyword(null,"on-idle","on-idle",2044706602));
if(cljs.core.truth_(on_idle)){
(on_idle.cljs$core$IFn$_invoke$arity$0 ? on_idle.cljs$core$IFn$_invoke$arity$0() : on_idle.call(null));


var G__29962 = cljs.core.next(seq__29667__$1);
var G__29963 = null;
var G__29964 = (0);
var G__29965 = (0);
seq__29667 = G__29962;
chunk__29669 = G__29963;
count__29670 = G__29964;
i__29671 = G__29965;
continue;
} else {
var G__29967 = cljs.core.next(seq__29667__$1);
var G__29968 = null;
var G__29969 = (0);
var G__29970 = (0);
seq__29667 = G__29967;
chunk__29669 = G__29968;
count__29670 = G__29969;
i__29671 = G__29970;
continue;
}
}
} else {
return null;
}
}
break;
}
});

//# sourceMappingURL=shadow.remote.runtime.shared.js.map
