goog.provide('solo_borg.views.home');
solo_borg.views.home.view = (function solo_borg$views$home$view(props){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"div","div",1057191632),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"button.button","button.button",1464242525),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"on-click","on-click",1632826543),(function (){
return solo_borg.state.select_view(new cljs.core.Keyword(null,"character-creator","character-creator",-2104335526),cljs.core.PersistentArrayMap.EMPTY);
})], null),"Create Character"], null)], null);
});

//# sourceMappingURL=solo_borg.views.home.js.map
