goog.provide('solo_borg.entity_interactions');
if((typeof solo_borg !== 'undefined') && (typeof solo_borg.entity_interactions !== 'undefined') && (typeof solo_borg.entity_interactions.touch !== 'undefined')){
} else {
solo_borg.entity_interactions.touch = (function (){var method_table__5599__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__5600__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var method_cache__5601__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__5602__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__5603__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),(function (){var fexpr__33363 = cljs.core.get_global_hierarchy;
return (fexpr__33363.cljs$core$IFn$_invoke$arity$0 ? fexpr__33363.cljs$core$IFn$_invoke$arity$0() : fexpr__33363.call(null));
})());
return (new cljs.core.MultiFn(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("solo-borg.entity-interactions","touch"),(function (entity,target){
return new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(target);
}),new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__5603__auto__,method_table__5599__auto__,prefer_table__5600__auto__,method_cache__5601__auto__,cached_hierarchy__5602__auto__));
})();
}
solo_borg.entity_interactions.touch.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"wall","wall",-787661558),null], null), null),(function (entity,target){
cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1("you touched wall, it was cold.");

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"game-state-updates","game-state-updates",-362758743),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"update-in","update-in",1208918828),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"players","players",-1361554569),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(entity),new cljs.core.Keyword(null,"hp","hp",-1541237831),(0)], null),cljs.core.inc], null)], null),new cljs.core.Keyword(null,"app-state-updates","app-state-updates",213049343),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"update-in","update-in",1208918828),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"players","players",-1361554569),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(entity),new cljs.core.Keyword(null,"hp","hp",-1541237831),(0)], null),cljs.core.inc], null)], null)], null);
}));
solo_borg.entity_interactions.touch.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"default","default",-1987822328),(function (entity,target){
cljs.pprint.pprint.cljs$core$IFn$_invoke$arity$1("there is nothing to touch.");

return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"game-state-updates","game-state-updates",-362758743),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"update-in","update-in",1208918828),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"players","players",-1361554569),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(entity),new cljs.core.Keyword(null,"hp","hp",-1541237831),(0)], null),cljs.core.inc], null)], null),new cljs.core.Keyword(null,"app-state-updates","app-state-updates",213049343),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"update-in","update-in",1208918828),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"players","players",-1361554569),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(entity),new cljs.core.Keyword(null,"hp","hp",-1541237831),(0)], null),cljs.core.inc], null)], null)], null);
}));
solo_borg.entity_interactions.touch.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"wall","wall",-787661558),null,new cljs.core.Keyword(null,"door","door",-956406127),null], null), null),(function (entity,target){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"update-in","update-in",1208918828),new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"players","players",-1361554569),new cljs.core.Keyword(null,"id","id",-1388402092).cljs$core$IFn$_invoke$arity$1(entity),new cljs.core.Keyword(null,"hp","hp",-1541237831),(0)], null),cljs.core.inc], null)], null);
}));
if((typeof solo_borg !== 'undefined') && (typeof solo_borg.entity_interactions !== 'undefined') && (typeof solo_borg.entity_interactions.interact !== 'undefined')){
} else {
solo_borg.entity_interactions.interact = (function (){var method_table__5599__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var prefer_table__5600__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var method_cache__5601__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var cached_hierarchy__5602__auto__ = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var hierarchy__5603__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$3(cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"hierarchy","hierarchy",-1053470341),(function (){var fexpr__33384 = cljs.core.get_global_hierarchy;
return (fexpr__33384.cljs$core$IFn$_invoke$arity$0 ? fexpr__33384.cljs$core$IFn$_invoke$arity$0() : fexpr__33384.call(null));
})());
return (new cljs.core.MultiFn(cljs.core.symbol.cljs$core$IFn$_invoke$arity$2("solo-borg.entity-interactions","interact"),cljs.core.identity,new cljs.core.Keyword(null,"default","default",-1987822328),hierarchy__5603__auto__,method_table__5599__auto__,prefer_table__5600__auto__,method_cache__5601__auto__,cached_hierarchy__5602__auto__));
})();
}
solo_borg.entity_interactions.interact.cljs$core$IMultiFn$_add_method$arity$3(null,new cljs.core.Keyword(null,"touch","touch",1496272469),(function (_,entity,target){
return solo_borg.entity_interactions.touch.cljs$core$IFn$_invoke$arity$2(entity,target);
}));

//# sourceMappingURL=solo_borg.entity_interactions.js.map
