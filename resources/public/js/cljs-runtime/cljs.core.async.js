goog.provide('cljs.core.async');
goog.scope(function(){
  cljs.core.async.goog$module$goog$array = goog.module.get('goog.array');
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30400 = (function (f,blockable,meta30401){
this.f = f;
this.blockable = blockable;
this.meta30401 = meta30401;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async30400.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30402,meta30401__$1){
var self__ = this;
var _30402__$1 = this;
return (new cljs.core.async.t_cljs$core$async30400(self__.f,self__.blockable,meta30401__$1));
}));

(cljs.core.async.t_cljs$core$async30400.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30402){
var self__ = this;
var _30402__$1 = this;
return self__.meta30401;
}));

(cljs.core.async.t_cljs$core$async30400.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async30400.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async30400.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
}));

(cljs.core.async.t_cljs$core$async30400.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
}));

(cljs.core.async.t_cljs$core$async30400.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta30401","meta30401",-1220422127,null)], null);
}));

(cljs.core.async.t_cljs$core$async30400.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async30400.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30400");

(cljs.core.async.t_cljs$core$async30400.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async30400");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30400.
 */
cljs.core.async.__GT_t_cljs$core$async30400 = (function cljs$core$async$__GT_t_cljs$core$async30400(f,blockable,meta30401){
return (new cljs.core.async.t_cljs$core$async30400(f,blockable,meta30401));
});


cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var G__30390 = arguments.length;
switch (G__30390) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(f,true);
}));

(cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
return (new cljs.core.async.t_cljs$core$async30400(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
}));

(cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2);

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer(n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer(n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if((!((buff == null)))){
if(((false) || ((cljs.core.PROTOCOL_SENTINEL === buff.cljs$core$async$impl$protocols$UnblockingBuffer$)))){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_(cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var G__30424 = arguments.length;
switch (G__30424) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,null,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(buf_or_n,xform,null);
}));

(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error(["Assert failed: ","buffer must be supplied when transducer is","\n","buf-or-n"].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.cljs$core$IFn$_invoke$arity$3(((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer(buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
}));

(cljs.core.async.chan.cljs$lang$maxFixedArity = 3);

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var G__30433 = arguments.length;
switch (G__30433) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1(null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2(xform,null);
}));

(cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3(cljs.core.async.impl.buffers.promise_buffer(),xform,ex_handler);
}));

(cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout(msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var G__30440 = arguments.length;
switch (G__30440) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3(port,fn1,true);
}));

(cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(ret)){
var val_33995 = cljs.core.deref(ret);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_33995) : fn1.call(null,val_33995));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(val_33995) : fn1.call(null,val_33995));
}));
}
} else {
}

return null;
}));

(cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3);

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn1 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn1 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var G__30451 = arguments.length;
switch (G__30451) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__5802__auto__)){
var ret = temp__5802__auto__;
return cljs.core.deref(ret);
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4(port,val,fn1,true);
}));

(cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__5802__auto__ = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1(fn1));
if(cljs.core.truth_(temp__5802__auto__)){
var retb = temp__5802__auto__;
var ret = cljs.core.deref(retb);
if(cljs.core.truth_(on_caller_QMARK_)){
(fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
} else {
cljs.core.async.impl.dispatch.run((function (){
return (fn1.cljs$core$IFn$_invoke$arity$1 ? fn1.cljs$core$IFn$_invoke$arity$1(ret) : fn1.call(null,ret));
}));
}

return ret;
} else {
return true;
}
}));

(cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4);

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_(port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__5593__auto___34010 = n;
var x_34011 = (0);
while(true){
if((x_34011 < n__5593__auto___34010)){
(a[x_34011] = x_34011);

var G__34014 = (x_34011 + (1));
x_34011 = G__34014;
continue;
} else {
}
break;
}

cljs.core.async.goog$module$goog$array.shuffle(a);

return a;
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30455 = (function (flag,meta30456){
this.flag = flag;
this.meta30456 = meta30456;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async30455.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30457,meta30456__$1){
var self__ = this;
var _30457__$1 = this;
return (new cljs.core.async.t_cljs$core$async30455(self__.flag,meta30456__$1));
}));

(cljs.core.async.t_cljs$core$async30455.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30457){
var self__ = this;
var _30457__$1 = this;
return self__.meta30456;
}));

(cljs.core.async.t_cljs$core$async30455.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async30455.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref(self__.flag);
}));

(cljs.core.async.t_cljs$core$async30455.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async30455.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.flag,null);

return true;
}));

(cljs.core.async.t_cljs$core$async30455.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta30456","meta30456",-1085412872,null)], null);
}));

(cljs.core.async.t_cljs$core$async30455.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async30455.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30455");

(cljs.core.async.t_cljs$core$async30455.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async30455");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30455.
 */
cljs.core.async.__GT_t_cljs$core$async30455 = (function cljs$core$async$__GT_t_cljs$core$async30455(flag,meta30456){
return (new cljs.core.async.t_cljs$core$async30455(flag,meta30456));
});


cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(true);
return (new cljs.core.async.t_cljs$core$async30455(flag,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30483 = (function (flag,cb,meta30484){
this.flag = flag;
this.cb = cb;
this.meta30484 = meta30484;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async30483.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30485,meta30484__$1){
var self__ = this;
var _30485__$1 = this;
return (new cljs.core.async.t_cljs$core$async30483(self__.flag,self__.cb,meta30484__$1));
}));

(cljs.core.async.t_cljs$core$async30483.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30485){
var self__ = this;
var _30485__$1 = this;
return self__.meta30484;
}));

(cljs.core.async.t_cljs$core$async30483.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async30483.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.flag);
}));

(cljs.core.async.t_cljs$core$async30483.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async30483.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit(self__.flag);

return self__.cb;
}));

(cljs.core.async.t_cljs$core$async30483.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta30484","meta30484",783638528,null)], null);
}));

(cljs.core.async.t_cljs$core$async30483.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async30483.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30483");

(cljs.core.async.t_cljs$core$async30483.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async30483");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async30483.
 */
cljs.core.async.__GT_t_cljs$core$async30483 = (function cljs$core$async$__GT_t_cljs$core$async30483(flag,cb,meta30484){
return (new cljs.core.async.t_cljs$core$async30483(flag,cb,meta30484));
});


cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
return (new cljs.core.async.t_cljs$core$async30483(flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
if((cljs.core.count(ports) > (0))){
} else {
throw (new Error(["Assert failed: ","alts must have at least one channel operation","\n","(pos? (count ports))"].join('')));
}

var flag = cljs.core.async.alt_flag();
var n = cljs.core.count(ports);
var idxs = cljs.core.async.random_array(n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.cljs$core$IFn$_invoke$arity$2(ports,idx);
var wport = ((cljs.core.vector_QMARK_(port))?(port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((0)) : port.call(null,(0))):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = (port.cljs$core$IFn$_invoke$arity$1 ? port.cljs$core$IFn$_invoke$arity$1((1)) : port.call(null,(1)));
return cljs.core.async.impl.protocols.put_BANG_(wport,val,cljs.core.async.alt_handler(flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30498_SHARP_){
var G__30507 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30498_SHARP_,wport], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__30507) : fret.call(null,G__30507));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.alt_handler(flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__30499_SHARP_){
var G__30508 = new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__30499_SHARP_,port], null);
return (fret.cljs$core$IFn$_invoke$arity$1 ? fret.cljs$core$IFn$_invoke$arity$1(G__30508) : fret.call(null,G__30508));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref(vbox),(function (){var or__5002__auto__ = wport;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return port;
}
})()], null));
} else {
var G__34033 = (i + (1));
i = G__34033;
continue;
}
} else {
return null;
}
break;
}
})();
var or__5002__auto__ = ret;
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
if(cljs.core.contains_QMARK_(opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__5804__auto__ = (function (){var and__5000__auto__ = flag.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1(null);
if(cljs.core.truth_(and__5000__auto__)){
return flag.cljs$core$async$impl$protocols$Handler$commit$arity$1(null);
} else {
return and__5000__auto__;
}
})();
if(cljs.core.truth_(temp__5804__auto__)){
var got = temp__5804__auto__;
return cljs.core.async.impl.channels.box(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___34038 = arguments.length;
var i__5727__auto___34039 = (0);
while(true){
if((i__5727__auto___34039 < len__5726__auto___34038)){
args__5732__auto__.push((arguments[i__5727__auto___34039]));

var G__34041 = (i__5727__auto___34039 + (1));
i__5727__auto___34039 = G__34041;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((1) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__5733__auto__);
});

(cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__30514){
var map__30516 = p__30514;
var map__30516__$1 = cljs.core.__destructure_map(map__30516);
var opts = map__30516__$1;
throw (new Error("alts! used not in (go ...) block"));
}));

(cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1));

/** @this {Function} */
(cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq30510){
var G__30511 = cljs.core.first(seq30510);
var seq30510__$1 = cljs.core.next(seq30510);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__30511,seq30510__$1);
}));

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_(port,val,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_(port,cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2(cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref(ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var G__30531 = arguments.length;
switch (G__30531) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3(from,to,true);
}));

(cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__30228__auto___34057 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_30583){
var state_val_30584 = (state_30583[(1)]);
if((state_val_30584 === (7))){
var inst_30575 = (state_30583[(2)]);
var state_30583__$1 = state_30583;
var statearr_30589_34064 = state_30583__$1;
(statearr_30589_34064[(2)] = inst_30575);

(statearr_30589_34064[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (1))){
var state_30583__$1 = state_30583;
var statearr_30590_34065 = state_30583__$1;
(statearr_30590_34065[(2)] = null);

(statearr_30590_34065[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (4))){
var inst_30554 = (state_30583[(7)]);
var inst_30554__$1 = (state_30583[(2)]);
var inst_30555 = (inst_30554__$1 == null);
var state_30583__$1 = (function (){var statearr_30593 = state_30583;
(statearr_30593[(7)] = inst_30554__$1);

return statearr_30593;
})();
if(cljs.core.truth_(inst_30555)){
var statearr_30594_34072 = state_30583__$1;
(statearr_30594_34072[(1)] = (5));

} else {
var statearr_30598_34073 = state_30583__$1;
(statearr_30598_34073[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (13))){
var state_30583__$1 = state_30583;
var statearr_30601_34078 = state_30583__$1;
(statearr_30601_34078[(2)] = null);

(statearr_30601_34078[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (6))){
var inst_30554 = (state_30583[(7)]);
var state_30583__$1 = state_30583;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30583__$1,(11),to,inst_30554);
} else {
if((state_val_30584 === (3))){
var inst_30577 = (state_30583[(2)]);
var state_30583__$1 = state_30583;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30583__$1,inst_30577);
} else {
if((state_val_30584 === (12))){
var state_30583__$1 = state_30583;
var statearr_30602_34088 = state_30583__$1;
(statearr_30602_34088[(2)] = null);

(statearr_30602_34088[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (2))){
var state_30583__$1 = state_30583;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30583__$1,(4),from);
} else {
if((state_val_30584 === (11))){
var inst_30565 = (state_30583[(2)]);
var state_30583__$1 = state_30583;
if(cljs.core.truth_(inst_30565)){
var statearr_30603_34090 = state_30583__$1;
(statearr_30603_34090[(1)] = (12));

} else {
var statearr_30606_34091 = state_30583__$1;
(statearr_30606_34091[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (9))){
var state_30583__$1 = state_30583;
var statearr_30607_34095 = state_30583__$1;
(statearr_30607_34095[(2)] = null);

(statearr_30607_34095[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (5))){
var state_30583__$1 = state_30583;
if(cljs.core.truth_(close_QMARK_)){
var statearr_30608_34100 = state_30583__$1;
(statearr_30608_34100[(1)] = (8));

} else {
var statearr_30609_34102 = state_30583__$1;
(statearr_30609_34102[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (14))){
var inst_30573 = (state_30583[(2)]);
var state_30583__$1 = state_30583;
var statearr_30610_34107 = state_30583__$1;
(statearr_30610_34107[(2)] = inst_30573);

(statearr_30610_34107[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (10))){
var inst_30561 = (state_30583[(2)]);
var state_30583__$1 = state_30583;
var statearr_30612_34108 = state_30583__$1;
(statearr_30612_34108[(2)] = inst_30561);

(statearr_30612_34108[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30584 === (8))){
var inst_30558 = cljs.core.async.close_BANG_(to);
var state_30583__$1 = state_30583;
var statearr_30614_34109 = state_30583__$1;
(statearr_30614_34109[(2)] = inst_30558);

(statearr_30614_34109[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_30615 = [null,null,null,null,null,null,null,null];
(statearr_30615[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_30615[(1)] = (1));

return statearr_30615;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_30583){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_30583);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e30616){var ex__29321__auto__ = e30616;
var statearr_30617_34111 = state_30583;
(statearr_30617_34111[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_30583[(4)]))){
var statearr_30622_34116 = state_30583;
(statearr_30622_34116[(1)] = cljs.core.first((state_30583[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34120 = state_30583;
state_30583 = G__34120;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_30583){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_30583);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_30625 = f__30229__auto__();
(statearr_30625[(6)] = c__30228__auto___34057);

return statearr_30625;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return to;
}));

(cljs.core.async.pipe.cljs$lang$maxFixedArity = 3);

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var results = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(n);
var process__$1 = (function (p__30628){
var vec__30630 = p__30628;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30630,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30630,(1),null);
var job = vec__30630;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((1),xf,ex_handler);
var c__30228__auto___34145 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_30640){
var state_val_30641 = (state_30640[(1)]);
if((state_val_30641 === (1))){
var state_30640__$1 = state_30640;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30640__$1,(2),res,v);
} else {
if((state_val_30641 === (2))){
var inst_30637 = (state_30640[(2)]);
var inst_30638 = cljs.core.async.close_BANG_(res);
var state_30640__$1 = (function (){var statearr_30645 = state_30640;
(statearr_30645[(7)] = inst_30637);

return statearr_30645;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_30640__$1,inst_30638);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0 = (function (){
var statearr_30649 = [null,null,null,null,null,null,null,null];
(statearr_30649[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__);

(statearr_30649[(1)] = (1));

return statearr_30649;
});
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1 = (function (state_30640){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_30640);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e30652){var ex__29321__auto__ = e30652;
var statearr_30654_34168 = state_30640;
(statearr_30654_34168[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_30640[(4)]))){
var statearr_30655_34170 = state_30640;
(statearr_30655_34170[(1)] = cljs.core.first((state_30640[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34171 = state_30640;
state_30640 = G__34171;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = function(state_30640){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1.call(this,state_30640);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_30657 = f__30229__auto__();
(statearr_30657[(6)] = c__30228__auto___34145);

return statearr_30657;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var async = (function (p__30663){
var vec__30664 = p__30663;
var v = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30664,(0),null);
var p = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(vec__30664,(1),null);
var job = vec__30664;
if((job == null)){
cljs.core.async.close_BANG_(results);

return null;
} else {
var res = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
(xf.cljs$core$IFn$_invoke$arity$2 ? xf.cljs$core$IFn$_invoke$arity$2(v,res) : xf.call(null,v,res));

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(p,res);

return true;
}
});
var n__5593__auto___34182 = n;
var __34183 = (0);
while(true){
if((__34183 < n__5593__auto___34182)){
var G__30667_34184 = type;
var G__30667_34185__$1 = (((G__30667_34184 instanceof cljs.core.Keyword))?G__30667_34184.fqn:null);
switch (G__30667_34185__$1) {
case "compute":
var c__30228__auto___34187 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__34183,c__30228__auto___34187,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async){
return (function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = ((function (__34183,c__30228__auto___34187,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async){
return (function (state_30681){
var state_val_30682 = (state_30681[(1)]);
if((state_val_30682 === (1))){
var state_30681__$1 = state_30681;
var statearr_30687_34194 = state_30681__$1;
(statearr_30687_34194[(2)] = null);

(statearr_30687_34194[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30682 === (2))){
var state_30681__$1 = state_30681;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30681__$1,(4),jobs);
} else {
if((state_val_30682 === (3))){
var inst_30679 = (state_30681[(2)]);
var state_30681__$1 = state_30681;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30681__$1,inst_30679);
} else {
if((state_val_30682 === (4))){
var inst_30670 = (state_30681[(2)]);
var inst_30672 = process__$1(inst_30670);
var state_30681__$1 = state_30681;
if(cljs.core.truth_(inst_30672)){
var statearr_30695_34204 = state_30681__$1;
(statearr_30695_34204[(1)] = (5));

} else {
var statearr_30697_34205 = state_30681__$1;
(statearr_30697_34205[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30682 === (5))){
var state_30681__$1 = state_30681;
var statearr_30698_34206 = state_30681__$1;
(statearr_30698_34206[(2)] = null);

(statearr_30698_34206[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30682 === (6))){
var state_30681__$1 = state_30681;
var statearr_30703_34215 = state_30681__$1;
(statearr_30703_34215[(2)] = null);

(statearr_30703_34215[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30682 === (7))){
var inst_30677 = (state_30681[(2)]);
var state_30681__$1 = state_30681;
var statearr_30705_34217 = state_30681__$1;
(statearr_30705_34217[(2)] = inst_30677);

(statearr_30705_34217[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__34183,c__30228__auto___34187,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async))
;
return ((function (__34183,switch__29317__auto__,c__30228__auto___34187,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0 = (function (){
var statearr_30712 = [null,null,null,null,null,null,null];
(statearr_30712[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__);

(statearr_30712[(1)] = (1));

return statearr_30712;
});
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1 = (function (state_30681){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_30681);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e30713){var ex__29321__auto__ = e30713;
var statearr_30714_34309 = state_30681;
(statearr_30714_34309[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_30681[(4)]))){
var statearr_30716_34310 = state_30681;
(statearr_30716_34310[(1)] = cljs.core.first((state_30681[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34313 = state_30681;
state_30681 = G__34313;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = function(state_30681){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1.call(this,state_30681);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__;
})()
;})(__34183,switch__29317__auto__,c__30228__auto___34187,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async))
})();
var state__30230__auto__ = (function (){var statearr_30720 = f__30229__auto__();
(statearr_30720[(6)] = c__30228__auto___34187);

return statearr_30720;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
});})(__34183,c__30228__auto___34187,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async))
);


break;
case "async":
var c__30228__auto___34320 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run(((function (__34183,c__30228__auto___34320,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async){
return (function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = ((function (__34183,c__30228__auto___34320,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async){
return (function (state_30737){
var state_val_30738 = (state_30737[(1)]);
if((state_val_30738 === (1))){
var state_30737__$1 = state_30737;
var statearr_30747_34325 = state_30737__$1;
(statearr_30747_34325[(2)] = null);

(statearr_30747_34325[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30738 === (2))){
var state_30737__$1 = state_30737;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30737__$1,(4),jobs);
} else {
if((state_val_30738 === (3))){
var inst_30735 = (state_30737[(2)]);
var state_30737__$1 = state_30737;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30737__$1,inst_30735);
} else {
if((state_val_30738 === (4))){
var inst_30724 = (state_30737[(2)]);
var inst_30726 = async(inst_30724);
var state_30737__$1 = state_30737;
if(cljs.core.truth_(inst_30726)){
var statearr_30750_34327 = state_30737__$1;
(statearr_30750_34327[(1)] = (5));

} else {
var statearr_30753_34329 = state_30737__$1;
(statearr_30753_34329[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30738 === (5))){
var state_30737__$1 = state_30737;
var statearr_30755_34330 = state_30737__$1;
(statearr_30755_34330[(2)] = null);

(statearr_30755_34330[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30738 === (6))){
var state_30737__$1 = state_30737;
var statearr_30761_34332 = state_30737__$1;
(statearr_30761_34332[(2)] = null);

(statearr_30761_34332[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30738 === (7))){
var inst_30732 = (state_30737[(2)]);
var state_30737__$1 = state_30737;
var statearr_30764_34335 = state_30737__$1;
(statearr_30764_34335[(2)] = inst_30732);

(statearr_30764_34335[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__34183,c__30228__auto___34320,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async))
;
return ((function (__34183,switch__29317__auto__,c__30228__auto___34320,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0 = (function (){
var statearr_30768 = [null,null,null,null,null,null,null];
(statearr_30768[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__);

(statearr_30768[(1)] = (1));

return statearr_30768;
});
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1 = (function (state_30737){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_30737);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e30769){var ex__29321__auto__ = e30769;
var statearr_30772_34346 = state_30737;
(statearr_30772_34346[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_30737[(4)]))){
var statearr_30774_34352 = state_30737;
(statearr_30774_34352[(1)] = cljs.core.first((state_30737[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34353 = state_30737;
state_30737 = G__34353;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = function(state_30737){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1.call(this,state_30737);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__;
})()
;})(__34183,switch__29317__auto__,c__30228__auto___34320,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async))
})();
var state__30230__auto__ = (function (){var statearr_30777 = f__30229__auto__();
(statearr_30777[(6)] = c__30228__auto___34320);

return statearr_30777;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
});})(__34183,c__30228__auto___34320,G__30667_34184,G__30667_34185__$1,n__5593__auto___34182,jobs,results,process__$1,async))
);


break;
default:
throw (new Error(["No matching clause: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(G__30667_34185__$1)].join('')));

}

var G__34361 = (__34183 + (1));
__34183 = G__34361;
continue;
} else {
}
break;
}

var c__30228__auto___34363 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_30811){
var state_val_30812 = (state_30811[(1)]);
if((state_val_30812 === (7))){
var inst_30807 = (state_30811[(2)]);
var state_30811__$1 = state_30811;
var statearr_30821_34364 = state_30811__$1;
(statearr_30821_34364[(2)] = inst_30807);

(statearr_30821_34364[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30812 === (1))){
var state_30811__$1 = state_30811;
var statearr_30823_34370 = state_30811__$1;
(statearr_30823_34370[(2)] = null);

(statearr_30823_34370[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30812 === (4))){
var inst_30788 = (state_30811[(7)]);
var inst_30788__$1 = (state_30811[(2)]);
var inst_30789 = (inst_30788__$1 == null);
var state_30811__$1 = (function (){var statearr_30827 = state_30811;
(statearr_30827[(7)] = inst_30788__$1);

return statearr_30827;
})();
if(cljs.core.truth_(inst_30789)){
var statearr_30830_34378 = state_30811__$1;
(statearr_30830_34378[(1)] = (5));

} else {
var statearr_30833_34381 = state_30811__$1;
(statearr_30833_34381[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30812 === (6))){
var inst_30788 = (state_30811[(7)]);
var inst_30795 = (state_30811[(8)]);
var inst_30795__$1 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var inst_30797 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_30798 = [inst_30788,inst_30795__$1];
var inst_30799 = (new cljs.core.PersistentVector(null,2,(5),inst_30797,inst_30798,null));
var state_30811__$1 = (function (){var statearr_30838 = state_30811;
(statearr_30838[(8)] = inst_30795__$1);

return statearr_30838;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30811__$1,(8),jobs,inst_30799);
} else {
if((state_val_30812 === (3))){
var inst_30809 = (state_30811[(2)]);
var state_30811__$1 = state_30811;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30811__$1,inst_30809);
} else {
if((state_val_30812 === (2))){
var state_30811__$1 = state_30811;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30811__$1,(4),from);
} else {
if((state_val_30812 === (9))){
var inst_30804 = (state_30811[(2)]);
var state_30811__$1 = (function (){var statearr_30843 = state_30811;
(statearr_30843[(9)] = inst_30804);

return statearr_30843;
})();
var statearr_30846_34391 = state_30811__$1;
(statearr_30846_34391[(2)] = null);

(statearr_30846_34391[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30812 === (5))){
var inst_30791 = cljs.core.async.close_BANG_(jobs);
var state_30811__$1 = state_30811;
var statearr_30848_34394 = state_30811__$1;
(statearr_30848_34394[(2)] = inst_30791);

(statearr_30848_34394[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30812 === (8))){
var inst_30795 = (state_30811[(8)]);
var inst_30801 = (state_30811[(2)]);
var state_30811__$1 = (function (){var statearr_30851 = state_30811;
(statearr_30851[(10)] = inst_30801);

return statearr_30851;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30811__$1,(9),results,inst_30795);
} else {
return null;
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0 = (function (){
var statearr_30858 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_30858[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__);

(statearr_30858[(1)] = (1));

return statearr_30858;
});
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1 = (function (state_30811){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_30811);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e30859){var ex__29321__auto__ = e30859;
var statearr_30862_34401 = state_30811;
(statearr_30862_34401[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_30811[(4)]))){
var statearr_30863_34403 = state_30811;
(statearr_30863_34403[(1)] = cljs.core.first((state_30811[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34405 = state_30811;
state_30811 = G__34405;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = function(state_30811){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1.call(this,state_30811);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_30867 = f__30229__auto__();
(statearr_30867[(6)] = c__30228__auto___34363);

return statearr_30867;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


var c__30228__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_30932){
var state_val_30933 = (state_30932[(1)]);
if((state_val_30933 === (7))){
var inst_30927 = (state_30932[(2)]);
var state_30932__$1 = state_30932;
var statearr_30946_34410 = state_30932__$1;
(statearr_30946_34410[(2)] = inst_30927);

(statearr_30946_34410[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (20))){
var state_30932__$1 = state_30932;
var statearr_30949_34413 = state_30932__$1;
(statearr_30949_34413[(2)] = null);

(statearr_30949_34413[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (1))){
var state_30932__$1 = state_30932;
var statearr_30956_34415 = state_30932__$1;
(statearr_30956_34415[(2)] = null);

(statearr_30956_34415[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (4))){
var inst_30877 = (state_30932[(7)]);
var inst_30877__$1 = (state_30932[(2)]);
var inst_30879 = (inst_30877__$1 == null);
var state_30932__$1 = (function (){var statearr_30962 = state_30932;
(statearr_30962[(7)] = inst_30877__$1);

return statearr_30962;
})();
if(cljs.core.truth_(inst_30879)){
var statearr_30965_34418 = state_30932__$1;
(statearr_30965_34418[(1)] = (5));

} else {
var statearr_30966_34419 = state_30932__$1;
(statearr_30966_34419[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (15))){
var inst_30905 = (state_30932[(8)]);
var state_30932__$1 = state_30932;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_30932__$1,(18),to,inst_30905);
} else {
if((state_val_30933 === (21))){
var inst_30921 = (state_30932[(2)]);
var state_30932__$1 = state_30932;
var statearr_30972_34420 = state_30932__$1;
(statearr_30972_34420[(2)] = inst_30921);

(statearr_30972_34420[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (13))){
var inst_30923 = (state_30932[(2)]);
var state_30932__$1 = (function (){var statearr_30975 = state_30932;
(statearr_30975[(9)] = inst_30923);

return statearr_30975;
})();
var statearr_30978_34422 = state_30932__$1;
(statearr_30978_34422[(2)] = null);

(statearr_30978_34422[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (6))){
var inst_30877 = (state_30932[(7)]);
var state_30932__$1 = state_30932;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30932__$1,(11),inst_30877);
} else {
if((state_val_30933 === (17))){
var inst_30916 = (state_30932[(2)]);
var state_30932__$1 = state_30932;
if(cljs.core.truth_(inst_30916)){
var statearr_30990_34424 = state_30932__$1;
(statearr_30990_34424[(1)] = (19));

} else {
var statearr_30991_34425 = state_30932__$1;
(statearr_30991_34425[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (3))){
var inst_30929 = (state_30932[(2)]);
var state_30932__$1 = state_30932;
return cljs.core.async.impl.ioc_helpers.return_chan(state_30932__$1,inst_30929);
} else {
if((state_val_30933 === (12))){
var inst_30889 = (state_30932[(10)]);
var state_30932__$1 = state_30932;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30932__$1,(14),inst_30889);
} else {
if((state_val_30933 === (2))){
var state_30932__$1 = state_30932;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_30932__$1,(4),results);
} else {
if((state_val_30933 === (19))){
var state_30932__$1 = state_30932;
var statearr_30999_34430 = state_30932__$1;
(statearr_30999_34430[(2)] = null);

(statearr_30999_34430[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (11))){
var inst_30889 = (state_30932[(2)]);
var state_30932__$1 = (function (){var statearr_31003 = state_30932;
(statearr_31003[(10)] = inst_30889);

return statearr_31003;
})();
var statearr_31007_34435 = state_30932__$1;
(statearr_31007_34435[(2)] = null);

(statearr_31007_34435[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (9))){
var state_30932__$1 = state_30932;
var statearr_31011_34436 = state_30932__$1;
(statearr_31011_34436[(2)] = null);

(statearr_31011_34436[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (5))){
var state_30932__$1 = state_30932;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31014_34441 = state_30932__$1;
(statearr_31014_34441[(1)] = (8));

} else {
var statearr_31015_34442 = state_30932__$1;
(statearr_31015_34442[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (14))){
var inst_30905 = (state_30932[(8)]);
var inst_30909 = (state_30932[(11)]);
var inst_30905__$1 = (state_30932[(2)]);
var inst_30908 = (inst_30905__$1 == null);
var inst_30909__$1 = cljs.core.not(inst_30908);
var state_30932__$1 = (function (){var statearr_31022 = state_30932;
(statearr_31022[(8)] = inst_30905__$1);

(statearr_31022[(11)] = inst_30909__$1);

return statearr_31022;
})();
if(inst_30909__$1){
var statearr_31025_34443 = state_30932__$1;
(statearr_31025_34443[(1)] = (15));

} else {
var statearr_31026_34444 = state_30932__$1;
(statearr_31026_34444[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (16))){
var inst_30909 = (state_30932[(11)]);
var state_30932__$1 = state_30932;
var statearr_31028_34445 = state_30932__$1;
(statearr_31028_34445[(2)] = inst_30909);

(statearr_31028_34445[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (10))){
var inst_30886 = (state_30932[(2)]);
var state_30932__$1 = state_30932;
var statearr_31030_34446 = state_30932__$1;
(statearr_31030_34446[(2)] = inst_30886);

(statearr_31030_34446[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (18))){
var inst_30913 = (state_30932[(2)]);
var state_30932__$1 = state_30932;
var statearr_31036_34447 = state_30932__$1;
(statearr_31036_34447[(2)] = inst_30913);

(statearr_31036_34447[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30933 === (8))){
var inst_30883 = cljs.core.async.close_BANG_(to);
var state_30932__$1 = state_30932;
var statearr_31041_34454 = state_30932__$1;
(statearr_31041_34454[(2)] = inst_30883);

(statearr_31041_34454[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0 = (function (){
var statearr_31049 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31049[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__);

(statearr_31049[(1)] = (1));

return statearr_31049;
});
var cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1 = (function (state_30932){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_30932);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e31054){var ex__29321__auto__ = e31054;
var statearr_31056_34466 = state_30932;
(statearr_31056_34466[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_30932[(4)]))){
var statearr_31057_34467 = state_30932;
(statearr_31057_34467[(1)] = cljs.core.first((state_30932[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34472 = state_30932;
state_30932 = G__34472;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__ = function(state_30932){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1.call(this,state_30932);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__29318__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_31064 = f__30229__auto__();
(statearr_31064[(6)] = c__30228__auto__);

return statearr_31064;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));

return c__30228__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). The
 *   presumption is that af will return immediately, having launched some
 *   asynchronous operation whose completion/callback will put results on
 *   the channel, then close! it. Outputs will be returned in order
 *   relative to the inputs. By default, the to channel will be closed
 *   when the from channel closes, but can be determined by the close?
 *   parameter. Will stop consuming the from channel if the to channel
 *   closes. See also pipeline, pipeline-blocking.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var G__31080 = arguments.length;
switch (G__31080) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5(n,to,af,from,true);
}));

(cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_(n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
}));

(cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5);

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var G__31102 = arguments.length;
switch (G__31102) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5(n,to,xf,from,true);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6(n,to,xf,from,close_QMARK_,null);
}));

(cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
}));

(cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6);

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var G__31130 = arguments.length;
switch (G__31130) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4(p,ch,null,null);
}));

(cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(t_buf_or_n);
var fc = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(f_buf_or_n);
var c__30228__auto___34489 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_31176){
var state_val_31177 = (state_31176[(1)]);
if((state_val_31177 === (7))){
var inst_31170 = (state_31176[(2)]);
var state_31176__$1 = state_31176;
var statearr_31189_34495 = state_31176__$1;
(statearr_31189_34495[(2)] = inst_31170);

(statearr_31189_34495[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (1))){
var state_31176__$1 = state_31176;
var statearr_31190_34498 = state_31176__$1;
(statearr_31190_34498[(2)] = null);

(statearr_31190_34498[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (4))){
var inst_31145 = (state_31176[(7)]);
var inst_31145__$1 = (state_31176[(2)]);
var inst_31147 = (inst_31145__$1 == null);
var state_31176__$1 = (function (){var statearr_31195 = state_31176;
(statearr_31195[(7)] = inst_31145__$1);

return statearr_31195;
})();
if(cljs.core.truth_(inst_31147)){
var statearr_31196_34504 = state_31176__$1;
(statearr_31196_34504[(1)] = (5));

} else {
var statearr_31197_34505 = state_31176__$1;
(statearr_31197_34505[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (13))){
var state_31176__$1 = state_31176;
var statearr_31202_34508 = state_31176__$1;
(statearr_31202_34508[(2)] = null);

(statearr_31202_34508[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (6))){
var inst_31145 = (state_31176[(7)]);
var inst_31153 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_31145) : p.call(null,inst_31145));
var state_31176__$1 = state_31176;
if(cljs.core.truth_(inst_31153)){
var statearr_31208_34510 = state_31176__$1;
(statearr_31208_34510[(1)] = (9));

} else {
var statearr_31213_34511 = state_31176__$1;
(statearr_31213_34511[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (3))){
var inst_31172 = (state_31176[(2)]);
var state_31176__$1 = state_31176;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31176__$1,inst_31172);
} else {
if((state_val_31177 === (12))){
var state_31176__$1 = state_31176;
var statearr_31218_34525 = state_31176__$1;
(statearr_31218_34525[(2)] = null);

(statearr_31218_34525[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (2))){
var state_31176__$1 = state_31176;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31176__$1,(4),ch);
} else {
if((state_val_31177 === (11))){
var inst_31145 = (state_31176[(7)]);
var inst_31159 = (state_31176[(2)]);
var state_31176__$1 = state_31176;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31176__$1,(8),inst_31159,inst_31145);
} else {
if((state_val_31177 === (9))){
var state_31176__$1 = state_31176;
var statearr_31226_34545 = state_31176__$1;
(statearr_31226_34545[(2)] = tc);

(statearr_31226_34545[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (5))){
var inst_31149 = cljs.core.async.close_BANG_(tc);
var inst_31150 = cljs.core.async.close_BANG_(fc);
var state_31176__$1 = (function (){var statearr_31231 = state_31176;
(statearr_31231[(8)] = inst_31149);

return statearr_31231;
})();
var statearr_31233_34556 = state_31176__$1;
(statearr_31233_34556[(2)] = inst_31150);

(statearr_31233_34556[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (14))){
var inst_31168 = (state_31176[(2)]);
var state_31176__$1 = state_31176;
var statearr_31234_34558 = state_31176__$1;
(statearr_31234_34558[(2)] = inst_31168);

(statearr_31234_34558[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (10))){
var state_31176__$1 = state_31176;
var statearr_31238_34562 = state_31176__$1;
(statearr_31238_34562[(2)] = fc);

(statearr_31238_34562[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31177 === (8))){
var inst_31162 = (state_31176[(2)]);
var state_31176__$1 = state_31176;
if(cljs.core.truth_(inst_31162)){
var statearr_31240_34563 = state_31176__$1;
(statearr_31240_34563[(1)] = (12));

} else {
var statearr_31241_34564 = state_31176__$1;
(statearr_31241_34564[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_31245 = [null,null,null,null,null,null,null,null,null];
(statearr_31245[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_31245[(1)] = (1));

return statearr_31245;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_31176){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_31176);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e31248){var ex__29321__auto__ = e31248;
var statearr_31249_34567 = state_31176;
(statearr_31249_34567[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_31176[(4)]))){
var statearr_31252_34571 = state_31176;
(statearr_31252_34571[(1)] = cljs.core.first((state_31176[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34576 = state_31176;
state_31176 = G__34576;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_31176){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_31176);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_31259 = f__30229__auto__();
(statearr_31259[(6)] = c__30228__auto___34489);

return statearr_31259;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
}));

(cljs.core.async.split.cljs$lang$maxFixedArity = 4);

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__30228__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_31289){
var state_val_31290 = (state_31289[(1)]);
if((state_val_31290 === (7))){
var inst_31285 = (state_31289[(2)]);
var state_31289__$1 = state_31289;
var statearr_31299_34589 = state_31289__$1;
(statearr_31299_34589[(2)] = inst_31285);

(statearr_31299_34589[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31290 === (1))){
var inst_31265 = init;
var inst_31266 = inst_31265;
var state_31289__$1 = (function (){var statearr_31300 = state_31289;
(statearr_31300[(7)] = inst_31266);

return statearr_31300;
})();
var statearr_31304_34597 = state_31289__$1;
(statearr_31304_34597[(2)] = null);

(statearr_31304_34597[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31290 === (4))){
var inst_31271 = (state_31289[(8)]);
var inst_31271__$1 = (state_31289[(2)]);
var inst_31273 = (inst_31271__$1 == null);
var state_31289__$1 = (function (){var statearr_31307 = state_31289;
(statearr_31307[(8)] = inst_31271__$1);

return statearr_31307;
})();
if(cljs.core.truth_(inst_31273)){
var statearr_31308_34606 = state_31289__$1;
(statearr_31308_34606[(1)] = (5));

} else {
var statearr_31309_34607 = state_31289__$1;
(statearr_31309_34607[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31290 === (6))){
var inst_31271 = (state_31289[(8)]);
var inst_31266 = (state_31289[(7)]);
var inst_31276 = (state_31289[(9)]);
var inst_31276__$1 = (f.cljs$core$IFn$_invoke$arity$2 ? f.cljs$core$IFn$_invoke$arity$2(inst_31266,inst_31271) : f.call(null,inst_31266,inst_31271));
var inst_31277 = cljs.core.reduced_QMARK_(inst_31276__$1);
var state_31289__$1 = (function (){var statearr_31314 = state_31289;
(statearr_31314[(9)] = inst_31276__$1);

return statearr_31314;
})();
if(inst_31277){
var statearr_31315_34615 = state_31289__$1;
(statearr_31315_34615[(1)] = (8));

} else {
var statearr_31316_34616 = state_31289__$1;
(statearr_31316_34616[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31290 === (3))){
var inst_31287 = (state_31289[(2)]);
var state_31289__$1 = state_31289;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31289__$1,inst_31287);
} else {
if((state_val_31290 === (2))){
var state_31289__$1 = state_31289;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31289__$1,(4),ch);
} else {
if((state_val_31290 === (9))){
var inst_31276 = (state_31289[(9)]);
var inst_31266 = inst_31276;
var state_31289__$1 = (function (){var statearr_31323 = state_31289;
(statearr_31323[(7)] = inst_31266);

return statearr_31323;
})();
var statearr_31326_34630 = state_31289__$1;
(statearr_31326_34630[(2)] = null);

(statearr_31326_34630[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31290 === (5))){
var inst_31266 = (state_31289[(7)]);
var state_31289__$1 = state_31289;
var statearr_31327_34633 = state_31289__$1;
(statearr_31327_34633[(2)] = inst_31266);

(statearr_31327_34633[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31290 === (10))){
var inst_31283 = (state_31289[(2)]);
var state_31289__$1 = state_31289;
var statearr_31331_34638 = state_31289__$1;
(statearr_31331_34638[(2)] = inst_31283);

(statearr_31331_34638[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31290 === (8))){
var inst_31276 = (state_31289[(9)]);
var inst_31279 = cljs.core.deref(inst_31276);
var state_31289__$1 = state_31289;
var statearr_31334_34639 = state_31289__$1;
(statearr_31334_34639[(2)] = inst_31279);

(statearr_31334_34639[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$reduce_$_state_machine__29318__auto__ = null;
var cljs$core$async$reduce_$_state_machine__29318__auto____0 = (function (){
var statearr_31336 = [null,null,null,null,null,null,null,null,null,null];
(statearr_31336[(0)] = cljs$core$async$reduce_$_state_machine__29318__auto__);

(statearr_31336[(1)] = (1));

return statearr_31336;
});
var cljs$core$async$reduce_$_state_machine__29318__auto____1 = (function (state_31289){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_31289);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e31342){var ex__29321__auto__ = e31342;
var statearr_31343_34640 = state_31289;
(statearr_31343_34640[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_31289[(4)]))){
var statearr_31350_34641 = state_31289;
(statearr_31350_34641[(1)] = cljs.core.first((state_31289[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34644 = state_31289;
state_31289 = G__34644;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__29318__auto__ = function(state_31289){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__29318__auto____1.call(this,state_31289);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__29318__auto____0;
cljs$core$async$reduce_$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__29318__auto____1;
return cljs$core$async$reduce_$_state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_31352 = f__30229__auto__();
(statearr_31352[(6)] = c__30228__auto__);

return statearr_31352;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));

return c__30228__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = (xform.cljs$core$IFn$_invoke$arity$1 ? xform.cljs$core$IFn$_invoke$arity$1(f) : xform.call(null,f));
var c__30228__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_31373){
var state_val_31374 = (state_31373[(1)]);
if((state_val_31374 === (1))){
var inst_31362 = cljs.core.async.reduce(f__$1,init,ch);
var state_31373__$1 = state_31373;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31373__$1,(2),inst_31362);
} else {
if((state_val_31374 === (2))){
var inst_31364 = (state_31373[(2)]);
var inst_31368 = (f__$1.cljs$core$IFn$_invoke$arity$1 ? f__$1.cljs$core$IFn$_invoke$arity$1(inst_31364) : f__$1.call(null,inst_31364));
var state_31373__$1 = state_31373;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31373__$1,inst_31368);
} else {
return null;
}
}
});
return (function() {
var cljs$core$async$transduce_$_state_machine__29318__auto__ = null;
var cljs$core$async$transduce_$_state_machine__29318__auto____0 = (function (){
var statearr_31383 = [null,null,null,null,null,null,null];
(statearr_31383[(0)] = cljs$core$async$transduce_$_state_machine__29318__auto__);

(statearr_31383[(1)] = (1));

return statearr_31383;
});
var cljs$core$async$transduce_$_state_machine__29318__auto____1 = (function (state_31373){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_31373);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e31386){var ex__29321__auto__ = e31386;
var statearr_31387_34672 = state_31373;
(statearr_31387_34672[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_31373[(4)]))){
var statearr_31388_34673 = state_31373;
(statearr_31388_34673[(1)] = cljs.core.first((state_31373[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34674 = state_31373;
state_31373 = G__34674;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__29318__auto__ = function(state_31373){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__29318__auto____1.call(this,state_31373);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__29318__auto____0;
cljs$core$async$transduce_$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__29318__auto____1;
return cljs$core$async$transduce_$_state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_31390 = f__30229__auto__();
(statearr_31390[(6)] = c__30228__auto__);

return statearr_31390;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));

return c__30228__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan_BANG_ = (function cljs$core$async$onto_chan_BANG_(var_args){
var G__31397 = arguments.length;
switch (G__31397) {
case 2:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__30228__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_31441){
var state_val_31442 = (state_31441[(1)]);
if((state_val_31442 === (7))){
var inst_31419 = (state_31441[(2)]);
var state_31441__$1 = state_31441;
var statearr_31450_34685 = state_31441__$1;
(statearr_31450_34685[(2)] = inst_31419);

(statearr_31450_34685[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (1))){
var inst_31410 = cljs.core.seq(coll);
var inst_31411 = inst_31410;
var state_31441__$1 = (function (){var statearr_31454 = state_31441;
(statearr_31454[(7)] = inst_31411);

return statearr_31454;
})();
var statearr_31458_34693 = state_31441__$1;
(statearr_31458_34693[(2)] = null);

(statearr_31458_34693[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (4))){
var inst_31411 = (state_31441[(7)]);
var inst_31417 = cljs.core.first(inst_31411);
var state_31441__$1 = state_31441;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_31441__$1,(7),ch,inst_31417);
} else {
if((state_val_31442 === (13))){
var inst_31433 = (state_31441[(2)]);
var state_31441__$1 = state_31441;
var statearr_31461_34698 = state_31441__$1;
(statearr_31461_34698[(2)] = inst_31433);

(statearr_31461_34698[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (6))){
var inst_31424 = (state_31441[(2)]);
var state_31441__$1 = state_31441;
if(cljs.core.truth_(inst_31424)){
var statearr_31464_34699 = state_31441__$1;
(statearr_31464_34699[(1)] = (8));

} else {
var statearr_31467_34700 = state_31441__$1;
(statearr_31467_34700[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (3))){
var inst_31438 = (state_31441[(2)]);
var state_31441__$1 = state_31441;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31441__$1,inst_31438);
} else {
if((state_val_31442 === (12))){
var state_31441__$1 = state_31441;
var statearr_31468_34706 = state_31441__$1;
(statearr_31468_34706[(2)] = null);

(statearr_31468_34706[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (2))){
var inst_31411 = (state_31441[(7)]);
var state_31441__$1 = state_31441;
if(cljs.core.truth_(inst_31411)){
var statearr_31469_34708 = state_31441__$1;
(statearr_31469_34708[(1)] = (4));

} else {
var statearr_31470_34709 = state_31441__$1;
(statearr_31470_34709[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (11))){
var inst_31430 = cljs.core.async.close_BANG_(ch);
var state_31441__$1 = state_31441;
var statearr_31472_34710 = state_31441__$1;
(statearr_31472_34710[(2)] = inst_31430);

(statearr_31472_34710[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (9))){
var state_31441__$1 = state_31441;
if(cljs.core.truth_(close_QMARK_)){
var statearr_31478_34716 = state_31441__$1;
(statearr_31478_34716[(1)] = (11));

} else {
var statearr_31480_34718 = state_31441__$1;
(statearr_31480_34718[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (5))){
var inst_31411 = (state_31441[(7)]);
var state_31441__$1 = state_31441;
var statearr_31483_34725 = state_31441__$1;
(statearr_31483_34725[(2)] = inst_31411);

(statearr_31483_34725[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (10))){
var inst_31435 = (state_31441[(2)]);
var state_31441__$1 = state_31441;
var statearr_31484_34727 = state_31441__$1;
(statearr_31484_34727[(2)] = inst_31435);

(statearr_31484_34727[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31442 === (8))){
var inst_31411 = (state_31441[(7)]);
var inst_31426 = cljs.core.next(inst_31411);
var inst_31411__$1 = inst_31426;
var state_31441__$1 = (function (){var statearr_31486 = state_31441;
(statearr_31486[(7)] = inst_31411__$1);

return statearr_31486;
})();
var statearr_31487_34729 = state_31441__$1;
(statearr_31487_34729[(2)] = null);

(statearr_31487_34729[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_31488 = [null,null,null,null,null,null,null,null];
(statearr_31488[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_31488[(1)] = (1));

return statearr_31488;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_31441){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_31441);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e31489){var ex__29321__auto__ = e31489;
var statearr_31491_34740 = state_31441;
(statearr_31491_34740[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_31441[(4)]))){
var statearr_31492_34744 = state_31441;
(statearr_31492_34744[(1)] = cljs.core.first((state_31441[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34746 = state_31441;
state_31441 = G__34746;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_31441){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_31441);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_31497 = f__30229__auto__();
(statearr_31497[(6)] = c__30228__auto__);

return statearr_31497;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));

return c__30228__auto__;
}));

(cljs.core.async.onto_chan_BANG_.cljs$lang$maxFixedArity = 3);

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan_BANG_ = (function cljs$core$async$to_chan_BANG_(coll){
var ch = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.bounded_count((100),coll));
cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$2(ch,coll);

return ch;
});
/**
 * Deprecated - use onto-chan!
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var G__31507 = arguments.length;
switch (G__31507) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,true);
}));

(cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
return cljs.core.async.onto_chan_BANG_.cljs$core$IFn$_invoke$arity$3(ch,coll,close_QMARK_);
}));

(cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - use to-chan!
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
return cljs.core.async.to_chan_BANG_(coll);
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

var cljs$core$async$Mux$muxch_STAR_$dyn_34767 = (function (_){
var x__5350__auto__ = (((_ == null))?null:_);
var m__5351__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__5351__auto__.call(null,_));
} else {
var m__5349__auto__ = (cljs.core.async.muxch_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(_) : m__5349__auto__.call(null,_));
} else {
throw cljs.core.missing_protocol("Mux.muxch*",_);
}
}
});
cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((((!((_ == null)))) && ((!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
return cljs$core$async$Mux$muxch_STAR_$dyn_34767(_);
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

var cljs$core$async$Mult$tap_STAR_$dyn_34770 = (function (m,ch,close_QMARK_){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__5351__auto__.call(null,m,ch,close_QMARK_));
} else {
var m__5349__auto__ = (cljs.core.async.tap_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$3(m,ch,close_QMARK_) : m__5349__auto__.call(null,m,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Mult.tap*",m);
}
}
});
cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
return cljs$core$async$Mult$tap_STAR_$dyn_34770(m,ch,close_QMARK_);
}
});

var cljs$core$async$Mult$untap_STAR_$dyn_34774 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null,m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.untap_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mult.untap*",m);
}
}
});
cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mult$untap_STAR_$dyn_34774(m,ch);
}
});

var cljs$core$async$Mult$untap_all_STAR_$dyn_34786 = (function (m){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5351__auto__.call(null,m));
} else {
var m__5349__auto__ = (cljs.core.async.untap_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5349__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mult.untap-all*",m);
}
}
});
cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mult$untap_all_STAR_$dyn_34786(m);
}
});


/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async31539 = (function (ch,cs,meta31540){
this.ch = ch;
this.cs = cs;
this.meta31540 = meta31540;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async31539.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_31541,meta31540__$1){
var self__ = this;
var _31541__$1 = this;
return (new cljs.core.async.t_cljs$core$async31539(self__.ch,self__.cs,meta31540__$1));
}));

(cljs.core.async.t_cljs$core$async31539.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_31541){
var self__ = this;
var _31541__$1 = this;
return self__.meta31540;
}));

(cljs.core.async.t_cljs$core$async31539.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async31539.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async31539.prototype.cljs$core$async$Mult$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async31539.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
}));

(cljs.core.async.t_cljs$core$async31539.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch__$1);

return null;
}));

(cljs.core.async.t_cljs$core$async31539.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
}));

(cljs.core.async.t_cljs$core$async31539.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta31540","meta31540",-1675313372,null)], null);
}));

(cljs.core.async.t_cljs$core$async31539.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async31539.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async31539");

(cljs.core.async.t_cljs$core$async31539.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async31539");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async31539.
 */
cljs.core.async.__GT_t_cljs$core$async31539 = (function cljs$core$async$__GT_t_cljs$core$async31539(ch,cs,meta31540){
return (new cljs.core.async.t_cljs$core$async31539(ch,cs,meta31540));
});


/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var m = (new cljs.core.async.t_cljs$core$async31539(ch,cs,cljs.core.PersistentArrayMap.EMPTY));
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = (function (_){
if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,true);
} else {
return null;
}
});
var c__30228__auto___34794 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_31707){
var state_val_31711 = (state_31707[(1)]);
if((state_val_31711 === (7))){
var inst_31701 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31712_34795 = state_31707__$1;
(statearr_31712_34795[(2)] = inst_31701);

(statearr_31712_34795[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (20))){
var inst_31593 = (state_31707[(7)]);
var inst_31606 = cljs.core.first(inst_31593);
var inst_31607 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31606,(0),null);
var inst_31608 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31606,(1),null);
var state_31707__$1 = (function (){var statearr_31716 = state_31707;
(statearr_31716[(8)] = inst_31607);

return statearr_31716;
})();
if(cljs.core.truth_(inst_31608)){
var statearr_31717_34797 = state_31707__$1;
(statearr_31717_34797[(1)] = (22));

} else {
var statearr_31718_34798 = state_31707__$1;
(statearr_31718_34798[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (27))){
var inst_31645 = (state_31707[(9)]);
var inst_31652 = (state_31707[(10)]);
var inst_31643 = (state_31707[(11)]);
var inst_31560 = (state_31707[(12)]);
var inst_31652__$1 = cljs.core._nth(inst_31643,inst_31645);
var inst_31653 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_31652__$1,inst_31560,done);
var state_31707__$1 = (function (){var statearr_31723 = state_31707;
(statearr_31723[(10)] = inst_31652__$1);

return statearr_31723;
})();
if(cljs.core.truth_(inst_31653)){
var statearr_31724_34800 = state_31707__$1;
(statearr_31724_34800[(1)] = (30));

} else {
var statearr_31725_34801 = state_31707__$1;
(statearr_31725_34801[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (1))){
var state_31707__$1 = state_31707;
var statearr_31728_34802 = state_31707__$1;
(statearr_31728_34802[(2)] = null);

(statearr_31728_34802[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (24))){
var inst_31593 = (state_31707[(7)]);
var inst_31615 = (state_31707[(2)]);
var inst_31616 = cljs.core.next(inst_31593);
var inst_31570 = inst_31616;
var inst_31571 = null;
var inst_31572 = (0);
var inst_31573 = (0);
var state_31707__$1 = (function (){var statearr_31729 = state_31707;
(statearr_31729[(13)] = inst_31572);

(statearr_31729[(14)] = inst_31570);

(statearr_31729[(15)] = inst_31615);

(statearr_31729[(16)] = inst_31573);

(statearr_31729[(17)] = inst_31571);

return statearr_31729;
})();
var statearr_31736_34805 = state_31707__$1;
(statearr_31736_34805[(2)] = null);

(statearr_31736_34805[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (39))){
var state_31707__$1 = state_31707;
var statearr_31744_34806 = state_31707__$1;
(statearr_31744_34806[(2)] = null);

(statearr_31744_34806[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (4))){
var inst_31560 = (state_31707[(12)]);
var inst_31560__$1 = (state_31707[(2)]);
var inst_31561 = (inst_31560__$1 == null);
var state_31707__$1 = (function (){var statearr_31750 = state_31707;
(statearr_31750[(12)] = inst_31560__$1);

return statearr_31750;
})();
if(cljs.core.truth_(inst_31561)){
var statearr_31751_34807 = state_31707__$1;
(statearr_31751_34807[(1)] = (5));

} else {
var statearr_31752_34808 = state_31707__$1;
(statearr_31752_34808[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (15))){
var inst_31572 = (state_31707[(13)]);
var inst_31570 = (state_31707[(14)]);
var inst_31573 = (state_31707[(16)]);
var inst_31571 = (state_31707[(17)]);
var inst_31589 = (state_31707[(2)]);
var inst_31590 = (inst_31573 + (1));
var tmp31738 = inst_31572;
var tmp31739 = inst_31570;
var tmp31740 = inst_31571;
var inst_31570__$1 = tmp31739;
var inst_31571__$1 = tmp31740;
var inst_31572__$1 = tmp31738;
var inst_31573__$1 = inst_31590;
var state_31707__$1 = (function (){var statearr_31753 = state_31707;
(statearr_31753[(13)] = inst_31572__$1);

(statearr_31753[(14)] = inst_31570__$1);

(statearr_31753[(16)] = inst_31573__$1);

(statearr_31753[(17)] = inst_31571__$1);

(statearr_31753[(18)] = inst_31589);

return statearr_31753;
})();
var statearr_31754_34811 = state_31707__$1;
(statearr_31754_34811[(2)] = null);

(statearr_31754_34811[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (21))){
var inst_31619 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31775_34814 = state_31707__$1;
(statearr_31775_34814[(2)] = inst_31619);

(statearr_31775_34814[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (31))){
var inst_31652 = (state_31707[(10)]);
var inst_31656 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_31652);
var state_31707__$1 = state_31707;
var statearr_31776_34816 = state_31707__$1;
(statearr_31776_34816[(2)] = inst_31656);

(statearr_31776_34816[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (32))){
var inst_31645 = (state_31707[(9)]);
var inst_31644 = (state_31707[(19)]);
var inst_31642 = (state_31707[(20)]);
var inst_31643 = (state_31707[(11)]);
var inst_31658 = (state_31707[(2)]);
var inst_31659 = (inst_31645 + (1));
var tmp31765 = inst_31644;
var tmp31766 = inst_31642;
var tmp31767 = inst_31643;
var inst_31642__$1 = tmp31766;
var inst_31643__$1 = tmp31767;
var inst_31644__$1 = tmp31765;
var inst_31645__$1 = inst_31659;
var state_31707__$1 = (function (){var statearr_31785 = state_31707;
(statearr_31785[(9)] = inst_31645__$1);

(statearr_31785[(21)] = inst_31658);

(statearr_31785[(19)] = inst_31644__$1);

(statearr_31785[(20)] = inst_31642__$1);

(statearr_31785[(11)] = inst_31643__$1);

return statearr_31785;
})();
var statearr_31786_34823 = state_31707__$1;
(statearr_31786_34823[(2)] = null);

(statearr_31786_34823[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (40))){
var inst_31673 = (state_31707[(22)]);
var inst_31677 = m.cljs$core$async$Mult$untap_STAR_$arity$2(null,inst_31673);
var state_31707__$1 = state_31707;
var statearr_31789_34824 = state_31707__$1;
(statearr_31789_34824[(2)] = inst_31677);

(statearr_31789_34824[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (33))){
var inst_31662 = (state_31707[(23)]);
var inst_31665 = cljs.core.chunked_seq_QMARK_(inst_31662);
var state_31707__$1 = state_31707;
if(inst_31665){
var statearr_31798_34825 = state_31707__$1;
(statearr_31798_34825[(1)] = (36));

} else {
var statearr_31799_34826 = state_31707__$1;
(statearr_31799_34826[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (13))){
var inst_31583 = (state_31707[(24)]);
var inst_31586 = cljs.core.async.close_BANG_(inst_31583);
var state_31707__$1 = state_31707;
var statearr_31804_34828 = state_31707__$1;
(statearr_31804_34828[(2)] = inst_31586);

(statearr_31804_34828[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (22))){
var inst_31607 = (state_31707[(8)]);
var inst_31612 = cljs.core.async.close_BANG_(inst_31607);
var state_31707__$1 = state_31707;
var statearr_31805_34830 = state_31707__$1;
(statearr_31805_34830[(2)] = inst_31612);

(statearr_31805_34830[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (36))){
var inst_31662 = (state_31707[(23)]);
var inst_31668 = cljs.core.chunk_first(inst_31662);
var inst_31669 = cljs.core.chunk_rest(inst_31662);
var inst_31670 = cljs.core.count(inst_31668);
var inst_31642 = inst_31669;
var inst_31643 = inst_31668;
var inst_31644 = inst_31670;
var inst_31645 = (0);
var state_31707__$1 = (function (){var statearr_31810 = state_31707;
(statearr_31810[(9)] = inst_31645);

(statearr_31810[(19)] = inst_31644);

(statearr_31810[(20)] = inst_31642);

(statearr_31810[(11)] = inst_31643);

return statearr_31810;
})();
var statearr_31811_34831 = state_31707__$1;
(statearr_31811_34831[(2)] = null);

(statearr_31811_34831[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (41))){
var inst_31662 = (state_31707[(23)]);
var inst_31679 = (state_31707[(2)]);
var inst_31680 = cljs.core.next(inst_31662);
var inst_31642 = inst_31680;
var inst_31643 = null;
var inst_31644 = (0);
var inst_31645 = (0);
var state_31707__$1 = (function (){var statearr_31812 = state_31707;
(statearr_31812[(25)] = inst_31679);

(statearr_31812[(9)] = inst_31645);

(statearr_31812[(19)] = inst_31644);

(statearr_31812[(20)] = inst_31642);

(statearr_31812[(11)] = inst_31643);

return statearr_31812;
})();
var statearr_31814_34836 = state_31707__$1;
(statearr_31814_34836[(2)] = null);

(statearr_31814_34836[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (43))){
var state_31707__$1 = state_31707;
var statearr_31815_34837 = state_31707__$1;
(statearr_31815_34837[(2)] = null);

(statearr_31815_34837[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (29))){
var inst_31688 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31819_34838 = state_31707__$1;
(statearr_31819_34838[(2)] = inst_31688);

(statearr_31819_34838[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (44))){
var inst_31698 = (state_31707[(2)]);
var state_31707__$1 = (function (){var statearr_31821 = state_31707;
(statearr_31821[(26)] = inst_31698);

return statearr_31821;
})();
var statearr_31823_34839 = state_31707__$1;
(statearr_31823_34839[(2)] = null);

(statearr_31823_34839[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (6))){
var inst_31630 = (state_31707[(27)]);
var inst_31629 = cljs.core.deref(cs);
var inst_31630__$1 = cljs.core.keys(inst_31629);
var inst_31635 = cljs.core.count(inst_31630__$1);
var inst_31636 = cljs.core.reset_BANG_(dctr,inst_31635);
var inst_31641 = cljs.core.seq(inst_31630__$1);
var inst_31642 = inst_31641;
var inst_31643 = null;
var inst_31644 = (0);
var inst_31645 = (0);
var state_31707__$1 = (function (){var statearr_31827 = state_31707;
(statearr_31827[(9)] = inst_31645);

(statearr_31827[(28)] = inst_31636);

(statearr_31827[(19)] = inst_31644);

(statearr_31827[(27)] = inst_31630__$1);

(statearr_31827[(20)] = inst_31642);

(statearr_31827[(11)] = inst_31643);

return statearr_31827;
})();
var statearr_31828_34843 = state_31707__$1;
(statearr_31828_34843[(2)] = null);

(statearr_31828_34843[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (28))){
var inst_31662 = (state_31707[(23)]);
var inst_31642 = (state_31707[(20)]);
var inst_31662__$1 = cljs.core.seq(inst_31642);
var state_31707__$1 = (function (){var statearr_31830 = state_31707;
(statearr_31830[(23)] = inst_31662__$1);

return statearr_31830;
})();
if(inst_31662__$1){
var statearr_31831_34845 = state_31707__$1;
(statearr_31831_34845[(1)] = (33));

} else {
var statearr_31832_34846 = state_31707__$1;
(statearr_31832_34846[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (25))){
var inst_31645 = (state_31707[(9)]);
var inst_31644 = (state_31707[(19)]);
var inst_31649 = (inst_31645 < inst_31644);
var inst_31650 = inst_31649;
var state_31707__$1 = state_31707;
if(cljs.core.truth_(inst_31650)){
var statearr_31833_34853 = state_31707__$1;
(statearr_31833_34853[(1)] = (27));

} else {
var statearr_31834_34854 = state_31707__$1;
(statearr_31834_34854[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (34))){
var state_31707__$1 = state_31707;
var statearr_31835_34855 = state_31707__$1;
(statearr_31835_34855[(2)] = null);

(statearr_31835_34855[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (17))){
var state_31707__$1 = state_31707;
var statearr_31836_34856 = state_31707__$1;
(statearr_31836_34856[(2)] = null);

(statearr_31836_34856[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (3))){
var inst_31703 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
return cljs.core.async.impl.ioc_helpers.return_chan(state_31707__$1,inst_31703);
} else {
if((state_val_31711 === (12))){
var inst_31624 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31843_34859 = state_31707__$1;
(statearr_31843_34859[(2)] = inst_31624);

(statearr_31843_34859[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (2))){
var state_31707__$1 = state_31707;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31707__$1,(4),ch);
} else {
if((state_val_31711 === (23))){
var state_31707__$1 = state_31707;
var statearr_31855_34866 = state_31707__$1;
(statearr_31855_34866[(2)] = null);

(statearr_31855_34866[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (35))){
var inst_31686 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31858_34870 = state_31707__$1;
(statearr_31858_34870[(2)] = inst_31686);

(statearr_31858_34870[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (19))){
var inst_31593 = (state_31707[(7)]);
var inst_31597 = cljs.core.chunk_first(inst_31593);
var inst_31598 = cljs.core.chunk_rest(inst_31593);
var inst_31599 = cljs.core.count(inst_31597);
var inst_31570 = inst_31598;
var inst_31571 = inst_31597;
var inst_31572 = inst_31599;
var inst_31573 = (0);
var state_31707__$1 = (function (){var statearr_31862 = state_31707;
(statearr_31862[(13)] = inst_31572);

(statearr_31862[(14)] = inst_31570);

(statearr_31862[(16)] = inst_31573);

(statearr_31862[(17)] = inst_31571);

return statearr_31862;
})();
var statearr_31864_34871 = state_31707__$1;
(statearr_31864_34871[(2)] = null);

(statearr_31864_34871[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (11))){
var inst_31570 = (state_31707[(14)]);
var inst_31593 = (state_31707[(7)]);
var inst_31593__$1 = cljs.core.seq(inst_31570);
var state_31707__$1 = (function (){var statearr_31868 = state_31707;
(statearr_31868[(7)] = inst_31593__$1);

return statearr_31868;
})();
if(inst_31593__$1){
var statearr_31869_34883 = state_31707__$1;
(statearr_31869_34883[(1)] = (16));

} else {
var statearr_31870_34884 = state_31707__$1;
(statearr_31870_34884[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (9))){
var inst_31626 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31875_34885 = state_31707__$1;
(statearr_31875_34885[(2)] = inst_31626);

(statearr_31875_34885[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (5))){
var inst_31568 = cljs.core.deref(cs);
var inst_31569 = cljs.core.seq(inst_31568);
var inst_31570 = inst_31569;
var inst_31571 = null;
var inst_31572 = (0);
var inst_31573 = (0);
var state_31707__$1 = (function (){var statearr_31877 = state_31707;
(statearr_31877[(13)] = inst_31572);

(statearr_31877[(14)] = inst_31570);

(statearr_31877[(16)] = inst_31573);

(statearr_31877[(17)] = inst_31571);

return statearr_31877;
})();
var statearr_31879_34897 = state_31707__$1;
(statearr_31879_34897[(2)] = null);

(statearr_31879_34897[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (14))){
var state_31707__$1 = state_31707;
var statearr_31881_34898 = state_31707__$1;
(statearr_31881_34898[(2)] = null);

(statearr_31881_34898[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (45))){
var inst_31695 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31884_34899 = state_31707__$1;
(statearr_31884_34899[(2)] = inst_31695);

(statearr_31884_34899[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (26))){
var inst_31630 = (state_31707[(27)]);
var inst_31690 = (state_31707[(2)]);
var inst_31691 = cljs.core.seq(inst_31630);
var state_31707__$1 = (function (){var statearr_31887 = state_31707;
(statearr_31887[(29)] = inst_31690);

return statearr_31887;
})();
if(inst_31691){
var statearr_31888_34900 = state_31707__$1;
(statearr_31888_34900[(1)] = (42));

} else {
var statearr_31890_34901 = state_31707__$1;
(statearr_31890_34901[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (16))){
var inst_31593 = (state_31707[(7)]);
var inst_31595 = cljs.core.chunked_seq_QMARK_(inst_31593);
var state_31707__$1 = state_31707;
if(inst_31595){
var statearr_31892_34905 = state_31707__$1;
(statearr_31892_34905[(1)] = (19));

} else {
var statearr_31895_34906 = state_31707__$1;
(statearr_31895_34906[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (38))){
var inst_31683 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31899_34911 = state_31707__$1;
(statearr_31899_34911[(2)] = inst_31683);

(statearr_31899_34911[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (30))){
var state_31707__$1 = state_31707;
var statearr_31901_34912 = state_31707__$1;
(statearr_31901_34912[(2)] = null);

(statearr_31901_34912[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (10))){
var inst_31573 = (state_31707[(16)]);
var inst_31571 = (state_31707[(17)]);
var inst_31582 = cljs.core._nth(inst_31571,inst_31573);
var inst_31583 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31582,(0),null);
var inst_31584 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_31582,(1),null);
var state_31707__$1 = (function (){var statearr_31909 = state_31707;
(statearr_31909[(24)] = inst_31583);

return statearr_31909;
})();
if(cljs.core.truth_(inst_31584)){
var statearr_31912_34913 = state_31707__$1;
(statearr_31912_34913[(1)] = (13));

} else {
var statearr_31913_34914 = state_31707__$1;
(statearr_31913_34914[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (18))){
var inst_31622 = (state_31707[(2)]);
var state_31707__$1 = state_31707;
var statearr_31914_34915 = state_31707__$1;
(statearr_31914_34915[(2)] = inst_31622);

(statearr_31914_34915[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (42))){
var state_31707__$1 = state_31707;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_31707__$1,(45),dchan);
} else {
if((state_val_31711 === (37))){
var inst_31662 = (state_31707[(23)]);
var inst_31673 = (state_31707[(22)]);
var inst_31560 = (state_31707[(12)]);
var inst_31673__$1 = cljs.core.first(inst_31662);
var inst_31674 = cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3(inst_31673__$1,inst_31560,done);
var state_31707__$1 = (function (){var statearr_31921 = state_31707;
(statearr_31921[(22)] = inst_31673__$1);

return statearr_31921;
})();
if(cljs.core.truth_(inst_31674)){
var statearr_31922_34918 = state_31707__$1;
(statearr_31922_34918[(1)] = (39));

} else {
var statearr_31926_34919 = state_31707__$1;
(statearr_31926_34919[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_31711 === (8))){
var inst_31572 = (state_31707[(13)]);
var inst_31573 = (state_31707[(16)]);
var inst_31575 = (inst_31573 < inst_31572);
var inst_31576 = inst_31575;
var state_31707__$1 = state_31707;
if(cljs.core.truth_(inst_31576)){
var statearr_31931_34920 = state_31707__$1;
(statearr_31931_34920[(1)] = (10));

} else {
var statearr_31933_34921 = state_31707__$1;
(statearr_31933_34921[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mult_$_state_machine__29318__auto__ = null;
var cljs$core$async$mult_$_state_machine__29318__auto____0 = (function (){
var statearr_31962 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_31962[(0)] = cljs$core$async$mult_$_state_machine__29318__auto__);

(statearr_31962[(1)] = (1));

return statearr_31962;
});
var cljs$core$async$mult_$_state_machine__29318__auto____1 = (function (state_31707){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_31707);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e31969){var ex__29321__auto__ = e31969;
var statearr_31970_34928 = state_31707;
(statearr_31970_34928[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_31707[(4)]))){
var statearr_31972_34929 = state_31707;
(statearr_31972_34929[(1)] = cljs.core.first((state_31707[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__34934 = state_31707;
state_31707 = G__34934;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__29318__auto__ = function(state_31707){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__29318__auto____1.call(this,state_31707);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__29318__auto____0;
cljs$core$async$mult_$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__29318__auto____1;
return cljs$core$async$mult_$_state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_31974 = f__30229__auto__();
(statearr_31974[(6)] = c__30228__auto___34794);

return statearr_31974;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var G__31986 = arguments.length;
switch (G__31986) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(mult,ch,true);
}));

(cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_(mult,ch,close_QMARK_);

return ch;
}));

(cljs.core.async.tap.cljs$lang$maxFixedArity = 3);

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_(mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_(mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

var cljs$core$async$Mix$admix_STAR_$dyn_34936 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null,m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.admix_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.admix*",m);
}
}
});
cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$admix_STAR_$dyn_34936(m,ch);
}
});

var cljs$core$async$Mix$unmix_STAR_$dyn_34942 = (function (m,ch){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5351__auto__.call(null,m,ch));
} else {
var m__5349__auto__ = (cljs.core.async.unmix_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,ch) : m__5349__auto__.call(null,m,ch));
} else {
throw cljs.core.missing_protocol("Mix.unmix*",m);
}
}
});
cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
return cljs$core$async$Mix$unmix_STAR_$dyn_34942(m,ch);
}
});

var cljs$core$async$Mix$unmix_all_STAR_$dyn_34949 = (function (m){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5351__auto__.call(null,m));
} else {
var m__5349__auto__ = (cljs.core.async.unmix_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(m) : m__5349__auto__.call(null,m));
} else {
throw cljs.core.missing_protocol("Mix.unmix-all*",m);
}
}
});
cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
return cljs$core$async$Mix$unmix_all_STAR_$dyn_34949(m);
}
});

var cljs$core$async$Mix$toggle_STAR_$dyn_34950 = (function (m,state_map){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__5351__auto__.call(null,m,state_map));
} else {
var m__5349__auto__ = (cljs.core.async.toggle_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,state_map) : m__5349__auto__.call(null,m,state_map));
} else {
throw cljs.core.missing_protocol("Mix.toggle*",m);
}
}
});
cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
return cljs$core$async$Mix$toggle_STAR_$dyn_34950(m,state_map);
}
});

var cljs$core$async$Mix$solo_mode_STAR_$dyn_34955 = (function (m,mode){
var x__5350__auto__ = (((m == null))?null:m);
var m__5351__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__5351__auto__.call(null,m,mode));
} else {
var m__5349__auto__ = (cljs.core.async.solo_mode_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(m,mode) : m__5349__auto__.call(null,m,mode));
} else {
throw cljs.core.missing_protocol("Mix.solo-mode*",m);
}
}
});
cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((((!((m == null)))) && ((!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
return cljs$core$async$Mix$solo_mode_STAR_$dyn_34955(m,mode);
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__5732__auto__ = [];
var len__5726__auto___34957 = arguments.length;
var i__5727__auto___34958 = (0);
while(true){
if((i__5727__auto___34958 < len__5726__auto___34957)){
args__5732__auto__.push((arguments[i__5727__auto___34958]));

var G__34960 = (i__5727__auto___34958 + (1));
i__5727__auto___34958 = G__34960;
continue;
} else {
}
break;
}

var argseq__5733__auto__ = ((((3) < args__5732__auto__.length))?(new cljs.core.IndexedSeq(args__5732__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__5733__auto__);
});

(cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__32088){
var map__32090 = p__32088;
var map__32090__$1 = cljs.core.__destructure_map(map__32090);
var opts = map__32090__$1;
var statearr_32092_34967 = state;
(statearr_32092_34967[(1)] = cont_block);


var temp__5804__auto__ = cljs.core.async.do_alts((function (val){
var statearr_32097_34968 = state;
(statearr_32097_34968[(2)] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state);
}),ports,opts);
if(cljs.core.truth_(temp__5804__auto__)){
var cb = temp__5804__auto__;
var statearr_32099_34969 = state;
(statearr_32099_34969[(2)] = cljs.core.deref(cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}));

(cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3));

/** @this {Function} */
(cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq32068){
var G__32069 = cljs.core.first(seq32068);
var seq32068__$1 = cljs.core.next(seq32068);
var G__32070 = cljs.core.first(seq32068__$1);
var seq32068__$2 = cljs.core.next(seq32068__$1);
var G__32071 = cljs.core.first(seq32068__$2);
var seq32068__$3 = cljs.core.next(seq32068__$2);
var self__5711__auto__ = this;
return self__5711__auto__.cljs$core$IFn$_invoke$arity$variadic(G__32069,G__32070,G__32071,seq32068__$3);
}));


/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32113 = (function (change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta32114){
this.change = change;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta32114 = meta32114;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32115,meta32114__$1){
var self__ = this;
var _32115__$1 = this;
return (new cljs.core.async.t_cljs$core$async32113(self__.change,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta32114__$1));
}));

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32115){
var self__ = this;
var _32115__$1 = this;
return self__.meta32114;
}));

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
}));

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$async$Mix$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$4(self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.dissoc,ch);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_(self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.cs,cljs.core.partial.cljs$core$IFn$_invoke$arity$2(cljs.core.merge_with,cljs.core.merge),state_map);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32113.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.solo_modes.cljs$core$IFn$_invoke$arity$1 ? self__.solo_modes.cljs$core$IFn$_invoke$arity$1(mode) : self__.solo_modes.call(null,mode)))){
} else {
throw (new Error(["Assert failed: ",["mode must be one of: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(self__.solo_modes)].join(''),"\n","(solo-modes mode)"].join('')));
}

cljs.core.reset_BANG_(self__.solo_mode,mode);

return (self__.changed.cljs$core$IFn$_invoke$arity$0 ? self__.changed.cljs$core$IFn$_invoke$arity$0() : self__.changed.call(null));
}));

(cljs.core.async.t_cljs$core$async32113.getBasis = (function (){
return new cljs.core.PersistentVector(null, 10, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta32114","meta32114",-1501481273,null)], null);
}));

(cljs.core.async.t_cljs$core$async32113.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32113.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32113");

(cljs.core.async.t_cljs$core$async32113.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async32113");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32113.
 */
cljs.core.async.__GT_t_cljs$core$async32113 = (function cljs$core$async$__GT_t_cljs$core$async32113(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta32114){
return (new cljs.core.async.t_cljs$core$async32113(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta32114));
});


/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.cljs$core$IFn$_invoke$arity$2(solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(cljs.core.async.sliding_buffer((1)));
var changed = (function (){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(change,true);
});
var pick = (function (attr,chs){
return cljs.core.reduce_kv((function (ret,c,v){
if(cljs.core.truth_((attr.cljs$core$IFn$_invoke$arity$1 ? attr.cljs$core$IFn$_invoke$arity$1(v) : attr.call(null,v)))){
return cljs.core.conj.cljs$core$IFn$_invoke$arity$2(ret,c);
} else {
return ret;
}
}),cljs.core.PersistentHashSet.EMPTY,chs);
});
var calc_state = (function (){
var chs = cljs.core.deref(cs);
var mode = cljs.core.deref(solo_mode);
var solos = pick(new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick(new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick(new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.cljs$core$IFn$_invoke$arity$2(((((cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && ((!(cljs.core.empty_QMARK_(solos))))))?cljs.core.vec(solos):cljs.core.vec(cljs.core.remove.cljs$core$IFn$_invoke$arity$2(pauses,cljs.core.keys(chs)))),change)], null);
});
var m = (new cljs.core.async.t_cljs$core$async32113(change,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
var c__30228__auto___34984 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_32204){
var state_val_32205 = (state_32204[(1)]);
if((state_val_32205 === (7))){
var inst_32160 = (state_32204[(2)]);
var state_32204__$1 = state_32204;
if(cljs.core.truth_(inst_32160)){
var statearr_32210_34985 = state_32204__$1;
(statearr_32210_34985[(1)] = (8));

} else {
var statearr_32211_34986 = state_32204__$1;
(statearr_32211_34986[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (20))){
var inst_32151 = (state_32204[(7)]);
var state_32204__$1 = state_32204;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32204__$1,(23),out,inst_32151);
} else {
if((state_val_32205 === (1))){
var inst_32129 = calc_state();
var inst_32130 = cljs.core.__destructure_map(inst_32129);
var inst_32131 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32130,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_32132 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32130,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32133 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32130,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_32134 = inst_32129;
var state_32204__$1 = (function (){var statearr_32212 = state_32204;
(statearr_32212[(8)] = inst_32134);

(statearr_32212[(9)] = inst_32131);

(statearr_32212[(10)] = inst_32132);

(statearr_32212[(11)] = inst_32133);

return statearr_32212;
})();
var statearr_32213_34987 = state_32204__$1;
(statearr_32213_34987[(2)] = null);

(statearr_32213_34987[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (24))){
var inst_32141 = (state_32204[(12)]);
var inst_32134 = inst_32141;
var state_32204__$1 = (function (){var statearr_32214 = state_32204;
(statearr_32214[(8)] = inst_32134);

return statearr_32214;
})();
var statearr_32215_34989 = state_32204__$1;
(statearr_32215_34989[(2)] = null);

(statearr_32215_34989[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (4))){
var inst_32155 = (state_32204[(13)]);
var inst_32151 = (state_32204[(7)]);
var inst_32150 = (state_32204[(2)]);
var inst_32151__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32150,(0),null);
var inst_32153 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32150,(1),null);
var inst_32155__$1 = (inst_32151__$1 == null);
var state_32204__$1 = (function (){var statearr_32216 = state_32204;
(statearr_32216[(14)] = inst_32153);

(statearr_32216[(13)] = inst_32155__$1);

(statearr_32216[(7)] = inst_32151__$1);

return statearr_32216;
})();
if(cljs.core.truth_(inst_32155__$1)){
var statearr_32220_34990 = state_32204__$1;
(statearr_32220_34990[(1)] = (5));

} else {
var statearr_32221_34993 = state_32204__$1;
(statearr_32221_34993[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (15))){
var inst_32142 = (state_32204[(15)]);
var inst_32174 = (state_32204[(16)]);
var inst_32174__$1 = cljs.core.empty_QMARK_(inst_32142);
var state_32204__$1 = (function (){var statearr_32222 = state_32204;
(statearr_32222[(16)] = inst_32174__$1);

return statearr_32222;
})();
if(inst_32174__$1){
var statearr_32223_34996 = state_32204__$1;
(statearr_32223_34996[(1)] = (17));

} else {
var statearr_32225_34997 = state_32204__$1;
(statearr_32225_34997[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (21))){
var inst_32141 = (state_32204[(12)]);
var inst_32134 = inst_32141;
var state_32204__$1 = (function (){var statearr_32229 = state_32204;
(statearr_32229[(8)] = inst_32134);

return statearr_32229;
})();
var statearr_32231_35001 = state_32204__$1;
(statearr_32231_35001[(2)] = null);

(statearr_32231_35001[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (13))){
var inst_32167 = (state_32204[(2)]);
var inst_32168 = calc_state();
var inst_32134 = inst_32168;
var state_32204__$1 = (function (){var statearr_32232 = state_32204;
(statearr_32232[(8)] = inst_32134);

(statearr_32232[(17)] = inst_32167);

return statearr_32232;
})();
var statearr_32233_35007 = state_32204__$1;
(statearr_32233_35007[(2)] = null);

(statearr_32233_35007[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (22))){
var inst_32197 = (state_32204[(2)]);
var state_32204__$1 = state_32204;
var statearr_32234_35008 = state_32204__$1;
(statearr_32234_35008[(2)] = inst_32197);

(statearr_32234_35008[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (6))){
var inst_32153 = (state_32204[(14)]);
var inst_32158 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_32153,change);
var state_32204__$1 = state_32204;
var statearr_32235_35012 = state_32204__$1;
(statearr_32235_35012[(2)] = inst_32158);

(statearr_32235_35012[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (25))){
var state_32204__$1 = state_32204;
var statearr_32242_35014 = state_32204__$1;
(statearr_32242_35014[(2)] = null);

(statearr_32242_35014[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (17))){
var inst_32153 = (state_32204[(14)]);
var inst_32143 = (state_32204[(18)]);
var inst_32176 = (inst_32143.cljs$core$IFn$_invoke$arity$1 ? inst_32143.cljs$core$IFn$_invoke$arity$1(inst_32153) : inst_32143.call(null,inst_32153));
var inst_32177 = cljs.core.not(inst_32176);
var state_32204__$1 = state_32204;
var statearr_32247_35015 = state_32204__$1;
(statearr_32247_35015[(2)] = inst_32177);

(statearr_32247_35015[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (3))){
var inst_32201 = (state_32204[(2)]);
var state_32204__$1 = state_32204;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32204__$1,inst_32201);
} else {
if((state_val_32205 === (12))){
var state_32204__$1 = state_32204;
var statearr_32249_35018 = state_32204__$1;
(statearr_32249_35018[(2)] = null);

(statearr_32249_35018[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (2))){
var inst_32134 = (state_32204[(8)]);
var inst_32141 = (state_32204[(12)]);
var inst_32141__$1 = cljs.core.__destructure_map(inst_32134);
var inst_32142 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32141__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_32143 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32141__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_32144 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32141__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_32204__$1 = (function (){var statearr_32253 = state_32204;
(statearr_32253[(15)] = inst_32142);

(statearr_32253[(18)] = inst_32143);

(statearr_32253[(12)] = inst_32141__$1);

return statearr_32253;
})();
return cljs.core.async.ioc_alts_BANG_(state_32204__$1,(4),inst_32144);
} else {
if((state_val_32205 === (23))){
var inst_32185 = (state_32204[(2)]);
var state_32204__$1 = state_32204;
if(cljs.core.truth_(inst_32185)){
var statearr_32254_35023 = state_32204__$1;
(statearr_32254_35023[(1)] = (24));

} else {
var statearr_32255_35024 = state_32204__$1;
(statearr_32255_35024[(1)] = (25));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (19))){
var inst_32180 = (state_32204[(2)]);
var state_32204__$1 = state_32204;
var statearr_32256_35025 = state_32204__$1;
(statearr_32256_35025[(2)] = inst_32180);

(statearr_32256_35025[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (11))){
var inst_32153 = (state_32204[(14)]);
var inst_32164 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(cs,cljs.core.dissoc,inst_32153);
var state_32204__$1 = state_32204;
var statearr_32262_35026 = state_32204__$1;
(statearr_32262_35026[(2)] = inst_32164);

(statearr_32262_35026[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (9))){
var inst_32153 = (state_32204[(14)]);
var inst_32142 = (state_32204[(15)]);
var inst_32171 = (state_32204[(19)]);
var inst_32171__$1 = (inst_32142.cljs$core$IFn$_invoke$arity$1 ? inst_32142.cljs$core$IFn$_invoke$arity$1(inst_32153) : inst_32142.call(null,inst_32153));
var state_32204__$1 = (function (){var statearr_32266 = state_32204;
(statearr_32266[(19)] = inst_32171__$1);

return statearr_32266;
})();
if(cljs.core.truth_(inst_32171__$1)){
var statearr_32267_35028 = state_32204__$1;
(statearr_32267_35028[(1)] = (14));

} else {
var statearr_32268_35030 = state_32204__$1;
(statearr_32268_35030[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (5))){
var inst_32155 = (state_32204[(13)]);
var state_32204__$1 = state_32204;
var statearr_32269_35032 = state_32204__$1;
(statearr_32269_35032[(2)] = inst_32155);

(statearr_32269_35032[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (14))){
var inst_32171 = (state_32204[(19)]);
var state_32204__$1 = state_32204;
var statearr_32270_35033 = state_32204__$1;
(statearr_32270_35033[(2)] = inst_32171);

(statearr_32270_35033[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (26))){
var inst_32193 = (state_32204[(2)]);
var state_32204__$1 = state_32204;
var statearr_32271_35037 = state_32204__$1;
(statearr_32271_35037[(2)] = inst_32193);

(statearr_32271_35037[(1)] = (22));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (16))){
var inst_32182 = (state_32204[(2)]);
var state_32204__$1 = state_32204;
if(cljs.core.truth_(inst_32182)){
var statearr_32273_35038 = state_32204__$1;
(statearr_32273_35038[(1)] = (20));

} else {
var statearr_32274_35039 = state_32204__$1;
(statearr_32274_35039[(1)] = (21));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (10))){
var inst_32199 = (state_32204[(2)]);
var state_32204__$1 = state_32204;
var statearr_32275_35040 = state_32204__$1;
(statearr_32275_35040[(2)] = inst_32199);

(statearr_32275_35040[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (18))){
var inst_32174 = (state_32204[(16)]);
var state_32204__$1 = state_32204;
var statearr_32276_35041 = state_32204__$1;
(statearr_32276_35041[(2)] = inst_32174);

(statearr_32276_35041[(1)] = (19));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32205 === (8))){
var inst_32151 = (state_32204[(7)]);
var inst_32162 = (inst_32151 == null);
var state_32204__$1 = state_32204;
if(cljs.core.truth_(inst_32162)){
var statearr_32277_35043 = state_32204__$1;
(statearr_32277_35043[(1)] = (11));

} else {
var statearr_32278_35044 = state_32204__$1;
(statearr_32278_35044[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mix_$_state_machine__29318__auto__ = null;
var cljs$core$async$mix_$_state_machine__29318__auto____0 = (function (){
var statearr_32286 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32286[(0)] = cljs$core$async$mix_$_state_machine__29318__auto__);

(statearr_32286[(1)] = (1));

return statearr_32286;
});
var cljs$core$async$mix_$_state_machine__29318__auto____1 = (function (state_32204){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_32204);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e32290){var ex__29321__auto__ = e32290;
var statearr_32291_35051 = state_32204;
(statearr_32291_35051[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_32204[(4)]))){
var statearr_32292_35053 = state_32204;
(statearr_32292_35053[(1)] = cljs.core.first((state_32204[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35054 = state_32204;
state_32204 = G__35054;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__29318__auto__ = function(state_32204){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__29318__auto____1.call(this,state_32204);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__29318__auto____0;
cljs$core$async$mix_$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__29318__auto____1;
return cljs$core$async$mix_$_state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_32293 = f__30229__auto__();
(statearr_32293[(6)] = c__30228__auto___34984);

return statearr_32293;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_(mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_(mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_(mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_(mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_(mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

var cljs$core$async$Pub$sub_STAR_$dyn_35064 = (function (p,v,ch,close_QMARK_){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$4 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__5351__auto__.call(null,p,v,ch,close_QMARK_));
} else {
var m__5349__auto__ = (cljs.core.async.sub_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$4 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$4(p,v,ch,close_QMARK_) : m__5349__auto__.call(null,p,v,ch,close_QMARK_));
} else {
throw cljs.core.missing_protocol("Pub.sub*",p);
}
}
});
cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
return cljs$core$async$Pub$sub_STAR_$dyn_35064(p,v,ch,close_QMARK_);
}
});

var cljs$core$async$Pub$unsub_STAR_$dyn_35090 = (function (p,v,ch){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__5351__auto__.call(null,p,v,ch));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$3 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$3(p,v,ch) : m__5349__auto__.call(null,p,v,ch));
} else {
throw cljs.core.missing_protocol("Pub.unsub*",p);
}
}
});
cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
return cljs$core$async$Pub$unsub_STAR_$dyn_35090(p,v,ch);
}
});

var cljs$core$async$Pub$unsub_all_STAR_$dyn_35092 = (function() {
var G__35094 = null;
var G__35094__1 = (function (p){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__5351__auto__.call(null,p));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$1 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$1(p) : m__5349__auto__.call(null,p));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
var G__35094__2 = (function (p,v){
var x__5350__auto__ = (((p == null))?null:p);
var m__5351__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__5350__auto__)]);
if((!((m__5351__auto__ == null)))){
return (m__5351__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5351__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__5351__auto__.call(null,p,v));
} else {
var m__5349__auto__ = (cljs.core.async.unsub_all_STAR_["_"]);
if((!((m__5349__auto__ == null)))){
return (m__5349__auto__.cljs$core$IFn$_invoke$arity$2 ? m__5349__auto__.cljs$core$IFn$_invoke$arity$2(p,v) : m__5349__auto__.call(null,p,v));
} else {
throw cljs.core.missing_protocol("Pub.unsub-all*",p);
}
}
});
G__35094 = function(p,v){
switch(arguments.length){
case 1:
return G__35094__1.call(this,p);
case 2:
return G__35094__2.call(this,p,v);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
G__35094.cljs$core$IFn$_invoke$arity$1 = G__35094__1;
G__35094.cljs$core$IFn$_invoke$arity$2 = G__35094__2;
return G__35094;
})()
;
cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var G__32354 = arguments.length;
switch (G__32354) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_35092(p);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((((!((p == null)))) && ((!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
return cljs$core$async$Pub$unsub_all_STAR_$dyn_35092(p,v);
}
}));

(cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2);



/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async32367 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta32368){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta32368 = meta32368;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_32369,meta32368__$1){
var self__ = this;
var _32369__$1 = this;
return (new cljs.core.async.t_cljs$core$async32367(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta32368__$1));
}));

(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_32369){
var self__ = this;
var _32369__$1 = this;
return self__.meta32368;
}));

(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$async$Mux$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
}));

(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$async$Pub$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = (self__.ensure_mult.cljs$core$IFn$_invoke$arity$1 ? self__.ensure_mult.cljs$core$IFn$_invoke$arity$1(topic) : self__.ensure_mult.call(null,topic));
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3(m,ch__$1,close_QMARK_);
}));

(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__5804__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(self__.mults),topic);
if(cljs.core.truth_(temp__5804__auto__)){
var m = temp__5804__auto__;
return cljs.core.async.untap(m,ch__$1);
} else {
return null;
}
}));

(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_(self__.mults,cljs.core.PersistentArrayMap.EMPTY);
}));

(cljs.core.async.t_cljs$core$async32367.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(self__.mults,cljs.core.dissoc,topic);
}));

(cljs.core.async.t_cljs$core$async32367.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta32368","meta32368",-98799822,null)], null);
}));

(cljs.core.async.t_cljs$core$async32367.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async32367.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async32367");

(cljs.core.async.t_cljs$core$async32367.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async32367");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async32367.
 */
cljs.core.async.__GT_t_cljs$core$async32367 = (function cljs$core$async$__GT_t_cljs$core$async32367(ch,topic_fn,buf_fn,mults,ensure_mult,meta32368){
return (new cljs.core.async.t_cljs$core$async32367(ch,topic_fn,buf_fn,mults,ensure_mult,meta32368));
});


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var G__32361 = arguments.length;
switch (G__32361) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3(ch,topic_fn,cljs.core.constantly(null));
}));

(cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = (function (topic){
var or__5002__auto__ = cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.deref(mults),topic);
if(cljs.core.truth_(or__5002__auto__)){
return or__5002__auto__;
} else {
return cljs.core.get.cljs$core$IFn$_invoke$arity$2(cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(mults,(function (p1__32356_SHARP_){
if(cljs.core.truth_((p1__32356_SHARP_.cljs$core$IFn$_invoke$arity$1 ? p1__32356_SHARP_.cljs$core$IFn$_invoke$arity$1(topic) : p1__32356_SHARP_.call(null,topic)))){
return p1__32356_SHARP_;
} else {
return cljs.core.assoc.cljs$core$IFn$_invoke$arity$3(p1__32356_SHARP_,topic,cljs.core.async.mult(cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((buf_fn.cljs$core$IFn$_invoke$arity$1 ? buf_fn.cljs$core$IFn$_invoke$arity$1(topic) : buf_fn.call(null,topic)))));
}
})),topic);
}
});
var p = (new cljs.core.async.t_cljs$core$async32367(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
var c__30228__auto___35117 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_32461){
var state_val_32462 = (state_32461[(1)]);
if((state_val_32462 === (7))){
var inst_32457 = (state_32461[(2)]);
var state_32461__$1 = state_32461;
var statearr_32464_35120 = state_32461__$1;
(statearr_32464_35120[(2)] = inst_32457);

(statearr_32464_35120[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (20))){
var state_32461__$1 = state_32461;
var statearr_32466_35124 = state_32461__$1;
(statearr_32466_35124[(2)] = null);

(statearr_32466_35124[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (1))){
var state_32461__$1 = state_32461;
var statearr_32468_35128 = state_32461__$1;
(statearr_32468_35128[(2)] = null);

(statearr_32468_35128[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (24))){
var inst_32440 = (state_32461[(7)]);
var inst_32449 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$3(mults,cljs.core.dissoc,inst_32440);
var state_32461__$1 = state_32461;
var statearr_32474_35130 = state_32461__$1;
(statearr_32474_35130[(2)] = inst_32449);

(statearr_32474_35130[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (4))){
var inst_32389 = (state_32461[(8)]);
var inst_32389__$1 = (state_32461[(2)]);
var inst_32390 = (inst_32389__$1 == null);
var state_32461__$1 = (function (){var statearr_32477 = state_32461;
(statearr_32477[(8)] = inst_32389__$1);

return statearr_32477;
})();
if(cljs.core.truth_(inst_32390)){
var statearr_32478_35134 = state_32461__$1;
(statearr_32478_35134[(1)] = (5));

} else {
var statearr_32482_35135 = state_32461__$1;
(statearr_32482_35135[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (15))){
var inst_32434 = (state_32461[(2)]);
var state_32461__$1 = state_32461;
var statearr_32485_35139 = state_32461__$1;
(statearr_32485_35139[(2)] = inst_32434);

(statearr_32485_35139[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (21))){
var inst_32454 = (state_32461[(2)]);
var state_32461__$1 = (function (){var statearr_32486 = state_32461;
(statearr_32486[(9)] = inst_32454);

return statearr_32486;
})();
var statearr_32487_35140 = state_32461__$1;
(statearr_32487_35140[(2)] = null);

(statearr_32487_35140[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (13))){
var inst_32416 = (state_32461[(10)]);
var inst_32418 = cljs.core.chunked_seq_QMARK_(inst_32416);
var state_32461__$1 = state_32461;
if(inst_32418){
var statearr_32491_35145 = state_32461__$1;
(statearr_32491_35145[(1)] = (16));

} else {
var statearr_32494_35147 = state_32461__$1;
(statearr_32494_35147[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (22))){
var inst_32446 = (state_32461[(2)]);
var state_32461__$1 = state_32461;
if(cljs.core.truth_(inst_32446)){
var statearr_32495_35150 = state_32461__$1;
(statearr_32495_35150[(1)] = (23));

} else {
var statearr_32496_35151 = state_32461__$1;
(statearr_32496_35151[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (6))){
var inst_32442 = (state_32461[(11)]);
var inst_32440 = (state_32461[(7)]);
var inst_32389 = (state_32461[(8)]);
var inst_32440__$1 = (topic_fn.cljs$core$IFn$_invoke$arity$1 ? topic_fn.cljs$core$IFn$_invoke$arity$1(inst_32389) : topic_fn.call(null,inst_32389));
var inst_32441 = cljs.core.deref(mults);
var inst_32442__$1 = cljs.core.get.cljs$core$IFn$_invoke$arity$2(inst_32441,inst_32440__$1);
var state_32461__$1 = (function (){var statearr_32499 = state_32461;
(statearr_32499[(11)] = inst_32442__$1);

(statearr_32499[(7)] = inst_32440__$1);

return statearr_32499;
})();
if(cljs.core.truth_(inst_32442__$1)){
var statearr_32500_35152 = state_32461__$1;
(statearr_32500_35152[(1)] = (19));

} else {
var statearr_32501_35153 = state_32461__$1;
(statearr_32501_35153[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (25))){
var inst_32451 = (state_32461[(2)]);
var state_32461__$1 = state_32461;
var statearr_32503_35155 = state_32461__$1;
(statearr_32503_35155[(2)] = inst_32451);

(statearr_32503_35155[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (17))){
var inst_32416 = (state_32461[(10)]);
var inst_32425 = cljs.core.first(inst_32416);
var inst_32426 = cljs.core.async.muxch_STAR_(inst_32425);
var inst_32427 = cljs.core.async.close_BANG_(inst_32426);
var inst_32428 = cljs.core.next(inst_32416);
var inst_32402 = inst_32428;
var inst_32403 = null;
var inst_32404 = (0);
var inst_32405 = (0);
var state_32461__$1 = (function (){var statearr_32505 = state_32461;
(statearr_32505[(12)] = inst_32404);

(statearr_32505[(13)] = inst_32402);

(statearr_32505[(14)] = inst_32405);

(statearr_32505[(15)] = inst_32427);

(statearr_32505[(16)] = inst_32403);

return statearr_32505;
})();
var statearr_32508_35168 = state_32461__$1;
(statearr_32508_35168[(2)] = null);

(statearr_32508_35168[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (3))){
var inst_32459 = (state_32461[(2)]);
var state_32461__$1 = state_32461;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32461__$1,inst_32459);
} else {
if((state_val_32462 === (12))){
var inst_32436 = (state_32461[(2)]);
var state_32461__$1 = state_32461;
var statearr_32511_35176 = state_32461__$1;
(statearr_32511_35176[(2)] = inst_32436);

(statearr_32511_35176[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (2))){
var state_32461__$1 = state_32461;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32461__$1,(4),ch);
} else {
if((state_val_32462 === (23))){
var state_32461__$1 = state_32461;
var statearr_32513_35180 = state_32461__$1;
(statearr_32513_35180[(2)] = null);

(statearr_32513_35180[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (19))){
var inst_32442 = (state_32461[(11)]);
var inst_32389 = (state_32461[(8)]);
var inst_32444 = cljs.core.async.muxch_STAR_(inst_32442);
var state_32461__$1 = state_32461;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32461__$1,(22),inst_32444,inst_32389);
} else {
if((state_val_32462 === (11))){
var inst_32416 = (state_32461[(10)]);
var inst_32402 = (state_32461[(13)]);
var inst_32416__$1 = cljs.core.seq(inst_32402);
var state_32461__$1 = (function (){var statearr_32514 = state_32461;
(statearr_32514[(10)] = inst_32416__$1);

return statearr_32514;
})();
if(inst_32416__$1){
var statearr_32518_35190 = state_32461__$1;
(statearr_32518_35190[(1)] = (13));

} else {
var statearr_32519_35199 = state_32461__$1;
(statearr_32519_35199[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (9))){
var inst_32438 = (state_32461[(2)]);
var state_32461__$1 = state_32461;
var statearr_32520_35205 = state_32461__$1;
(statearr_32520_35205[(2)] = inst_32438);

(statearr_32520_35205[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (5))){
var inst_32399 = cljs.core.deref(mults);
var inst_32400 = cljs.core.vals(inst_32399);
var inst_32401 = cljs.core.seq(inst_32400);
var inst_32402 = inst_32401;
var inst_32403 = null;
var inst_32404 = (0);
var inst_32405 = (0);
var state_32461__$1 = (function (){var statearr_32523 = state_32461;
(statearr_32523[(12)] = inst_32404);

(statearr_32523[(13)] = inst_32402);

(statearr_32523[(14)] = inst_32405);

(statearr_32523[(16)] = inst_32403);

return statearr_32523;
})();
var statearr_32525_35220 = state_32461__$1;
(statearr_32525_35220[(2)] = null);

(statearr_32525_35220[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (14))){
var state_32461__$1 = state_32461;
var statearr_32529_35221 = state_32461__$1;
(statearr_32529_35221[(2)] = null);

(statearr_32529_35221[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (16))){
var inst_32416 = (state_32461[(10)]);
var inst_32420 = cljs.core.chunk_first(inst_32416);
var inst_32421 = cljs.core.chunk_rest(inst_32416);
var inst_32422 = cljs.core.count(inst_32420);
var inst_32402 = inst_32421;
var inst_32403 = inst_32420;
var inst_32404 = inst_32422;
var inst_32405 = (0);
var state_32461__$1 = (function (){var statearr_32531 = state_32461;
(statearr_32531[(12)] = inst_32404);

(statearr_32531[(13)] = inst_32402);

(statearr_32531[(14)] = inst_32405);

(statearr_32531[(16)] = inst_32403);

return statearr_32531;
})();
var statearr_32533_35227 = state_32461__$1;
(statearr_32533_35227[(2)] = null);

(statearr_32533_35227[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (10))){
var inst_32404 = (state_32461[(12)]);
var inst_32402 = (state_32461[(13)]);
var inst_32405 = (state_32461[(14)]);
var inst_32403 = (state_32461[(16)]);
var inst_32410 = cljs.core._nth(inst_32403,inst_32405);
var inst_32411 = cljs.core.async.muxch_STAR_(inst_32410);
var inst_32412 = cljs.core.async.close_BANG_(inst_32411);
var inst_32413 = (inst_32405 + (1));
var tmp32526 = inst_32404;
var tmp32527 = inst_32402;
var tmp32528 = inst_32403;
var inst_32402__$1 = tmp32527;
var inst_32403__$1 = tmp32528;
var inst_32404__$1 = tmp32526;
var inst_32405__$1 = inst_32413;
var state_32461__$1 = (function (){var statearr_32537 = state_32461;
(statearr_32537[(17)] = inst_32412);

(statearr_32537[(12)] = inst_32404__$1);

(statearr_32537[(13)] = inst_32402__$1);

(statearr_32537[(14)] = inst_32405__$1);

(statearr_32537[(16)] = inst_32403__$1);

return statearr_32537;
})();
var statearr_32538_35234 = state_32461__$1;
(statearr_32538_35234[(2)] = null);

(statearr_32538_35234[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (18))){
var inst_32431 = (state_32461[(2)]);
var state_32461__$1 = state_32461;
var statearr_32543_35238 = state_32461__$1;
(statearr_32543_35238[(2)] = inst_32431);

(statearr_32543_35238[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32462 === (8))){
var inst_32404 = (state_32461[(12)]);
var inst_32405 = (state_32461[(14)]);
var inst_32407 = (inst_32405 < inst_32404);
var inst_32408 = inst_32407;
var state_32461__$1 = state_32461;
if(cljs.core.truth_(inst_32408)){
var statearr_32544_35242 = state_32461__$1;
(statearr_32544_35242[(1)] = (10));

} else {
var statearr_32545_35243 = state_32461__$1;
(statearr_32545_35243[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_32547 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32547[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_32547[(1)] = (1));

return statearr_32547;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_32461){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_32461);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e32551){var ex__29321__auto__ = e32551;
var statearr_32552_35249 = state_32461;
(statearr_32552_35249[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_32461[(4)]))){
var statearr_32553_35250 = state_32461;
(statearr_32553_35250[(1)] = cljs.core.first((state_32461[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35251 = state_32461;
state_32461 = G__35251;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_32461){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_32461);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_32557 = f__30229__auto__();
(statearr_32557[(6)] = c__30228__auto___35117);

return statearr_32557;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return p;
}));

(cljs.core.async.pub.cljs$lang$maxFixedArity = 3);

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var G__32584 = arguments.length;
switch (G__32584) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4(p,topic,ch,true);
}));

(cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_(p,topic,ch,close_QMARK_);
}));

(cljs.core.async.sub.cljs$lang$maxFixedArity = 4);

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_(p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var G__32615 = arguments.length;
switch (G__32615) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_(p);
}));

(cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_(p,topic);
}));

(cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2);

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var G__32641 = arguments.length;
switch (G__32641) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3(f,chs,null);
}));

(cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec(chs);
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var cnt = cljs.core.count(chs__$1);
var rets = cljs.core.object_array.cljs$core$IFn$_invoke$arity$1(cnt);
var dchan = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
var dctr = cljs.core.atom.cljs$core$IFn$_invoke$arity$1(null);
var done = cljs.core.mapv.cljs$core$IFn$_invoke$arity$2((function (i){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2(dchan,rets.slice((0)));
} else {
return null;
}
});
}),cljs.core.range.cljs$core$IFn$_invoke$arity$1(cnt));
if((cnt === (0))){
cljs.core.async.close_BANG_(out);
} else {
var c__30228__auto___35283 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_32712){
var state_val_32713 = (state_32712[(1)]);
if((state_val_32713 === (7))){
var state_32712__$1 = state_32712;
var statearr_32716_35284 = state_32712__$1;
(statearr_32716_35284[(2)] = null);

(statearr_32716_35284[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (1))){
var state_32712__$1 = state_32712;
var statearr_32717_35285 = state_32712__$1;
(statearr_32717_35285[(2)] = null);

(statearr_32717_35285[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (4))){
var inst_32654 = (state_32712[(7)]);
var inst_32658 = (state_32712[(8)]);
var inst_32660 = (inst_32658 < inst_32654);
var state_32712__$1 = state_32712;
if(cljs.core.truth_(inst_32660)){
var statearr_32726_35287 = state_32712__$1;
(statearr_32726_35287[(1)] = (6));

} else {
var statearr_32728_35288 = state_32712__$1;
(statearr_32728_35288[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (15))){
var inst_32694 = (state_32712[(9)]);
var inst_32700 = cljs.core.apply.cljs$core$IFn$_invoke$arity$2(f,inst_32694);
var state_32712__$1 = state_32712;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32712__$1,(17),out,inst_32700);
} else {
if((state_val_32713 === (13))){
var inst_32694 = (state_32712[(9)]);
var inst_32694__$1 = (state_32712[(2)]);
var inst_32696 = cljs.core.some(cljs.core.nil_QMARK_,inst_32694__$1);
var state_32712__$1 = (function (){var statearr_32733 = state_32712;
(statearr_32733[(9)] = inst_32694__$1);

return statearr_32733;
})();
if(cljs.core.truth_(inst_32696)){
var statearr_32734_35290 = state_32712__$1;
(statearr_32734_35290[(1)] = (14));

} else {
var statearr_32735_35291 = state_32712__$1;
(statearr_32735_35291[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (6))){
var state_32712__$1 = state_32712;
var statearr_32736_35292 = state_32712__$1;
(statearr_32736_35292[(2)] = null);

(statearr_32736_35292[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (17))){
var inst_32702 = (state_32712[(2)]);
var state_32712__$1 = (function (){var statearr_32742 = state_32712;
(statearr_32742[(10)] = inst_32702);

return statearr_32742;
})();
var statearr_32743_35295 = state_32712__$1;
(statearr_32743_35295[(2)] = null);

(statearr_32743_35295[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (3))){
var inst_32707 = (state_32712[(2)]);
var state_32712__$1 = state_32712;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32712__$1,inst_32707);
} else {
if((state_val_32713 === (12))){
var _ = (function (){var statearr_32745 = state_32712;
(statearr_32745[(4)] = cljs.core.rest((state_32712[(4)])));

return statearr_32745;
})();
var state_32712__$1 = state_32712;
var ex32741 = (state_32712__$1[(2)]);
var statearr_32747_35303 = state_32712__$1;
(statearr_32747_35303[(5)] = ex32741);


if((ex32741 instanceof Object)){
var statearr_32761_35304 = state_32712__$1;
(statearr_32761_35304[(1)] = (11));

(statearr_32761_35304[(5)] = null);

} else {
throw ex32741;

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (2))){
var inst_32652 = cljs.core.reset_BANG_(dctr,cnt);
var inst_32654 = cnt;
var inst_32658 = (0);
var state_32712__$1 = (function (){var statearr_32774 = state_32712;
(statearr_32774[(7)] = inst_32654);

(statearr_32774[(11)] = inst_32652);

(statearr_32774[(8)] = inst_32658);

return statearr_32774;
})();
var statearr_32775_35310 = state_32712__$1;
(statearr_32775_35310[(2)] = null);

(statearr_32775_35310[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (11))){
var inst_32669 = (state_32712[(2)]);
var inst_32670 = cljs.core.swap_BANG_.cljs$core$IFn$_invoke$arity$2(dctr,cljs.core.dec);
var state_32712__$1 = (function (){var statearr_32778 = state_32712;
(statearr_32778[(12)] = inst_32669);

return statearr_32778;
})();
var statearr_32779_35316 = state_32712__$1;
(statearr_32779_35316[(2)] = inst_32670);

(statearr_32779_35316[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (9))){
var inst_32658 = (state_32712[(8)]);
var _ = (function (){var statearr_32781 = state_32712;
(statearr_32781[(4)] = cljs.core.cons((12),(state_32712[(4)])));

return statearr_32781;
})();
var inst_32676 = (chs__$1.cljs$core$IFn$_invoke$arity$1 ? chs__$1.cljs$core$IFn$_invoke$arity$1(inst_32658) : chs__$1.call(null,inst_32658));
var inst_32677 = (done.cljs$core$IFn$_invoke$arity$1 ? done.cljs$core$IFn$_invoke$arity$1(inst_32658) : done.call(null,inst_32658));
var inst_32678 = cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2(inst_32676,inst_32677);
var ___$1 = (function (){var statearr_32783 = state_32712;
(statearr_32783[(4)] = cljs.core.rest((state_32712[(4)])));

return statearr_32783;
})();
var state_32712__$1 = state_32712;
var statearr_32785_35320 = state_32712__$1;
(statearr_32785_35320[(2)] = inst_32678);

(statearr_32785_35320[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (5))){
var inst_32688 = (state_32712[(2)]);
var state_32712__$1 = (function (){var statearr_32789 = state_32712;
(statearr_32789[(13)] = inst_32688);

return statearr_32789;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32712__$1,(13),dchan);
} else {
if((state_val_32713 === (14))){
var inst_32698 = cljs.core.async.close_BANG_(out);
var state_32712__$1 = state_32712;
var statearr_32790_35321 = state_32712__$1;
(statearr_32790_35321[(2)] = inst_32698);

(statearr_32790_35321[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (16))){
var inst_32705 = (state_32712[(2)]);
var state_32712__$1 = state_32712;
var statearr_32791_35328 = state_32712__$1;
(statearr_32791_35328[(2)] = inst_32705);

(statearr_32791_35328[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (10))){
var inst_32658 = (state_32712[(8)]);
var inst_32681 = (state_32712[(2)]);
var inst_32682 = (inst_32658 + (1));
var inst_32658__$1 = inst_32682;
var state_32712__$1 = (function (){var statearr_32792 = state_32712;
(statearr_32792[(14)] = inst_32681);

(statearr_32792[(8)] = inst_32658__$1);

return statearr_32792;
})();
var statearr_32795_35335 = state_32712__$1;
(statearr_32795_35335[(2)] = null);

(statearr_32795_35335[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32713 === (8))){
var inst_32686 = (state_32712[(2)]);
var state_32712__$1 = state_32712;
var statearr_32796_35337 = state_32712__$1;
(statearr_32796_35337[(2)] = inst_32686);

(statearr_32796_35337[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_32803 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32803[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_32803[(1)] = (1));

return statearr_32803;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_32712){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_32712);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e32812){var ex__29321__auto__ = e32812;
var statearr_32813_35338 = state_32712;
(statearr_32813_35338[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_32712[(4)]))){
var statearr_32817_35339 = state_32712;
(statearr_32817_35339[(1)] = cljs.core.first((state_32712[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35341 = state_32712;
state_32712 = G__35341;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_32712){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_32712);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_32821 = f__30229__auto__();
(statearr_32821[(6)] = c__30228__auto___35283);

return statearr_32821;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));

}

return out;
}));

(cljs.core.async.map.cljs$lang$maxFixedArity = 3);

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var G__32826 = arguments.length;
switch (G__32826) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2(chs,null);
}));

(cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30228__auto___35348 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_32869){
var state_val_32870 = (state_32869[(1)]);
if((state_val_32870 === (7))){
var inst_32843 = (state_32869[(7)]);
var inst_32845 = (state_32869[(8)]);
var inst_32843__$1 = (state_32869[(2)]);
var inst_32845__$1 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32843__$1,(0),null);
var inst_32846 = cljs.core.nth.cljs$core$IFn$_invoke$arity$3(inst_32843__$1,(1),null);
var inst_32847 = (inst_32845__$1 == null);
var state_32869__$1 = (function (){var statearr_32879 = state_32869;
(statearr_32879[(9)] = inst_32846);

(statearr_32879[(7)] = inst_32843__$1);

(statearr_32879[(8)] = inst_32845__$1);

return statearr_32879;
})();
if(cljs.core.truth_(inst_32847)){
var statearr_32889_35352 = state_32869__$1;
(statearr_32889_35352[(1)] = (8));

} else {
var statearr_32890_35353 = state_32869__$1;
(statearr_32890_35353[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32870 === (1))){
var inst_32833 = cljs.core.vec(chs);
var inst_32834 = inst_32833;
var state_32869__$1 = (function (){var statearr_32891 = state_32869;
(statearr_32891[(10)] = inst_32834);

return statearr_32891;
})();
var statearr_32892_35358 = state_32869__$1;
(statearr_32892_35358[(2)] = null);

(statearr_32892_35358[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32870 === (4))){
var inst_32834 = (state_32869[(10)]);
var state_32869__$1 = state_32869;
return cljs.core.async.ioc_alts_BANG_(state_32869__$1,(7),inst_32834);
} else {
if((state_val_32870 === (6))){
var inst_32861 = (state_32869[(2)]);
var state_32869__$1 = state_32869;
var statearr_32894_35365 = state_32869__$1;
(statearr_32894_35365[(2)] = inst_32861);

(statearr_32894_35365[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32870 === (3))){
var inst_32863 = (state_32869[(2)]);
var state_32869__$1 = state_32869;
return cljs.core.async.impl.ioc_helpers.return_chan(state_32869__$1,inst_32863);
} else {
if((state_val_32870 === (2))){
var inst_32834 = (state_32869[(10)]);
var inst_32836 = cljs.core.count(inst_32834);
var inst_32837 = (inst_32836 > (0));
var state_32869__$1 = state_32869;
if(cljs.core.truth_(inst_32837)){
var statearr_32911_35368 = state_32869__$1;
(statearr_32911_35368[(1)] = (4));

} else {
var statearr_32912_35369 = state_32869__$1;
(statearr_32912_35369[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32870 === (11))){
var inst_32834 = (state_32869[(10)]);
var inst_32854 = (state_32869[(2)]);
var tmp32908 = inst_32834;
var inst_32834__$1 = tmp32908;
var state_32869__$1 = (function (){var statearr_32913 = state_32869;
(statearr_32913[(11)] = inst_32854);

(statearr_32913[(10)] = inst_32834__$1);

return statearr_32913;
})();
var statearr_32914_35371 = state_32869__$1;
(statearr_32914_35371[(2)] = null);

(statearr_32914_35371[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32870 === (9))){
var inst_32845 = (state_32869[(8)]);
var state_32869__$1 = state_32869;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32869__$1,(11),out,inst_32845);
} else {
if((state_val_32870 === (5))){
var inst_32859 = cljs.core.async.close_BANG_(out);
var state_32869__$1 = state_32869;
var statearr_32918_35375 = state_32869__$1;
(statearr_32918_35375[(2)] = inst_32859);

(statearr_32918_35375[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32870 === (10))){
var inst_32857 = (state_32869[(2)]);
var state_32869__$1 = state_32869;
var statearr_32919_35377 = state_32869__$1;
(statearr_32919_35377[(2)] = inst_32857);

(statearr_32919_35377[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32870 === (8))){
var inst_32846 = (state_32869[(9)]);
var inst_32843 = (state_32869[(7)]);
var inst_32845 = (state_32869[(8)]);
var inst_32834 = (state_32869[(10)]);
var inst_32849 = (function (){var cs = inst_32834;
var vec__32839 = inst_32843;
var v = inst_32845;
var c = inst_32846;
return (function (p1__32822_SHARP_){
return cljs.core.not_EQ_.cljs$core$IFn$_invoke$arity$2(c,p1__32822_SHARP_);
});
})();
var inst_32850 = cljs.core.filterv(inst_32849,inst_32834);
var inst_32834__$1 = inst_32850;
var state_32869__$1 = (function (){var statearr_32923 = state_32869;
(statearr_32923[(10)] = inst_32834__$1);

return statearr_32923;
})();
var statearr_32924_35381 = state_32869__$1;
(statearr_32924_35381[(2)] = null);

(statearr_32924_35381[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_32930 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_32930[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_32930[(1)] = (1));

return statearr_32930;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_32869){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_32869);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e32934){var ex__29321__auto__ = e32934;
var statearr_32938_35385 = state_32869;
(statearr_32938_35385[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_32869[(4)]))){
var statearr_32939_35387 = state_32869;
(statearr_32939_35387[(1)] = cljs.core.first((state_32869[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35388 = state_32869;
state_32869 = G__35388;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_32869){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_32869);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_32947 = f__30229__auto__();
(statearr_32947[(6)] = c__30228__auto___35348);

return statearr_32947;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return out;
}));

(cljs.core.async.merge.cljs$lang$maxFixedArity = 2);

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce(cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var G__32950 = arguments.length;
switch (G__32950) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30228__auto___35397 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_32992){
var state_val_32993 = (state_32992[(1)]);
if((state_val_32993 === (7))){
var inst_32973 = (state_32992[(7)]);
var inst_32973__$1 = (state_32992[(2)]);
var inst_32974 = (inst_32973__$1 == null);
var inst_32975 = cljs.core.not(inst_32974);
var state_32992__$1 = (function (){var statearr_33003 = state_32992;
(statearr_33003[(7)] = inst_32973__$1);

return statearr_33003;
})();
if(inst_32975){
var statearr_33004_35400 = state_32992__$1;
(statearr_33004_35400[(1)] = (8));

} else {
var statearr_33005_35401 = state_32992__$1;
(statearr_33005_35401[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32993 === (1))){
var inst_32962 = (0);
var state_32992__$1 = (function (){var statearr_33006 = state_32992;
(statearr_33006[(8)] = inst_32962);

return statearr_33006;
})();
var statearr_33010_35402 = state_32992__$1;
(statearr_33010_35402[(2)] = null);

(statearr_33010_35402[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32993 === (4))){
var state_32992__$1 = state_32992;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_32992__$1,(7),ch);
} else {
if((state_val_32993 === (6))){
var inst_32987 = (state_32992[(2)]);
var state_32992__$1 = state_32992;
var statearr_33016_35404 = state_32992__$1;
(statearr_33016_35404[(2)] = inst_32987);

(statearr_33016_35404[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32993 === (3))){
var inst_32989 = (state_32992[(2)]);
var inst_32990 = cljs.core.async.close_BANG_(out);
var state_32992__$1 = (function (){var statearr_33024 = state_32992;
(statearr_33024[(9)] = inst_32989);

return statearr_33024;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_32992__$1,inst_32990);
} else {
if((state_val_32993 === (2))){
var inst_32962 = (state_32992[(8)]);
var inst_32964 = (inst_32962 < n);
var state_32992__$1 = state_32992;
if(cljs.core.truth_(inst_32964)){
var statearr_33025_35407 = state_32992__$1;
(statearr_33025_35407[(1)] = (4));

} else {
var statearr_33026_35408 = state_32992__$1;
(statearr_33026_35408[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32993 === (11))){
var inst_32962 = (state_32992[(8)]);
var inst_32978 = (state_32992[(2)]);
var inst_32979 = (inst_32962 + (1));
var inst_32962__$1 = inst_32979;
var state_32992__$1 = (function (){var statearr_33028 = state_32992;
(statearr_33028[(10)] = inst_32978);

(statearr_33028[(8)] = inst_32962__$1);

return statearr_33028;
})();
var statearr_33029_35413 = state_32992__$1;
(statearr_33029_35413[(2)] = null);

(statearr_33029_35413[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32993 === (9))){
var state_32992__$1 = state_32992;
var statearr_33030_35414 = state_32992__$1;
(statearr_33030_35414[(2)] = null);

(statearr_33030_35414[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32993 === (5))){
var state_32992__$1 = state_32992;
var statearr_33031_35415 = state_32992__$1;
(statearr_33031_35415[(2)] = null);

(statearr_33031_35415[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32993 === (10))){
var inst_32984 = (state_32992[(2)]);
var state_32992__$1 = state_32992;
var statearr_33034_35418 = state_32992__$1;
(statearr_33034_35418[(2)] = inst_32984);

(statearr_33034_35418[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_32993 === (8))){
var inst_32973 = (state_32992[(7)]);
var state_32992__$1 = state_32992;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_32992__$1,(11),out,inst_32973);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_33043 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33043[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_33043[(1)] = (1));

return statearr_33043;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_32992){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_32992);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e33047){var ex__29321__auto__ = e33047;
var statearr_33048_35421 = state_32992;
(statearr_33048_35421[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_32992[(4)]))){
var statearr_33049_35422 = state_32992;
(statearr_33049_35422[(1)] = cljs.core.first((state_32992[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35423 = state_32992;
state_32992 = G__35423;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_32992){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_32992);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_33051 = f__30229__auto__();
(statearr_33051[(6)] = c__30228__auto___35397);

return statearr_33051;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return out;
}));

(cljs.core.async.take.cljs$lang$maxFixedArity = 3);


/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33084 = (function (f,ch,meta33062,_,fn1,meta33085){
this.f = f;
this.ch = ch;
this.meta33062 = meta33062;
this._ = _;
this.fn1 = fn1;
this.meta33085 = meta33085;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33084.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33086,meta33085__$1){
var self__ = this;
var _33086__$1 = this;
return (new cljs.core.async.t_cljs$core$async33084(self__.f,self__.ch,self__.meta33062,self__._,self__.fn1,meta33085__$1));
}));

(cljs.core.async.t_cljs$core$async33084.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33086){
var self__ = this;
var _33086__$1 = this;
return self__.meta33085;
}));

(cljs.core.async.t_cljs$core$async33084.prototype.cljs$core$async$impl$protocols$Handler$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33084.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_(self__.fn1);
}));

(cljs.core.async.t_cljs$core$async33084.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
}));

(cljs.core.async.t_cljs$core$async33084.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit(self__.fn1);
return (function (p1__33058_SHARP_){
var G__33100 = (((p1__33058_SHARP_ == null))?null:(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(p1__33058_SHARP_) : self__.f.call(null,p1__33058_SHARP_)));
return (f1.cljs$core$IFn$_invoke$arity$1 ? f1.cljs$core$IFn$_invoke$arity$1(G__33100) : f1.call(null,G__33100));
});
}));

(cljs.core.async.t_cljs$core$async33084.getBasis = (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33062","meta33062",1298943621,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async33061","cljs.core.async/t_cljs$core$async33061",-1668851677,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta33085","meta33085",-1382699270,null)], null);
}));

(cljs.core.async.t_cljs$core$async33084.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33084.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33084");

(cljs.core.async.t_cljs$core$async33084.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async33084");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33084.
 */
cljs.core.async.__GT_t_cljs$core$async33084 = (function cljs$core$async$__GT_t_cljs$core$async33084(f,ch,meta33062,_,fn1,meta33085){
return (new cljs.core.async.t_cljs$core$async33084(f,ch,meta33062,_,fn1,meta33085));
});



/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33061 = (function (f,ch,meta33062){
this.f = f;
this.ch = ch;
this.meta33062 = meta33062;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33063,meta33062__$1){
var self__ = this;
var _33063__$1 = this;
return (new cljs.core.async.t_cljs$core$async33061(self__.f,self__.ch,meta33062__$1));
}));

(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33063){
var self__ = this;
var _33063__$1 = this;
return self__.meta33062;
}));

(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_(self__.ch,(new cljs.core.async.t_cljs$core$async33084(self__.f,self__.ch,self__.meta33062,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY)));
if(cljs.core.truth_((function (){var and__5000__auto__ = ret;
if(cljs.core.truth_(and__5000__auto__)){
return (!((cljs.core.deref(ret) == null)));
} else {
return and__5000__auto__;
}
})())){
return cljs.core.async.impl.channels.box((function (){var G__33108 = cljs.core.deref(ret);
return (self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(G__33108) : self__.f.call(null,G__33108));
})());
} else {
return ret;
}
}));

(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33061.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
}));

(cljs.core.async.t_cljs$core$async33061.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33062","meta33062",1298943621,null)], null);
}));

(cljs.core.async.t_cljs$core$async33061.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33061.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33061");

(cljs.core.async.t_cljs$core$async33061.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async33061");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33061.
 */
cljs.core.async.__GT_t_cljs$core$async33061 = (function cljs$core$async$__GT_t_cljs$core$async33061(f,ch,meta33062){
return (new cljs.core.async.t_cljs$core$async33061(f,ch,meta33062));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
return (new cljs.core.async.t_cljs$core$async33061(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33116 = (function (f,ch,meta33117){
this.f = f;
this.ch = ch;
this.meta33117 = meta33117;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33116.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33118,meta33117__$1){
var self__ = this;
var _33118__$1 = this;
return (new cljs.core.async.t_cljs$core$async33116(self__.f,self__.ch,meta33117__$1));
}));

(cljs.core.async.t_cljs$core$async33116.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33118){
var self__ = this;
var _33118__$1 = this;
return self__.meta33117;
}));

(cljs.core.async.t_cljs$core$async33116.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33116.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33116.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33116.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async33116.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33116.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,(self__.f.cljs$core$IFn$_invoke$arity$1 ? self__.f.cljs$core$IFn$_invoke$arity$1(val) : self__.f.call(null,val)),fn1);
}));

(cljs.core.async.t_cljs$core$async33116.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33117","meta33117",2128210804,null)], null);
}));

(cljs.core.async.t_cljs$core$async33116.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33116.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33116");

(cljs.core.async.t_cljs$core$async33116.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async33116");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33116.
 */
cljs.core.async.__GT_t_cljs$core$async33116 = (function cljs$core$async$__GT_t_cljs$core$async33116(f,ch,meta33117){
return (new cljs.core.async.t_cljs$core$async33116(f,ch,meta33117));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
return (new cljs.core.async.t_cljs$core$async33116(f,ch,cljs.core.PersistentArrayMap.EMPTY));
});

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async33136 = (function (p,ch,meta33137){
this.p = p;
this.ch = ch;
this.meta33137 = meta33137;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
});
(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_33138,meta33137__$1){
var self__ = this;
var _33138__$1 = this;
return (new cljs.core.async.t_cljs$core$async33136(self__.p,self__.ch,meta33137__$1));
}));

(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_33138){
var self__ = this;
var _33138__$1 = this;
return self__.meta33137;
}));

(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$async$impl$protocols$Channel$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_(self__.ch);
}));

(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$async$impl$protocols$ReadPort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_(self__.ch,fn1);
}));

(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$async$impl$protocols$WritePort$ = cljs.core.PROTOCOL_SENTINEL);

(cljs.core.async.t_cljs$core$async33136.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_((self__.p.cljs$core$IFn$_invoke$arity$1 ? self__.p.cljs$core$IFn$_invoke$arity$1(val) : self__.p.call(null,val)))){
return cljs.core.async.impl.protocols.put_BANG_(self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box(cljs.core.not(cljs.core.async.impl.protocols.closed_QMARK_(self__.ch)));
}
}));

(cljs.core.async.t_cljs$core$async33136.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta33137","meta33137",1256119667,null)], null);
}));

(cljs.core.async.t_cljs$core$async33136.cljs$lang$type = true);

(cljs.core.async.t_cljs$core$async33136.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async33136");

(cljs.core.async.t_cljs$core$async33136.cljs$lang$ctorPrWriter = (function (this__5287__auto__,writer__5288__auto__,opt__5289__auto__){
return cljs.core._write(writer__5288__auto__,"cljs.core.async/t_cljs$core$async33136");
}));

/**
 * Positional factory function for cljs.core.async/t_cljs$core$async33136.
 */
cljs.core.async.__GT_t_cljs$core$async33136 = (function cljs$core$async$__GT_t_cljs$core$async33136(p,ch,meta33137){
return (new cljs.core.async.t_cljs$core$async33136(p,ch,meta33137));
});


/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
return (new cljs.core.async.t_cljs$core$async33136(p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_(cljs.core.complement(p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var G__33183 = arguments.length;
switch (G__33183) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30228__auto___35493 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_33212){
var state_val_33213 = (state_33212[(1)]);
if((state_val_33213 === (7))){
var inst_33208 = (state_33212[(2)]);
var state_33212__$1 = state_33212;
var statearr_33219_35504 = state_33212__$1;
(statearr_33219_35504[(2)] = inst_33208);

(statearr_33219_35504[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33213 === (1))){
var state_33212__$1 = state_33212;
var statearr_33227_35505 = state_33212__$1;
(statearr_33227_35505[(2)] = null);

(statearr_33227_35505[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33213 === (4))){
var inst_33191 = (state_33212[(7)]);
var inst_33191__$1 = (state_33212[(2)]);
var inst_33192 = (inst_33191__$1 == null);
var state_33212__$1 = (function (){var statearr_33234 = state_33212;
(statearr_33234[(7)] = inst_33191__$1);

return statearr_33234;
})();
if(cljs.core.truth_(inst_33192)){
var statearr_33235_35517 = state_33212__$1;
(statearr_33235_35517[(1)] = (5));

} else {
var statearr_33237_35522 = state_33212__$1;
(statearr_33237_35522[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33213 === (6))){
var inst_33191 = (state_33212[(7)]);
var inst_33196 = (p.cljs$core$IFn$_invoke$arity$1 ? p.cljs$core$IFn$_invoke$arity$1(inst_33191) : p.call(null,inst_33191));
var state_33212__$1 = state_33212;
if(cljs.core.truth_(inst_33196)){
var statearr_33238_35523 = state_33212__$1;
(statearr_33238_35523[(1)] = (8));

} else {
var statearr_33239_35524 = state_33212__$1;
(statearr_33239_35524[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33213 === (3))){
var inst_33210 = (state_33212[(2)]);
var state_33212__$1 = state_33212;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33212__$1,inst_33210);
} else {
if((state_val_33213 === (2))){
var state_33212__$1 = state_33212;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33212__$1,(4),ch);
} else {
if((state_val_33213 === (11))){
var inst_33202 = (state_33212[(2)]);
var state_33212__$1 = state_33212;
var statearr_33242_35525 = state_33212__$1;
(statearr_33242_35525[(2)] = inst_33202);

(statearr_33242_35525[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33213 === (9))){
var state_33212__$1 = state_33212;
var statearr_33244_35528 = state_33212__$1;
(statearr_33244_35528[(2)] = null);

(statearr_33244_35528[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33213 === (5))){
var inst_33194 = cljs.core.async.close_BANG_(out);
var state_33212__$1 = state_33212;
var statearr_33247_35530 = state_33212__$1;
(statearr_33247_35530[(2)] = inst_33194);

(statearr_33247_35530[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33213 === (10))){
var inst_33205 = (state_33212[(2)]);
var state_33212__$1 = (function (){var statearr_33249 = state_33212;
(statearr_33249[(8)] = inst_33205);

return statearr_33249;
})();
var statearr_33250_35535 = state_33212__$1;
(statearr_33250_35535[(2)] = null);

(statearr_33250_35535[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33213 === (8))){
var inst_33191 = (state_33212[(7)]);
var state_33212__$1 = state_33212;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33212__$1,(11),out,inst_33191);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_33258 = [null,null,null,null,null,null,null,null,null];
(statearr_33258[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_33258[(1)] = (1));

return statearr_33258;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_33212){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_33212);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e33259){var ex__29321__auto__ = e33259;
var statearr_33260_35542 = state_33212;
(statearr_33260_35542[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_33212[(4)]))){
var statearr_33261_35543 = state_33212;
(statearr_33261_35543[(1)] = cljs.core.first((state_33212[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35545 = state_33212;
state_33212 = G__35545;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_33212){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_33212);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_33263 = f__30229__auto__();
(statearr_33263[(6)] = c__30228__auto___35493);

return statearr_33263;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return out;
}));

(cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var G__33267 = arguments.length;
switch (G__33267) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3(p,ch,null);
}));

(cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3(cljs.core.complement(p),ch,buf_or_n);
}));

(cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3);

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__30228__auto__ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_33386){
var state_val_33387 = (state_33386[(1)]);
if((state_val_33387 === (7))){
var inst_33376 = (state_33386[(2)]);
var state_33386__$1 = state_33386;
var statearr_33394_35556 = state_33386__$1;
(statearr_33394_35556[(2)] = inst_33376);

(statearr_33394_35556[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (20))){
var inst_33319 = (state_33386[(7)]);
var inst_33342 = (state_33386[(2)]);
var inst_33343 = cljs.core.next(inst_33319);
var inst_33302 = inst_33343;
var inst_33303 = null;
var inst_33304 = (0);
var inst_33305 = (0);
var state_33386__$1 = (function (){var statearr_33401 = state_33386;
(statearr_33401[(8)] = inst_33302);

(statearr_33401[(9)] = inst_33342);

(statearr_33401[(10)] = inst_33305);

(statearr_33401[(11)] = inst_33303);

(statearr_33401[(12)] = inst_33304);

return statearr_33401;
})();
var statearr_33402_35561 = state_33386__$1;
(statearr_33402_35561[(2)] = null);

(statearr_33402_35561[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (1))){
var state_33386__$1 = state_33386;
var statearr_33403_35562 = state_33386__$1;
(statearr_33403_35562[(2)] = null);

(statearr_33403_35562[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (4))){
var inst_33284 = (state_33386[(13)]);
var inst_33284__$1 = (state_33386[(2)]);
var inst_33285 = (inst_33284__$1 == null);
var state_33386__$1 = (function (){var statearr_33404 = state_33386;
(statearr_33404[(13)] = inst_33284__$1);

return statearr_33404;
})();
if(cljs.core.truth_(inst_33285)){
var statearr_33405_35570 = state_33386__$1;
(statearr_33405_35570[(1)] = (5));

} else {
var statearr_33406_35571 = state_33386__$1;
(statearr_33406_35571[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (15))){
var state_33386__$1 = state_33386;
var statearr_33410_35572 = state_33386__$1;
(statearr_33410_35572[(2)] = null);

(statearr_33410_35572[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (21))){
var state_33386__$1 = state_33386;
var statearr_33411_35573 = state_33386__$1;
(statearr_33411_35573[(2)] = null);

(statearr_33411_35573[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (13))){
var inst_33302 = (state_33386[(8)]);
var inst_33305 = (state_33386[(10)]);
var inst_33303 = (state_33386[(11)]);
var inst_33304 = (state_33386[(12)]);
var inst_33315 = (state_33386[(2)]);
var inst_33316 = (inst_33305 + (1));
var tmp33407 = inst_33302;
var tmp33408 = inst_33303;
var tmp33409 = inst_33304;
var inst_33302__$1 = tmp33407;
var inst_33303__$1 = tmp33408;
var inst_33304__$1 = tmp33409;
var inst_33305__$1 = inst_33316;
var state_33386__$1 = (function (){var statearr_33415 = state_33386;
(statearr_33415[(8)] = inst_33302__$1);

(statearr_33415[(14)] = inst_33315);

(statearr_33415[(10)] = inst_33305__$1);

(statearr_33415[(11)] = inst_33303__$1);

(statearr_33415[(12)] = inst_33304__$1);

return statearr_33415;
})();
var statearr_33416_35574 = state_33386__$1;
(statearr_33416_35574[(2)] = null);

(statearr_33416_35574[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (22))){
var state_33386__$1 = state_33386;
var statearr_33417_35575 = state_33386__$1;
(statearr_33417_35575[(2)] = null);

(statearr_33417_35575[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (6))){
var inst_33284 = (state_33386[(13)]);
var inst_33297 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_33284) : f.call(null,inst_33284));
var inst_33298 = cljs.core.seq(inst_33297);
var inst_33302 = inst_33298;
var inst_33303 = null;
var inst_33304 = (0);
var inst_33305 = (0);
var state_33386__$1 = (function (){var statearr_33420 = state_33386;
(statearr_33420[(8)] = inst_33302);

(statearr_33420[(10)] = inst_33305);

(statearr_33420[(11)] = inst_33303);

(statearr_33420[(12)] = inst_33304);

return statearr_33420;
})();
var statearr_33421_35576 = state_33386__$1;
(statearr_33421_35576[(2)] = null);

(statearr_33421_35576[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (17))){
var inst_33319 = (state_33386[(7)]);
var inst_33323 = cljs.core.chunk_first(inst_33319);
var inst_33324 = cljs.core.chunk_rest(inst_33319);
var inst_33325 = cljs.core.count(inst_33323);
var inst_33302 = inst_33324;
var inst_33303 = inst_33323;
var inst_33304 = inst_33325;
var inst_33305 = (0);
var state_33386__$1 = (function (){var statearr_33424 = state_33386;
(statearr_33424[(8)] = inst_33302);

(statearr_33424[(10)] = inst_33305);

(statearr_33424[(11)] = inst_33303);

(statearr_33424[(12)] = inst_33304);

return statearr_33424;
})();
var statearr_33428_35578 = state_33386__$1;
(statearr_33428_35578[(2)] = null);

(statearr_33428_35578[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (3))){
var inst_33381 = (state_33386[(2)]);
var state_33386__$1 = state_33386;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33386__$1,inst_33381);
} else {
if((state_val_33387 === (12))){
var inst_33357 = (state_33386[(2)]);
var state_33386__$1 = state_33386;
var statearr_33429_35580 = state_33386__$1;
(statearr_33429_35580[(2)] = inst_33357);

(statearr_33429_35580[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (2))){
var state_33386__$1 = state_33386;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33386__$1,(4),in$);
} else {
if((state_val_33387 === (23))){
var inst_33374 = (state_33386[(2)]);
var state_33386__$1 = state_33386;
var statearr_33430_35582 = state_33386__$1;
(statearr_33430_35582[(2)] = inst_33374);

(statearr_33430_35582[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (19))){
var inst_33349 = (state_33386[(2)]);
var state_33386__$1 = state_33386;
var statearr_33431_35583 = state_33386__$1;
(statearr_33431_35583[(2)] = inst_33349);

(statearr_33431_35583[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (11))){
var inst_33302 = (state_33386[(8)]);
var inst_33319 = (state_33386[(7)]);
var inst_33319__$1 = cljs.core.seq(inst_33302);
var state_33386__$1 = (function (){var statearr_33433 = state_33386;
(statearr_33433[(7)] = inst_33319__$1);

return statearr_33433;
})();
if(inst_33319__$1){
var statearr_33434_35584 = state_33386__$1;
(statearr_33434_35584[(1)] = (14));

} else {
var statearr_33435_35585 = state_33386__$1;
(statearr_33435_35585[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (9))){
var inst_33359 = (state_33386[(2)]);
var inst_33361 = cljs.core.async.impl.protocols.closed_QMARK_(out);
var state_33386__$1 = (function (){var statearr_33436 = state_33386;
(statearr_33436[(15)] = inst_33359);

return statearr_33436;
})();
if(cljs.core.truth_(inst_33361)){
var statearr_33437_35586 = state_33386__$1;
(statearr_33437_35586[(1)] = (21));

} else {
var statearr_33438_35587 = state_33386__$1;
(statearr_33438_35587[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (5))){
var inst_33287 = cljs.core.async.close_BANG_(out);
var state_33386__$1 = state_33386;
var statearr_33440_35589 = state_33386__$1;
(statearr_33440_35589[(2)] = inst_33287);

(statearr_33440_35589[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (14))){
var inst_33319 = (state_33386[(7)]);
var inst_33321 = cljs.core.chunked_seq_QMARK_(inst_33319);
var state_33386__$1 = state_33386;
if(inst_33321){
var statearr_33442_35594 = state_33386__$1;
(statearr_33442_35594[(1)] = (17));

} else {
var statearr_33444_35596 = state_33386__$1;
(statearr_33444_35596[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (16))){
var inst_33353 = (state_33386[(2)]);
var state_33386__$1 = state_33386;
var statearr_33446_35597 = state_33386__$1;
(statearr_33446_35597[(2)] = inst_33353);

(statearr_33446_35597[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33387 === (10))){
var inst_33305 = (state_33386[(10)]);
var inst_33303 = (state_33386[(11)]);
var inst_33310 = cljs.core._nth(inst_33303,inst_33305);
var state_33386__$1 = state_33386;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33386__$1,(13),out,inst_33310);
} else {
if((state_val_33387 === (18))){
var inst_33319 = (state_33386[(7)]);
var inst_33334 = cljs.core.first(inst_33319);
var state_33386__$1 = state_33386;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33386__$1,(20),out,inst_33334);
} else {
if((state_val_33387 === (8))){
var inst_33305 = (state_33386[(10)]);
var inst_33304 = (state_33386[(12)]);
var inst_33307 = (inst_33305 < inst_33304);
var inst_33308 = inst_33307;
var state_33386__$1 = state_33386;
if(cljs.core.truth_(inst_33308)){
var statearr_33450_35600 = state_33386__$1;
(statearr_33450_35600[(1)] = (10));

} else {
var statearr_33451_35601 = state_33386__$1;
(statearr_33451_35601[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__29318__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__29318__auto____0 = (function (){
var statearr_33455 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33455[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__29318__auto__);

(statearr_33455[(1)] = (1));

return statearr_33455;
});
var cljs$core$async$mapcat_STAR__$_state_machine__29318__auto____1 = (function (state_33386){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_33386);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e33459){var ex__29321__auto__ = e33459;
var statearr_33466_35603 = state_33386;
(statearr_33466_35603[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_33386[(4)]))){
var statearr_33467_35605 = state_33386;
(statearr_33467_35605[(1)] = cljs.core.first((state_33386[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35607 = state_33386;
state_33386 = G__35607;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__29318__auto__ = function(state_33386){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__29318__auto____1.call(this,state_33386);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__29318__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__29318__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_33468 = f__30229__auto__();
(statearr_33468[(6)] = c__30228__auto__);

return statearr_33468;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));

return c__30228__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var G__33470 = arguments.length;
switch (G__33470) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3(f,in$,null);
}));

(cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return out;
}));

(cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var G__33473 = arguments.length;
switch (G__33473) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3(f,out,null);
}));

(cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
cljs.core.async.mapcat_STAR_(f,in$,out);

return in$;
}));

(cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var G__33482 = arguments.length;
switch (G__33482) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2(ch,null);
}));

(cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30228__auto___35629 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_33518){
var state_val_33519 = (state_33518[(1)]);
if((state_val_33519 === (7))){
var inst_33513 = (state_33518[(2)]);
var state_33518__$1 = state_33518;
var statearr_33520_35631 = state_33518__$1;
(statearr_33520_35631[(2)] = inst_33513);

(statearr_33520_35631[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33519 === (1))){
var inst_33489 = null;
var state_33518__$1 = (function (){var statearr_33522 = state_33518;
(statearr_33522[(7)] = inst_33489);

return statearr_33522;
})();
var statearr_33526_35633 = state_33518__$1;
(statearr_33526_35633[(2)] = null);

(statearr_33526_35633[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33519 === (4))){
var inst_33498 = (state_33518[(8)]);
var inst_33498__$1 = (state_33518[(2)]);
var inst_33499 = (inst_33498__$1 == null);
var inst_33500 = cljs.core.not(inst_33499);
var state_33518__$1 = (function (){var statearr_33527 = state_33518;
(statearr_33527[(8)] = inst_33498__$1);

return statearr_33527;
})();
if(inst_33500){
var statearr_33528_35635 = state_33518__$1;
(statearr_33528_35635[(1)] = (5));

} else {
var statearr_33529_35636 = state_33518__$1;
(statearr_33529_35636[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33519 === (6))){
var state_33518__$1 = state_33518;
var statearr_33530_35637 = state_33518__$1;
(statearr_33530_35637[(2)] = null);

(statearr_33530_35637[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33519 === (3))){
var inst_33515 = (state_33518[(2)]);
var inst_33516 = cljs.core.async.close_BANG_(out);
var state_33518__$1 = (function (){var statearr_33531 = state_33518;
(statearr_33531[(9)] = inst_33515);

return statearr_33531;
})();
return cljs.core.async.impl.ioc_helpers.return_chan(state_33518__$1,inst_33516);
} else {
if((state_val_33519 === (2))){
var state_33518__$1 = state_33518;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33518__$1,(4),ch);
} else {
if((state_val_33519 === (11))){
var inst_33498 = (state_33518[(8)]);
var inst_33507 = (state_33518[(2)]);
var inst_33489 = inst_33498;
var state_33518__$1 = (function (){var statearr_33546 = state_33518;
(statearr_33546[(10)] = inst_33507);

(statearr_33546[(7)] = inst_33489);

return statearr_33546;
})();
var statearr_33551_35643 = state_33518__$1;
(statearr_33551_35643[(2)] = null);

(statearr_33551_35643[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33519 === (9))){
var inst_33498 = (state_33518[(8)]);
var state_33518__$1 = state_33518;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33518__$1,(11),out,inst_33498);
} else {
if((state_val_33519 === (5))){
var inst_33489 = (state_33518[(7)]);
var inst_33498 = (state_33518[(8)]);
var inst_33502 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_33498,inst_33489);
var state_33518__$1 = state_33518;
if(inst_33502){
var statearr_33553_35644 = state_33518__$1;
(statearr_33553_35644[(1)] = (8));

} else {
var statearr_33554_35645 = state_33518__$1;
(statearr_33554_35645[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33519 === (10))){
var inst_33510 = (state_33518[(2)]);
var state_33518__$1 = state_33518;
var statearr_33555_35646 = state_33518__$1;
(statearr_33555_35646[(2)] = inst_33510);

(statearr_33555_35646[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33519 === (8))){
var inst_33489 = (state_33518[(7)]);
var tmp33552 = inst_33489;
var inst_33489__$1 = tmp33552;
var state_33518__$1 = (function (){var statearr_33558 = state_33518;
(statearr_33558[(7)] = inst_33489__$1);

return statearr_33558;
})();
var statearr_33559_35647 = state_33518__$1;
(statearr_33559_35647[(2)] = null);

(statearr_33559_35647[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_33560 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33560[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_33560[(1)] = (1));

return statearr_33560;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_33518){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_33518);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e33563){var ex__29321__auto__ = e33563;
var statearr_33567_35648 = state_33518;
(statearr_33567_35648[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_33518[(4)]))){
var statearr_33568_35649 = state_33518;
(statearr_33568_35649[(1)] = cljs.core.first((state_33518[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35650 = state_33518;
state_33518 = G__35650;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_33518){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_33518);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_33569 = f__30229__auto__();
(statearr_33569[(6)] = c__30228__auto___35629);

return statearr_33569;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return out;
}));

(cljs.core.async.unique.cljs$lang$maxFixedArity = 2);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var G__33571 = arguments.length;
switch (G__33571) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3(n,ch,null);
}));

(cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30228__auto___35653 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_33640){
var state_val_33641 = (state_33640[(1)]);
if((state_val_33641 === (7))){
var inst_33636 = (state_33640[(2)]);
var state_33640__$1 = state_33640;
var statearr_33648_35655 = state_33640__$1;
(statearr_33648_35655[(2)] = inst_33636);

(statearr_33648_35655[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (1))){
var inst_33579 = (new Array(n));
var inst_33586 = inst_33579;
var inst_33587 = (0);
var state_33640__$1 = (function (){var statearr_33651 = state_33640;
(statearr_33651[(7)] = inst_33587);

(statearr_33651[(8)] = inst_33586);

return statearr_33651;
})();
var statearr_33652_35656 = state_33640__$1;
(statearr_33652_35656[(2)] = null);

(statearr_33652_35656[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (4))){
var inst_33590 = (state_33640[(9)]);
var inst_33590__$1 = (state_33640[(2)]);
var inst_33592 = (inst_33590__$1 == null);
var inst_33593 = cljs.core.not(inst_33592);
var state_33640__$1 = (function (){var statearr_33656 = state_33640;
(statearr_33656[(9)] = inst_33590__$1);

return statearr_33656;
})();
if(inst_33593){
var statearr_33657_35657 = state_33640__$1;
(statearr_33657_35657[(1)] = (5));

} else {
var statearr_33658_35658 = state_33640__$1;
(statearr_33658_35658[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (15))){
var inst_33630 = (state_33640[(2)]);
var state_33640__$1 = state_33640;
var statearr_33671_35659 = state_33640__$1;
(statearr_33671_35659[(2)] = inst_33630);

(statearr_33671_35659[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (13))){
var state_33640__$1 = state_33640;
var statearr_33672_35660 = state_33640__$1;
(statearr_33672_35660[(2)] = null);

(statearr_33672_35660[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (6))){
var inst_33587 = (state_33640[(7)]);
var inst_33620 = (inst_33587 > (0));
var state_33640__$1 = state_33640;
if(cljs.core.truth_(inst_33620)){
var statearr_33676_35661 = state_33640__$1;
(statearr_33676_35661[(1)] = (12));

} else {
var statearr_33678_35662 = state_33640__$1;
(statearr_33678_35662[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (3))){
var inst_33638 = (state_33640[(2)]);
var state_33640__$1 = state_33640;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33640__$1,inst_33638);
} else {
if((state_val_33641 === (12))){
var inst_33586 = (state_33640[(8)]);
var inst_33624 = cljs.core.vec(inst_33586);
var state_33640__$1 = state_33640;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33640__$1,(15),out,inst_33624);
} else {
if((state_val_33641 === (2))){
var state_33640__$1 = state_33640;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33640__$1,(4),ch);
} else {
if((state_val_33641 === (11))){
var inst_33614 = (state_33640[(2)]);
var inst_33615 = (new Array(n));
var inst_33586 = inst_33615;
var inst_33587 = (0);
var state_33640__$1 = (function (){var statearr_33689 = state_33640;
(statearr_33689[(10)] = inst_33614);

(statearr_33689[(7)] = inst_33587);

(statearr_33689[(8)] = inst_33586);

return statearr_33689;
})();
var statearr_33691_35664 = state_33640__$1;
(statearr_33691_35664[(2)] = null);

(statearr_33691_35664[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (9))){
var inst_33586 = (state_33640[(8)]);
var inst_33612 = cljs.core.vec(inst_33586);
var state_33640__$1 = state_33640;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33640__$1,(11),out,inst_33612);
} else {
if((state_val_33641 === (5))){
var inst_33587 = (state_33640[(7)]);
var inst_33603 = (state_33640[(11)]);
var inst_33590 = (state_33640[(9)]);
var inst_33586 = (state_33640[(8)]);
var inst_33600 = (inst_33586[inst_33587] = inst_33590);
var inst_33603__$1 = (inst_33587 + (1));
var inst_33606 = (inst_33603__$1 < n);
var state_33640__$1 = (function (){var statearr_33697 = state_33640;
(statearr_33697[(11)] = inst_33603__$1);

(statearr_33697[(12)] = inst_33600);

return statearr_33697;
})();
if(cljs.core.truth_(inst_33606)){
var statearr_33698_35666 = state_33640__$1;
(statearr_33698_35666[(1)] = (8));

} else {
var statearr_33699_35667 = state_33640__$1;
(statearr_33699_35667[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (14))){
var inst_33633 = (state_33640[(2)]);
var inst_33634 = cljs.core.async.close_BANG_(out);
var state_33640__$1 = (function (){var statearr_33702 = state_33640;
(statearr_33702[(13)] = inst_33633);

return statearr_33702;
})();
var statearr_33704_35668 = state_33640__$1;
(statearr_33704_35668[(2)] = inst_33634);

(statearr_33704_35668[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (10))){
var inst_33618 = (state_33640[(2)]);
var state_33640__$1 = state_33640;
var statearr_33705_35669 = state_33640__$1;
(statearr_33705_35669[(2)] = inst_33618);

(statearr_33705_35669[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33641 === (8))){
var inst_33603 = (state_33640[(11)]);
var inst_33586 = (state_33640[(8)]);
var tmp33701 = inst_33586;
var inst_33586__$1 = tmp33701;
var inst_33587 = inst_33603;
var state_33640__$1 = (function (){var statearr_33706 = state_33640;
(statearr_33706[(7)] = inst_33587);

(statearr_33706[(8)] = inst_33586__$1);

return statearr_33706;
})();
var statearr_33707_35671 = state_33640__$1;
(statearr_33707_35671[(2)] = null);

(statearr_33707_35671[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_33711 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33711[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_33711[(1)] = (1));

return statearr_33711;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_33640){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_33640);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e33715){var ex__29321__auto__ = e33715;
var statearr_33716_35672 = state_33640;
(statearr_33716_35672[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_33640[(4)]))){
var statearr_33717_35673 = state_33640;
(statearr_33717_35673[(1)] = cljs.core.first((state_33640[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35674 = state_33640;
state_33640 = G__35674;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_33640){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_33640);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_33718 = f__30229__auto__();
(statearr_33718[(6)] = c__30228__auto___35653);

return statearr_33718;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return out;
}));

(cljs.core.async.partition.cljs$lang$maxFixedArity = 3);

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var G__33722 = arguments.length;
switch (G__33722) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error(["Invalid arity: ",cljs.core.str.cljs$core$IFn$_invoke$arity$1(arguments.length)].join('')));

}
});

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3(f,ch,null);
}));

(cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1(buf_or_n);
var c__30228__auto___35680 = cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((1));
cljs.core.async.impl.dispatch.run((function (){
var f__30229__auto__ = (function (){var switch__29317__auto__ = (function (state_33777){
var state_val_33778 = (state_33777[(1)]);
if((state_val_33778 === (7))){
var inst_33773 = (state_33777[(2)]);
var state_33777__$1 = state_33777;
var statearr_33782_35682 = state_33777__$1;
(statearr_33782_35682[(2)] = inst_33773);

(statearr_33782_35682[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (1))){
var inst_33727 = [];
var inst_33728 = inst_33727;
var inst_33729 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_33777__$1 = (function (){var statearr_33786 = state_33777;
(statearr_33786[(7)] = inst_33729);

(statearr_33786[(8)] = inst_33728);

return statearr_33786;
})();
var statearr_33789_35687 = state_33777__$1;
(statearr_33789_35687[(2)] = null);

(statearr_33789_35687[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (4))){
var inst_33732 = (state_33777[(9)]);
var inst_33732__$1 = (state_33777[(2)]);
var inst_33733 = (inst_33732__$1 == null);
var inst_33734 = cljs.core.not(inst_33733);
var state_33777__$1 = (function (){var statearr_33791 = state_33777;
(statearr_33791[(9)] = inst_33732__$1);

return statearr_33791;
})();
if(inst_33734){
var statearr_33792_35688 = state_33777__$1;
(statearr_33792_35688[(1)] = (5));

} else {
var statearr_33793_35689 = state_33777__$1;
(statearr_33793_35689[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (15))){
var inst_33728 = (state_33777[(8)]);
var inst_33765 = cljs.core.vec(inst_33728);
var state_33777__$1 = state_33777;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33777__$1,(18),out,inst_33765);
} else {
if((state_val_33778 === (13))){
var inst_33760 = (state_33777[(2)]);
var state_33777__$1 = state_33777;
var statearr_33796_35691 = state_33777__$1;
(statearr_33796_35691[(2)] = inst_33760);

(statearr_33796_35691[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (6))){
var inst_33728 = (state_33777[(8)]);
var inst_33762 = inst_33728.length;
var inst_33763 = (inst_33762 > (0));
var state_33777__$1 = state_33777;
if(cljs.core.truth_(inst_33763)){
var statearr_33799_35693 = state_33777__$1;
(statearr_33799_35693[(1)] = (15));

} else {
var statearr_33801_35694 = state_33777__$1;
(statearr_33801_35694[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (17))){
var inst_33770 = (state_33777[(2)]);
var inst_33771 = cljs.core.async.close_BANG_(out);
var state_33777__$1 = (function (){var statearr_33805 = state_33777;
(statearr_33805[(10)] = inst_33770);

return statearr_33805;
})();
var statearr_33806_35696 = state_33777__$1;
(statearr_33806_35696[(2)] = inst_33771);

(statearr_33806_35696[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (3))){
var inst_33775 = (state_33777[(2)]);
var state_33777__$1 = state_33777;
return cljs.core.async.impl.ioc_helpers.return_chan(state_33777__$1,inst_33775);
} else {
if((state_val_33778 === (12))){
var inst_33728 = (state_33777[(8)]);
var inst_33753 = cljs.core.vec(inst_33728);
var state_33777__$1 = state_33777;
return cljs.core.async.impl.ioc_helpers.put_BANG_(state_33777__$1,(14),out,inst_33753);
} else {
if((state_val_33778 === (2))){
var state_33777__$1 = state_33777;
return cljs.core.async.impl.ioc_helpers.take_BANG_(state_33777__$1,(4),ch);
} else {
if((state_val_33778 === (11))){
var inst_33742 = (state_33777[(11)]);
var inst_33728 = (state_33777[(8)]);
var inst_33732 = (state_33777[(9)]);
var inst_33750 = inst_33728.push(inst_33732);
var tmp33807 = inst_33728;
var inst_33728__$1 = tmp33807;
var inst_33729 = inst_33742;
var state_33777__$1 = (function (){var statearr_33812 = state_33777;
(statearr_33812[(12)] = inst_33750);

(statearr_33812[(7)] = inst_33729);

(statearr_33812[(8)] = inst_33728__$1);

return statearr_33812;
})();
var statearr_33813_35699 = state_33777__$1;
(statearr_33813_35699[(2)] = null);

(statearr_33813_35699[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (9))){
var inst_33729 = (state_33777[(7)]);
var inst_33746 = cljs.core.keyword_identical_QMARK_(inst_33729,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var state_33777__$1 = state_33777;
var statearr_33822_35700 = state_33777__$1;
(statearr_33822_35700[(2)] = inst_33746);

(statearr_33822_35700[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (5))){
var inst_33743 = (state_33777[(13)]);
var inst_33742 = (state_33777[(11)]);
var inst_33729 = (state_33777[(7)]);
var inst_33732 = (state_33777[(9)]);
var inst_33742__$1 = (f.cljs$core$IFn$_invoke$arity$1 ? f.cljs$core$IFn$_invoke$arity$1(inst_33732) : f.call(null,inst_33732));
var inst_33743__$1 = cljs.core._EQ_.cljs$core$IFn$_invoke$arity$2(inst_33742__$1,inst_33729);
var state_33777__$1 = (function (){var statearr_33823 = state_33777;
(statearr_33823[(13)] = inst_33743__$1);

(statearr_33823[(11)] = inst_33742__$1);

return statearr_33823;
})();
if(inst_33743__$1){
var statearr_33824_35706 = state_33777__$1;
(statearr_33824_35706[(1)] = (8));

} else {
var statearr_33825_35707 = state_33777__$1;
(statearr_33825_35707[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (14))){
var inst_33742 = (state_33777[(11)]);
var inst_33732 = (state_33777[(9)]);
var inst_33755 = (state_33777[(2)]);
var inst_33756 = [];
var inst_33757 = inst_33756.push(inst_33732);
var inst_33728 = inst_33756;
var inst_33729 = inst_33742;
var state_33777__$1 = (function (){var statearr_33827 = state_33777;
(statearr_33827[(14)] = inst_33757);

(statearr_33827[(7)] = inst_33729);

(statearr_33827[(8)] = inst_33728);

(statearr_33827[(15)] = inst_33755);

return statearr_33827;
})();
var statearr_33829_35714 = state_33777__$1;
(statearr_33829_35714[(2)] = null);

(statearr_33829_35714[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (16))){
var state_33777__$1 = state_33777;
var statearr_33832_35718 = state_33777__$1;
(statearr_33832_35718[(2)] = null);

(statearr_33832_35718[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (10))){
var inst_33748 = (state_33777[(2)]);
var state_33777__$1 = state_33777;
if(cljs.core.truth_(inst_33748)){
var statearr_33834_35725 = state_33777__$1;
(statearr_33834_35725[(1)] = (11));

} else {
var statearr_33836_35727 = state_33777__$1;
(statearr_33836_35727[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (18))){
var inst_33767 = (state_33777[(2)]);
var state_33777__$1 = state_33777;
var statearr_33837_35729 = state_33777__$1;
(statearr_33837_35729[(2)] = inst_33767);

(statearr_33837_35729[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33778 === (8))){
var inst_33743 = (state_33777[(13)]);
var state_33777__$1 = state_33777;
var statearr_33844_35730 = state_33777__$1;
(statearr_33844_35730[(2)] = inst_33743);

(statearr_33844_35730[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});
return (function() {
var cljs$core$async$state_machine__29318__auto__ = null;
var cljs$core$async$state_machine__29318__auto____0 = (function (){
var statearr_33848 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33848[(0)] = cljs$core$async$state_machine__29318__auto__);

(statearr_33848[(1)] = (1));

return statearr_33848;
});
var cljs$core$async$state_machine__29318__auto____1 = (function (state_33777){
while(true){
var ret_value__29319__auto__ = (function (){try{while(true){
var result__29320__auto__ = switch__29317__auto__(state_33777);
if(cljs.core.keyword_identical_QMARK_(result__29320__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__29320__auto__;
}
break;
}
}catch (e33853){var ex__29321__auto__ = e33853;
var statearr_33854_35737 = state_33777;
(statearr_33854_35737[(2)] = ex__29321__auto__);


if(cljs.core.seq((state_33777[(4)]))){
var statearr_33855_35738 = state_33777;
(statearr_33855_35738[(1)] = cljs.core.first((state_33777[(4)])));

} else {
throw ex__29321__auto__;
}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
}})();
if(cljs.core.keyword_identical_QMARK_(ret_value__29319__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35740 = state_33777;
state_33777 = G__35740;
continue;
} else {
return ret_value__29319__auto__;
}
break;
}
});
cljs$core$async$state_machine__29318__auto__ = function(state_33777){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__29318__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__29318__auto____1.call(this,state_33777);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__29318__auto____0;
cljs$core$async$state_machine__29318__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__29318__auto____1;
return cljs$core$async$state_machine__29318__auto__;
})()
})();
var state__30230__auto__ = (function (){var statearr_33860 = f__30229__auto__();
(statearr_33860[(6)] = c__30228__auto___35680);

return statearr_33860;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped(state__30230__auto__);
}));


return out;
}));

(cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3);


//# sourceMappingURL=cljs.core.async.js.map
