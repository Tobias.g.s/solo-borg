(ns solo-borg.world-tick)

;; the progression of world state

(def activities
  {
    :combat {:do-tick? false}
    :resting {:do-tick? true}
   })

(def world-map
  {
   :current-ticks 0
   :day-length 144
   :year 0
   :day 1
   :month 1
   :activity :combat
   })


(defn tick? [game-state]
  (-> activities (:activity game-state) :do-tick?))

(defn maybe-do-tick [game-state]
  (if (tick? game-state)
    (assoc game-state :current-ticks (inc (:current-ticks game-state)))
    game-state)
  )
