(ns solo-borg.core
  (:require
   [cljs.pprint :as pprint]
   [reagent.core :as r]
   [reagent.dom.client :as rdom]
   [solo-borg.views :refer [main-view]]))

(defonce root
  (rdom/create-root (.getElementById js/document "app")))

(defn render []
  (rdom/render
   root (r/as-element (if goog.DEBUG
                        [(fn [] [main-view])]
                        [main-view]))))

(defn ^:dev/after-load start []
  (js/console.log "start")
  (render))

(defn ^:export init []
  (start)
  (pprint/pprint "hello borg"))


