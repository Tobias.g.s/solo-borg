(ns solo-borg.state
  (:require
   [reagent.core :as r]))

(defonce game (r/atom {}))
(defonce app (r/atom {:selected-view [:home {}]}))

(defn select-view [key props]
  (swap! app assoc
         :selected-view [key props]))
