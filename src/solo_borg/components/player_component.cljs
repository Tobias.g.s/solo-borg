(ns solo-borg.components.player-component)


(defn stat-block [{:keys [key stats]}]
  (into [:div.box]
        (map-indexed
         (fn [idx stat]
           [:p.subtitle {:key (str "statblock" idx)}(str (name (first stat)) ":" (second stat))])
         stats)))


(defn omens [{:keys [omens]}]
   [:p.subtitle (str "Omens: " omens)])

(defn player [props]
  (let [player-data (:player props)]
    [:div.box
     [:p.subtitle (str "Name: " (:name player-data))]
     [:p.subtitle (str "Class: " (name (:class player-data)))]
     [:p.subtitle "Stats: "]
     [stat-block player-data]
     [:p.subtitle (str "Hp: " (:hp player-data))]
     [:p.subtitle (str "Silver: " (:silver player-data))]
     [omens player-data]]))



