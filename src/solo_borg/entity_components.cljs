(ns solo-borg.entity-components
  (:require
   [cljs.pprint :as pprint]
   [solo-borg.state :as state]
   [solo-borg.utils.common :as common-utils]))

(def classes
  {:gutter-born-scum {:modifiers {:stats {:strength -2
                                          :agility 0
                                          :presence 0
                                          :toughness 0}
                                  :omens 1
                                  :hp (fn [] (inc (rand-int 6)))
                                  :silver  (fn [] (* 10 (* 2 (common-utils/roll-dice 6))))}}
   :esoteric-hermit {:modifiers {:stats {:strength -2
                                         :agility 0
                                         :presence 2
                                         :toughness 0}
                                 :omens 4
                                 :hp (fn [] (inc (rand-int 4)))
                                 :silver (fn [] (* 10 (* 2 (common-utils/roll-dice 6))))}}
   :wretched-royalty {:modifiers {:stats {:strength 0
                                          :agility 0
                                          :presence 0
                                          :toughness 0}
                                  :omens 4
                                  :hp (fn [] (inc (rand-int 6)))
                                  :silver (fn [] (* 10 (* 4 (common-utils/roll-dice 6))))}}
   :heretical-priest {:modifiers {:stats {:strength -2
                                          :agility 0
                                          :presence 2
                                          :toughness 0}
                                  :omens 4
                                  :hp (fn []  (inc (rand-int 8)))
                                  :silver (fn  [] (* 10 (* 3 (common-utils/roll-dice 6))))}}
   :occult-herbmaster {:modifiers {:stats {:strength -2
                                           :agility 0
                                           :presence 0
                                           :toughness 2}
                                   :omens 4
                                   :hp (fn [] (inc (rand-int 6)))
                                   :silver (fn [] (* 10 (* 2 (common-utils/roll-dice 6))))}}
   :fanged-deserter {:modifiers {:stats {:strength 2
                                         :agility -1
                                         :presence -1
                                         :toughness 0}
                                 :omens 2
                                 :hp (fn [] (inc (rand-int 8)))
                                 :silver (fn [] (* 10 (* 2 (common-utils/roll-dice 6))))}}})

(def curses
  {:gluttony {:type :curse :description "you are fat kek" :modifiers {:stats {:toughness 2 :agility -1}}}})


(def base-traits
  {:breathing {:title "" :description ""}
   :head {:title "" :description ""}
   :torso {:title "" :description ""}
   :left-arm {:title "" :description ""}
   :right-arm {:title "" :description ""}
   :left-leg {:title "" :description ""}
   :right-leg {:title "" :description ""}
   :animated {:title "" :description ""}})

(def terrible-traits
  {
   :endlessly-aggravated {}
   :inferiority-complex {}
   :problems-complex {}
   :loud-mouth {}
   :cruel {}
   :egocentric {}
   :nihilist {}
   :prone-to-substanc-abuse {}
   :conflicted {}
   :shrewd {}
   :vindictive {}
   :cowardly {}
   :lazy {}
   :suspicious {}
   :ruthless {}
   :worried {}
   :bitter {}
   :deceitful {}
   :wasteful {}
   :arrogant {}
   })

(def broken-body-traits ; roll 1
  {
   :staring-maniac-gaze {}
   :covered-in-blasphemous-tattoos {}
   :face-rotting {}
   :masked {}
   :lost-three-toes {}
   :limps {}
   })

(def bad-behaviour-traits ; roll 2 
  {
   :obsessivly-collect-small-sharp-stones {}
   :wont-use-blade-without-testing-it-on-your-own-flesh {}
   :arms-knitted-with-scars {}
   :cant-stop-drinking-once-started {}
   :gambling-addict{}
   :cant-tolarate-critisism {}
   })


(def neutral-traits
  {:ugly {}
   :hook-hand {}})

(def negative-traits
  {:ugly {}})

(def positive-traits
  {:handsome {}})

(def names
  {"senkil" {:type #{:npc :player}}
   "vemut" {:type #{:npc :player}}
   "keftar" {:type #{:npc :player}}
   "domm" {:type #{:npc :player}}
   "agn" {:type #{:npc :player}}
   "katla" {:type #{:npc :player}}
   "nagl" {:type #{:npc :player}}
   "svind" {:type #{:npc :player}}
   "kutz" {:type #{:npc :player}}
   "felban" {:type #{:npc :player}}})

(defn pick-random [collection]
  (-> collection keys shuffle first))

(defn generate-stats [class]
  (merge-with + (get-in class [:modifiers :stats] {})
              {:strength (common-utils/dice->stat (apply + (common-utils/roll-multiple-dice 3 6)))
               :agility (common-utils/dice->stat (apply + (common-utils/roll-multiple-dice 3 6)))
               :presence (common-utils/dice->stat (apply + (common-utils/roll-multiple-dice 3 6)))
               :toughness (common-utils/dice->stat (apply + (common-utils/roll-multiple-dice 3 6)))}))

(defn pick-name []
  (pick-random names))

(defn pick-multiple-names [amount]
  (assert (and amount (<= amount (count names))))
  (take amount (-> names keys shuffle)))

(def player-example
  {:type #{:player-character :undead}
   :name "Knut"
   :stats {:strength 0
           :agility -1
           :presence 3
           :toughness 1}
   :hp [-1 1]
   :omens 2
   :class :gutter-born-scum
   :traits #{:strong, :handsome, :well-endowed, :heretic,  :left-hand-is-hook,  :right-hand, :left-leg, :right-leg, :head, :torso, :face-rotting, :bleeding,  :hungry, :panicked, :empovered}})

(defn create-random-player []
  (let [class (pick-random classes)]
    {:name (pick-random names)
     :stats (generate-stats class)
     :class class
     :omens (get-in classes [class :modifiers :omens])
     :hp (+ (max 0 ((get-in classes [class :modifiers :hp]))))
     :silver ((get-in classes [class :modifiers :silver]))}))
(defn add-player! [& [player]]
  (let [player (or player (create-random-player))
        id (or (:id player) (random-uuid))]
    (swap! state/game assoc-in [:players id] (assoc player :id id))))

(defn delete-player! [& [id]]
    (swap! state/game update-in [:players] (fn [players] (dissoc players id))))
