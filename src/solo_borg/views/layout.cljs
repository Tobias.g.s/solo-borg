(ns solo-borg.views.layout
  (:require
   [solo-borg.utils.react :as react]
   ["@formkit/auto-animate$default" :as aa]
   [cljs.pprint :as m]))

(defn default [& content]
  (let [[parent-ref] (react/use-auto-animate)]
    (cljs.pprint/pprint (js-keys aa))
    [:div.container
     (into [:section.section {:ref parent-ref}] content)]))



