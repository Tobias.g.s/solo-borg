(ns solo-borg.views.character-creator
  (:require
   [solo-borg.components.player-component :as player-component]
   [solo-borg.entity-components :as ec]
   [solo-borg.state :refer [select-view] :as state]
   [solo-borg.utils.react :as react]))

(defn view [_props]
  (let [[parent-ref] (react/use-auto-animate)]
    [:div
     [:button.button
      {:on-click #(select-view :home {})}
      "home"]
     [:h1.title {:style {:color "black"}} "Scum Birther"]
     [:button.button
      {:on-click (fn [] (ec/add-player!))}
      "Birth scum"]
     (into [:div.box

            {:ref parent-ref}]
           (map-indexed
            (fn [idx player]
              [:div
               {:key (str "player something" idx)}
               [:button.button
                {:on-click (fn [] (ec/delete-player! (first player)))}
                "x"]
               [player-component/player
                {:key (str "player something" idx)
                 :player (second player)}]])
            (:players @state/game)))]))







