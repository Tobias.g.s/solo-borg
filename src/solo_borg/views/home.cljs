(ns solo-borg.views.home
  (:require [solo-borg.state :refer [select-view]]))

(defn view [props]
  [:div
   [:button.button
    {:on-click #(select-view :character-creator {})}
    "Create Character"]])

