(ns solo-borg.entity-interactions
  (:require
   [cljs.pprint :as pprint]
   [solo-borg.entity-components :as ec]))

(defmulti touch
  (fn [entity target]
    (:type target)))

(defmethod touch #{:wall}
  [entity target]
  (cond (and (contains? (:type entity) :player-character)
             (contains? (:traits target) :cursed))
        {:game-state-updates [[:update-in [:players (:id entity) :traits] (fn [traits] (conj traits (ec/pick-random ec/curses)))]]
         :app-state-updates [[:update-in [:players (:id entity) :hp 0] inc]]}

        :else
        (pprint/pprint "you touched wall, it was cold."))
  {:game-state-updates [[:update-in [:players (:id entity) :hp 0] inc]]
   :app-state-updates [[:update-in [:players (:id entity) :hp 0] inc]]})

(defmethod touch :default
  [entity target]
  (pprint/pprint "there is nothing to touch.")
  {:game-state-updates [[:update-in [:players (:id entity) :hp 0] inc]]
   :app-state-updates [[:update-in [:players (:id entity) :hp 0] inc]]})

(defmethod touch #{:wall :door}
  [entity target]
  [[:update-in [:players (:id entity) :hp 0] inc]])

(defmulti interact identity)

(defmethod interact :touch
  [_ entity target] ;entity is pretty much self i don't know why do we do this??
  (touch entity target))

(defmethod interact :default
  [_ entity target])
