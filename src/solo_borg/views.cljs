(ns solo-borg.views (:require
                     [solo-borg.state :as state]
                     [solo-borg.views.character-creator :as character-creator]
                     [solo-borg.views.home :as home]
                     [solo-borg.views.layout :as layout]
                     [cljs.pprint :as pprint]))

(defmulti scenes (fn
                   [[key _props]]
                   key))

(defmethod scenes :home
  [[_ props]] props
  [home/view props])

(defmethod scenes :character-creator
  [[_ props]]
  (pprint/pprint _)
  (pprint/pprint props)
  [:f> character-creator/view props])

(defn main-view []
  (let [selected-view (:selected-view @state/app)]
    [:f> layout/default (scenes selected-view)]))
