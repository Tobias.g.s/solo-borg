(ns solo-borg.utils.react
  (:require
   ["react" :as react]
   ["@formkit/auto-animate/react" :refer [useAutoAnimate]]))

(def use-state
  react/useState)

(def use-effect
  react/useEffect)

(def use-auto-animate
  useAutoAnimate)
