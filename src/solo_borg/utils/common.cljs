(ns solo-borg.utils.common)

(defn roll-dice [mod]
  (assert (and mod (> mod 1)))
  (inc (rand-int mod)))

(defn roll-multiple-dice [amount mod]
  (assert (and amount (>= amount 1)))
  (take amount (repeatedly #(roll-dice mod))))

(defn dice->stat [num]
  (assert (and (>= num 1) (<= num 20)))
  (cond
    (<= 1 num 4) -3
    (<= 5 num 6) -2
    (<= 7 num 8) -1
    (<= 9 num 12) 0
    (<= 13 num 14) 1
    (<= 15 num 16) 2
    (<= 17 num 20) 3
    :else 0))

